<?php

use yii\db\Migration;

class m180205_160816_create_table__kkb_payment_gate_profile extends Migration
{
    const TABLE_KKB_PAYMENT_GATE_PROFILE = '{{%kkb_payment_gate_profile}}';
    const TABLE_PAYMENT_GATE_TYPE = '{{%payment_gate_type}}';
    const PAYMENT_GATE_TYPE_ID = 8;

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_KKB_PAYMENT_GATE_PROFILE, [
            'id'                     => $this->primaryKey(),
            'paymentName'            => $this->string(32)->unique()->notNull(),
            'merchantId'             => $this->string(32)->unique()->notNull(),
            'merchantName'           => $this->string()->notNull(),
            'certificateId'          => $this->string(32)->notNull(),
            'privateCertificate'     => $this->binary()->notNull(),
            'privateCertificatePass' => $this->string()->notNull(),
            'publicCertificate'      => $this->binary()->notNull(),
            'isDebug'                => $this->integer(1)->notNull()->defaultValue(0),
            'cardBindingSum'         => $this->integer()->notNull()->defaultValue(1),
            'commission'             => $this->float()->defaultValue(0),
            'currency'               => $this->string(3),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->insert(self::TABLE_PAYMENT_GATE_TYPE, [
            'id'        => self::PAYMENT_GATE_TYPE_ID,
            'name'      => 'КАЗКОМ (Kkb)',
            'classname' => 'Kkb',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_KKB_PAYMENT_GATE_PROFILE);
        $this->delete(self::TABLE_PAYMENT_GATE_TYPE, ['id' => self::PAYMENT_GATE_TYPE_ID]);
    }
}
