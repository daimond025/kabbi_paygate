<?php

use yii\db\Migration;

class m171216_181332_paygate_profile__increase_column_size extends Migration
{
    const TABLE_PAYCOM_PROFILE = '{{%paycom_profile}}';
    const COLUMN_API_ID = 'apiId';
    const COLUMN_PASSWORD = 'password';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_PAYCOM_PROFILE, self::COLUMN_API_ID, $this->string(255)->defaultValue(''));
        $this->alterColumn(self::TABLE_PAYCOM_PROFILE, self::COLUMN_PASSWORD, $this->string(255)->defaultValue(''));
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_PAYCOM_PROFILE, self::COLUMN_API_ID, $this->string(32)->defaultValue(''));
        $this->alterColumn(self::TABLE_PAYCOM_PROFILE, self::COLUMN_PASSWORD, $this->string(32)->defaultValue(''));
    }
}
