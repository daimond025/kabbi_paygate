<?php

use yii\db\Migration;

/**
 * Handles the creation for table `yandex_order_binding`.
 */
class m160622_105011_create_yandex_order_binding extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('yandex_order_binding', [
            'id' => $this->primaryKey(),
            'shopId' => $this->bigInteger()->notNull(),
            'clientId' => $this->string(32)->notNull(),
            'invoiceId' => $this->string()->notNull(),
            'pan' => $this->string(32),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('yandex_order_binding');
    }
}
