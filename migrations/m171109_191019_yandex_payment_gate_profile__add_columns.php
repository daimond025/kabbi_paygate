<?php

use yii\db\Migration;

class m171109_191019_yandex_payment_gate_profile__add_columns extends Migration
{
    const TABLE_NAME = 'yandex_payment_gate_profile';
    const COLUMN_SECURE_DEAL_PAYMENT_SHOP_ID = 'secureDealPaymentShopId';
    const COLUMN_SECURE_DEAL_PAYMENT_SCID = 'secureDealPaymentScid';
    const COLUMN_SECURE_DEAL_PAYMENT_SHOP_ARTICLE_ID = 'secureDealPaymentShopArticleId';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SHOP_ID, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SCID, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SHOP_ARTICLE_ID, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SHOP_ID);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SCID);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_PAYMENT_SHOP_ARTICLE_ID);
    }

}
