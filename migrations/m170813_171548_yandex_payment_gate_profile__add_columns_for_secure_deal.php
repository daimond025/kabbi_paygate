<?php

use yii\db\Migration;

class m170813_171548_yandex_payment_gate_profile__add_columns_for_secure_deal extends Migration
{
    const TABLE_NAME = 'yandex_payment_gate_profile';
    const COLUMN_SECURE_DEAL = 'secureDeal';
    const COLUMN_SECURE_DEAL_AGENT_ID = 'agentId';
    const COLUMN_SECURE_DEAL_SHOP_ID = 'secureDealShopId';
    const COLUMN_SECURE_DEAL_SCID = 'secureDealScid';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL, $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_AGENT_ID, $this->string(250));
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SHOP_ID, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SCID, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_AGENT_ID);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SHOP_ID);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SCID);
    }
}
