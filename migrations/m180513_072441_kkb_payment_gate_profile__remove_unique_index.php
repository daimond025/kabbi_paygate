<?php

use yii\db\Migration;

class m180513_072441_kkb_payment_gate_profile__remove_unique_index extends Migration
{
    const TABLE_NAME = '{{%kkb_payment_gate_profile}}';
    const COLUMN_NAME = 'merchantId';
    const INDEX_NAME = 'merchantId';

    public function safeUp()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, self::COLUMN_NAME);
    }

    public function safeDown()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, self::COLUMN_NAME, true);
    }
}
