<?php

use yii\db\Migration;

class m160406_121233_currency extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('currency', [
            'code' => $this->primaryKey(),
            'oldCode' => $this->integer()->notNull(),
            'description' => $this->string(5), 
        ], $tableOptions);
        $this->batchInsert('currency', ['code', 'oldCode', 'description'], [
            [944,  31, 'AZN'], // Азербайджанский манат 
            [975, 100, 'BGN'], // Болгарский лев
            [941, 891, 'RSD'], // Сербский динар
            [936, 288, 'GHS'], // Ганский седи
            [946, 642, 'RON'], // Румынский лей
            [643, 810, 'RUB'],
            [938, 736, 'SDG'], // Суданский фунт (Суданский динар)
            [949, 792, 'TRY'], // Турецкая лира
            [937, 862, 'VEF'], // Венесуэльский боливар 
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('currency');
    }
}
