<?php

use yii\db\Migration;

class m171117_151424_yandex_payment_gate_profile__add_column__card_binding_sum extends Migration
{
    const TABLE_NAME = '{{%yandex_payment_gate_profile}}';
    const COLUMN_NAME = 'cardBindingSum';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
