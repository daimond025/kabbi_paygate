<?php

use yii\db\Migration;

class m171104_213046_yandex_payment__drop_column__card_synonym extends Migration
{
    const TABLE_YANDEX_PAYMENT = '{{%yandex_payment}}';
    const COLUMN_NAME = 'cardSynonym';


    public function safeUp()
    {
        $this->dropColumn(self::TABLE_YANDEX_PAYMENT, self::COLUMN_NAME);
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_YANDEX_PAYMENT, self::COLUMN_NAME, $this->string());
    }

}
