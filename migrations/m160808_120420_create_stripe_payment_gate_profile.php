<?php

use yii\db\Migration;

/**
 * Handles the creation for table `stripe_payment_gate_profile`.
 */
class m160808_120420_create_stripe_payment_gate_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('stripe_payment_gate_profile', [
            'id' => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'publicKey' => $this->string()->notNull(),
            'privateKey' => $this->string()->notNull(),
            'commission' => $this->float()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('stripe_payment_gate_profile');
    }
}
