<?php

use yii\db\Migration;

class m180126_182936_yandex_payment_gate_profile__add_receipt_columns extends Migration
{
    const TABLE_NAME = '{{%yandex_payment_gate_profile}}';
    const COLUMN_USE_RECEIPT = 'useReceipt';
    const COLUMN_PRODUCT = 'product';
    const COLUMN_VAT = 'vat';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_USE_RECEIPT, $this->integer(1)->defaultValue(0));
        $this->addColumn(self::TABLE_NAME, self::COLUMN_PRODUCT, $this->string(128));
        $this->addColumn(self::TABLE_NAME, self::COLUMN_VAT, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_USE_RECEIPT);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_PRODUCT);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_VAT);
    }
}
