<?php

use yii\db\Migration;

class m180128_080053_payment_gate_profile__add_currency extends Migration
{
    const TABLE_NAME = '{{%payment_gate_profile}}';
    const COLUMN_NAME = 'currency';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string(3));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}
