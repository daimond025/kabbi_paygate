<?php

use yii\db\Migration;

class m171202_191158_create_table__paycom_card extends Migration
{
    const TABLE_NAME = '{{%paycom_card}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'          => $this->primaryKey(),
            'paymentName' => $this->string(32)->notNull(),
            'clientId'    => $this->string(32)->notNull(),
            'pan'         => $this->string(32)->notNull(),
            'token'       => $this->string(500)->notNull(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_paycom_card', self::TABLE_NAME, ['paymentName', 'clientId']);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
