<?php

use yii\db\Migration;

class m180616_202846_stripe_payment_gate_profile__add_column__client_id extends Migration
{
    const TABLE_NAME = '{{%stripe_payment_gate_profile}}';
    const COLUMN_CLIENT_ID = 'clientId';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_CLIENT_ID, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_CLIENT_ID);
    }
}
