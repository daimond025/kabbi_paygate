<?php

use yii\db\Migration;

class m160912_123601_create_sberbank_profile_payment_gate extends Migration
{
    public function up()
    {
        $this->createTable('sberbank_payment_gate_profile', [
            'id' => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'usernameBinding' => $this->string(32)->defaultValue(''),
            'passwordBinding' => $this->string(32)->defaultValue(''),
            'usernamePayment' => $this->string(32)->defaultValue(''),
            'passwordPayment' => $this->string(32)->defaultValue(''),
            'url' => $this->string(255)->notNull(),
            'commission' => $this->float()->defaultValue(0),
            'returnUrl' => $this->string(255)->defaultValue('finish.html'),
            'failUrl' => $this->string(255),
        ]);
        $this->execute('INSERT INTO sberbank_payment_gate_profile 
            (paymentName, usernameBinding, passwordBinding, url, commission, returnUrl, failUrl) 
            SELECT paymentName, username, password, url, commission, returnUrl, failUrl 
            FROM payment_gate_profile WHERE typeId = 2');
        $this->delete('payment_gate_profile', ['typeId' => 2]);
    }

    public function down()
    {
        $this->delete('payment_gate_profile', ['typeId' => 2]);
        $this->execute('INSERT INTO payment_gate_profile
            (paymentName, username, password, url, commission, returnUrl, failUrl, typeId)
            SELECT paymentName, usernameBinding, passwordBinding, url, commission, returnUrl, failUrl, 2
            FROM sberbank_payment_gate_profile');
        $this->dropTable('sberbank_payment_gate_profile');
    }

}
