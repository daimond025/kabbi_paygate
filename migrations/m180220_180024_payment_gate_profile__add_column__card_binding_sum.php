<?php

use yii\db\Migration;

class m180220_180024_payment_gate_profile__add_column__card_binding_sum extends Migration
{
    const TABLE_NAME = '{{%payment_gate_profile}}';
    const COLUMN_NAME = 'cardBindingSum';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
