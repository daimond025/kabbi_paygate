<?php

use yii\db\Migration;

class m170327_183944_create_yandex_payment_register extends Migration
{
    const TABLE_YANDEX_ORDER = 'yandex_payment_register';

    public function up()
    {
        $this->createTable(self::TABLE_YANDEX_ORDER, [
            'id'             => $this->primaryKey(),
            'shopId'         => $this->integer()->notNull(),
            'paymentName'    => $this->string()->notNull(),
            'customerNumber' => $this->string()->notNull(),
            'orderNumber'    => $this->string()->notNull(),
            'gootaxResult'   => "ENUM('WAITING','SUCCESSFUL','FAILED') NULL DEFAULT 'WAITING'",
            'yandexResult'   => "ENUM('WAITING','SUCCESSFUL','FAILED') NULL DEFAULT 'WAITING'",
            'createdAt'      => $this->integer(),
            'updatedAt'      => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_YANDEX_ORDER);
    }

}
