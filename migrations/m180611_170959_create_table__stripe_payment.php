<?php

use yii\db\Migration;

class m180611_170959_create_table__stripe_payment extends Migration
{
    const TABLE_NAME = '{{%stripe_payment}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'paymentName'   => $this->string(32)->notNull(),
            'clientId'      => $this->string(32)->notNull(),
            'orderNumber'   => $this->string(32)->notNull(),
            'paymentId'     => $this->string()->notNull(),
            'stripeAccount' => $this->string(),
            'amount'        => $this->integer(),
            'currency'      => $this->string(),


            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_stripe_payment', self::TABLE_NAME, ['paymentName', 'clientId']);
        $this->createIndex('idx_stripe_payment_paymentId', self::TABLE_NAME, 'paymentId');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
