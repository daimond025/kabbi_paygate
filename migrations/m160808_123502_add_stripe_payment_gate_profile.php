<?php

use yii\db\Migration;

class m160808_123502_add_stripe_payment_gate_profile extends Migration
{
    const TABLE_NAME = 'payment_gate_type';

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'id'        => 5,
            'name'      => 'Страйп',
            'classname' => 'Stripe',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['classname' => 'Stripe']);
    }
}
