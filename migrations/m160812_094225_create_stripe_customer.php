<?php

use yii\db\Migration;

/**
 * Handles the creation for table `stripe_customer`.
 */
class m160812_094225_create_stripe_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('stripe_customer', [
            'id' => $this->primaryKey(),
            'profileId' => $this->integer()->notNull(),
            'clientId' => $this->string()->notNull(),
            'stripeCustomerId' => $this->string()->notNull(),
        ]);
        $this->addForeignKey('profile_customer', 'stripe_customer', ['profileId'],
            'stripe_payment_gate_profile', ['id'], 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('stripe_customer');
    }
}
