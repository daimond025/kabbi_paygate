<?php

use yii\db\Migration;

class m160725_075457_payment_gate_profile extends Migration
{
    
    public function safeUp()
    {
        $this->alterColumn('payment_gate_profile', 'commission', $this->float()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('payment_gate_profile', 'commission', $this->smallInteger(3)->defaultValue(0));
    }
    
}
