<?php

use yii\db\Migration;

class m171220_040330_yandex_payment_gate_profile__add_audit_columns extends Migration
{
    const TABLE_NAME = '{{%yandex_payment_gate_profile}}';
    const COLUMN_CREATED_AT = 'createdAt';
    const COLUMN_UPDATED_AT = 'updatedAt';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_CREATED_AT, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COLUMN_UPDATED_AT, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_CREATED_AT);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_UPDATED_AT);
    }
}
