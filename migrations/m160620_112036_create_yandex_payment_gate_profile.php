<?php

use yii\db\Migration;

/**
 * Handles the creation for table `yandex_payment_gate_profile`.
 */
class m160620_112036_create_yandex_payment_gate_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('yandex_payment_gate_profile', [
            'id' => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'shopId' => $this->integer()->notNull(),
            'scid' => $this->integer()->notNull(),
            'password' => $this->string(255),
            'commission' => $this->float()->defaultValue(0),
            'isDebug' => $this->boolean(),
            'certificateContent' => $this->binary()->notNull(),
            'certificatePassword' => $this->string()->defaultValue(''),
            'keyContent' => $this->binary()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('yandex_payment_gate_profile');
    }
}
