<?php

use yii\db\Migration;

class m180701_120601_create_table__bepaid_profile extends Migration
{
    const TABLE_BEPAID_PROFILE = '{{%bepaid_profile}}';
    const TABLE_PAYMENT_GATE_TYPE = '{{%payment_gate_type}}';

    const TYPE_ID = 10;

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_BEPAID_PROFILE, [
            'id'          => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'shopId'      => $this->integer()->notNull(),
            'shopKey'     => $this->string()->notNull(),

            'isDebug'        => $this->integer(1)->notNull()->defaultValue(0),
            'commission'     => $this->float()->defaultValue(0),
            'cardBindingSum' => $this->integer()->notNull()->defaultValue(1),
            'currency'       => $this->string(3)->notNull(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->insert(self::TABLE_PAYMENT_GATE_TYPE, [
            'id'        => self::TYPE_ID,
            'name'      => 'BePaid',
            'classname' => 'BePaid',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_PAYMENT_GATE_TYPE, ['id' => self::TYPE_ID]);
        $this->dropTable(self::TABLE_BEPAID_PROFILE);
    }
}
