<?php

use yii\db\Migration;

class m171217_085147_create_table__paycom_payment extends Migration
{
    const TABLE_NAME = '{{%paycom_payment}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'          => $this->primaryKey(),
            'paymentName' => $this->string(32)->notNull(),
            'clientId'    => $this->string(32)->notNull(),
            'orderNumber' => $this->string(32)->notNull(),
            'token'       => $this->string(500)->notNull(),
            'receiptId'   => $this->string(500)->notNull(),
            'amount'      => $this->integer(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_paycom_payment', self::TABLE_NAME, ['paymentName', 'clientId']);
        $this->createIndex('idx_paycom_payment_receiptId', self::TABLE_NAME, 'receiptId');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
