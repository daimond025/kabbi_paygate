<?php

use yii\db\Migration;

class m160211_064201_payment_gate_type extends Migration
{
    const TABLE_NAME = 'payment_gate_type';
    
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'classname' => $this->string(60)->notNull(),
        ], $tableOptions);
        
        $this->batchInsert(self::TABLE_NAME, ['name', 'classname'], [
            ['АльфаБанк', 'AlphaBank'],
            ['Сбербанк, ВТБ', 'BasicPayment'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
