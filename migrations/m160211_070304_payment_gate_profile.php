<?php

use yii\db\Migration;

class m160211_070304_payment_gate_profile extends Migration
{
    const TABLE_NAME = 'payment_gate_profile';
    
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'typeId' => $this->integer()->notNull(),
            'username' => $this->string(32)->defaultValue(''),
            'password' => $this->string(32)->defaultValue(''),
            'url' => $this->string(255)->notNull(),
            'commission' => $this->smallInteger(3)->defaultValue(0),
            'returnUrl' => $this->string(255)->defaultValue('finish.html'),
            'failUrl' => $this->string(255),
        ], $tableOptions);
        $this->addForeignKey('payment_type', self::TABLE_NAME, ['typeId'],
            'payment_gate_type', ['id'], 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('payment_type', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
