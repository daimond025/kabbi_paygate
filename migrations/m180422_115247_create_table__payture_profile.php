<?php

use yii\db\Migration;

class m180422_115247_create_table__payture_profile extends Migration
{
    const TABLE_PAYTURE_PROFILE = '{{%payture_profile}}';
    const TABLE_PAYMENT_GATE_TYPE = '{{%payment_gate_type}}';

    const TYPE_ID = 9;

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_PAYTURE_PROFILE, [
            'id'             => $this->primaryKey(),
            'paymentName'    => $this->string(32)->unique()->notNull(),
            'key3dsAdd'      => $this->string()->notNull(),
            'keyPay'         => $this->string()->notNull(),
            'password'       => $this->string()->notNull(),
            'isDebug'        => $this->integer(1)->notNull()->defaultValue(0),
            'commission'     => $this->float()->defaultValue(0),
            'currency'       => $this->string(3)->notNull(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->insert(self::TABLE_PAYMENT_GATE_TYPE, [
            'id'        => self::TYPE_ID,
            'name'      => 'Payture',
            'classname' => 'Payture',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_PAYMENT_GATE_TYPE, ['id' => self::TYPE_ID]);
        $this->dropTable(self::TABLE_PAYTURE_PROFILE);
    }
}
