<?php

use yii\db\Migration;

class m160526_174841_payment_gate_type__update_records extends Migration
{
    const TABLE_NAME = 'payment_gate_type';
    const CLASS_BASICPAYMENT = 'BasicPayment';
    const CLASS_SBERBANK = 'SberBank';
    const CLASS_VTB = 'Vtb';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME, [
            'name'      => 'Сбербанк',
            'classname' => self::CLASS_SBERBANK,
        ], ['classname' => self::CLASS_BASICPAYMENT]);

        $this->insert(self::TABLE_NAME, [
            'name'      => 'ВТБ',
            'classname' => self::CLASS_VTB,
        ]);
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME, [
            'name'      => 'Сбербанк, ВТБ',
            'classname' => self::CLASS_BASICPAYMENT,
        ], ['classname' => self::CLASS_SBERBANK]);

        $this->delete(self::TABLE_NAME, ['classname' => self::CLASS_VTB]);
    }
}
