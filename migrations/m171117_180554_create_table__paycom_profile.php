<?php

use yii\db\Migration;

class m171117_180554_create_table__paycom_profile extends Migration
{
    const TABLE_PAYCOM_PROFILE = '{{%paycom_profile}}';
    const TABLE_PAYMENT_GATE_TYPE = '{{%payment_gate_type}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_PAYCOM_PROFILE, [
            'id'          => $this->primaryKey(),
            'paymentName' => $this->string(32)->unique()->notNull(),
            'apiId'       => $this->string(32)->defaultValue(''),
            'password'    => $this->string(32)->defaultValue(''),
            'isDebug'     => $this->integer(1)->notNull()->defaultValue(0),
            'commission'  => $this->float()->defaultValue(0),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->insert(self::TABLE_PAYMENT_GATE_TYPE, [
            'id'        => 7,
            'name'      => 'Paycom',
            'classname' => 'Paycom',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_PAYCOM_PROFILE);
        $this->delete(self::TABLE_PAYMENT_GATE_TYPE, ['classname' => 'Paycom']);
    }

}
