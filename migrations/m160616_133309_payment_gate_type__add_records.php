<?php

use yii\db\Migration;

class m160616_133309_payment_gate_type__add_records extends Migration
{
    
    const TABLE_NAME = 'payment_gate_type';
    
    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'id'        => 4,
            'name'      => 'Яндекс Касса',
            'classname' => 'YandexCashDesk',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['classname' => 'YandexCashDesk']);
    }
    
}
