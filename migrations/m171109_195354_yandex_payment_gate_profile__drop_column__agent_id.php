<?php

use yii\db\Migration;

class m171109_195354_yandex_payment_gate_profile__drop_column__agent_id extends Migration
{
    const TABLE_YANDEX_PAYMENT_GATE_PROFILE = '{{%yandex_payment_gate_profile}}';
    const COLUMN_NAME = 'agentId';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_YANDEX_PAYMENT_GATE_PROFILE, self::COLUMN_NAME);
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_YANDEX_PAYMENT_GATE_PROFILE, self::COLUMN_NAME, $this->string());
    }
}
