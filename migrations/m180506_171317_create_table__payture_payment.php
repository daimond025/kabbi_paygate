<?php

use yii\db\Migration;

class m180506_171317_create_table__payture_payment extends Migration
{
    const TABLE_PAYTURE_PAYMENT = '{{%payture_payment}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_PAYTURE_PAYMENT, [
            'id'              => $this->primaryKey(),
            'paymentName'     => $this->string(32)->notNull(),
            'clientId'        => $this->string(32)->notNull(),
            'orderNumber'     => $this->string(32)->notNull(),
            'merchantOrderId' => $this->string()->notNull(),
            'amount'          => $this->integer(),
            'currency'        => $this->integer(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('ids_payture_payment', self::TABLE_PAYTURE_PAYMENT,
            ['paymentName', 'clientId', 'orderNumber']);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_PAYTURE_PAYMENT);
    }
}
