<?php

use yii\db\Migration;

class m171104_204615_drop_table__yandex_profit_card extends Migration
{
    const TABLE_NAME = '{{%yandex_profit_card}}';
    const TABLE_INDEX = 'idx_yandex_profit_card';

    public function safeUp()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    public function safeDown()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'agentId'       => $this->string(255)->notNull(),
            'clientId'      => $this->string(255)->notNull(),
            'accountNumber' => $this->string(255)->notNull(),
            'synonym'       => $this->string(255)->notNull(),
            'pan'           => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createIndex(self::TABLE_INDEX, self::TABLE_NAME, ['agentId', 'clientId']);
    }

}
