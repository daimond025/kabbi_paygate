<?php

use yii\db\Migration;

class m170830_080212_yandex_payment extends Migration
{
    const TABLE_YANDEX_PAYMENT = '{{%yandex_payment}}';
    const INDEX_YANDEX_PAYMENT_1 = 'idx_yandex_payment_1';
    const INDEX_YANDEX_PAYMENT_2 = 'idx_yandex_payment_2';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_YANDEX_PAYMENT, [
            'id'             => $this->primaryKey(),
            'paymentName'    => $this->string()->notNull(),
            'customerNumber' => $this->string()->notNull(),
            'orderNumber'    => $this->string()->notNull(),

            'baseInvoiceId' => $this->string(),
            'destination'   => $this->string(),
            'cardSynonym'   => $this->string(),
            'sum'           => $this->decimal(12, 2),

            'shopId'        => $this->string()->notNull(),
            'shopArticleId' => $this->string(),

            'avisoInvoiceId' => $this->string(),
            'avisoSum'       => $this->decimal(12, 2),
            'avisoCurrency'  => $this->string(),

            'repeatId'          => $this->string(),
            'repeatStatus'      => $this->string(),
            'repeatError'       => $this->string(),
            'repeatTechMessage' => $this->string(),
            'repeatProcessedDT' => $this->string(),

            'cancelId'          => $this->string(),
            'cancelStatus'      => $this->string(),
            'cancelError'       => $this->string(),
            'cancelTechMessage' => $this->string(),
            'cancelProcessedDT' => $this->string(),

            'confirmId'        => $this->string(),
            'confirmStatus'    => $this->string(),
            'confirmError'     => $this->string(),
            'confirmInvoiceId' => $this->string(),
            'confirmRequestDT' => $this->string(),

            'returnId'          => $this->string(),
            'returnStatus'      => $this->string(),
            'returnError'       => $this->string(),
            'returnTechMessage' => $this->string(),
            'returnProcessedDT' => $this->string(),

            'gootaxResult' => "ENUM('WAITING','SUCCESS','FAIL') NULL DEFAULT 'WAITING'",

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(self::INDEX_YANDEX_PAYMENT_1, self::TABLE_YANDEX_PAYMENT, ['orderNumber', 'shopId']);
        $this->createIndex(self::INDEX_YANDEX_PAYMENT_2, self::TABLE_YANDEX_PAYMENT, ['paymentName', 'shopId']);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_YANDEX_PAYMENT);
    }

}
