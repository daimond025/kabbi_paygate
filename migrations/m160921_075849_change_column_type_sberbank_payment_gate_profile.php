<?php

use yii\db\Migration;

class m160921_075849_change_column_type_sberbank_payment_gate_profile extends Migration
{
    public function up()
    {
        $this->alterColumn('sberbank_payment_gate_profile', 'passwordBinding', $this->string(255)->defaultValue(''));
        $this->alterColumn('sberbank_payment_gate_profile', 'passwordPayment', $this->string(255)->defaultValue(''));
    }

    public function down()
    {
        $this->alterColumn('sberbank_payment_gate_profile', 'passwordBinding', $this->string(32)->defaultValue(''));
        $this->alterColumn('sberbank_payment_gate_profile', 'passwordPayment', $this->string(32)->defaultValue(''));
    }
    
}
