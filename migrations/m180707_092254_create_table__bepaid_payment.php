<?php

use yii\db\Migration;

class m180707_092254_create_table__bepaid_payment extends Migration
{
    const TABLE_BEPAID_PAYMENT = '{{%bepaid_payment}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_BEPAID_PAYMENT, [
            'id'          => $this->primaryKey(),
            'paymentName' => $this->string(32)->notNull(),
            'clientId'    => $this->string(32)->notNull(),
            'orderNumber' => $this->string(32)->notNull(),
            'uid'         => $this->string()->notNull(),
            'amount'      => $this->integer(),
            'currency'    => $this->integer(),

            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_bepaid_payment', self::TABLE_BEPAID_PAYMENT,
            ['paymentName', 'clientId', 'orderNumber']);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_BEPAID_PAYMENT);
    }
}
