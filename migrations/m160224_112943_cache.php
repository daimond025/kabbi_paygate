<?php

use yii\db\Migration;

class m160224_112943_cache extends Migration
{
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            $blob = 'LONGBLOB';
        } else {
            $blob = 'BLOB';
        } 
        $this->createTable('cache', [
            'id' => $this->string(128)->notNull(),
            'expire' => $this->integer(),
            'data' => $blob,
        ], $tableOptions);
        $this->addPrimaryKey('id', 'cache', ['id']);
    }

    public function safeDown()
    {
        $this->dropTable('cache');
    }
}
