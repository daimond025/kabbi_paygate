<?php

use yii\db\Migration;

class m170828_074142_yandex_payment_gate_profile__add_columns extends Migration
{
    const TABLE_NAME = '{{%yandex_payment_gate_profile}}';
    const COLUMN_SHOP_ARTICLE_ID = 'shopArticleId';
    const COLUMN_SECURE_DEAL_SHOP_ARTICLE_ID = 'secureDealShopArticleId';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SHOP_ARTICLE_ID, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SHOP_ARTICLE_ID, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SHOP_ARTICLE_ID);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_SECURE_DEAL_SHOP_ARTICLE_ID);
    }

}
