<?php

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$db = require __DIR__ . '/db.php';

$config = [
    'id'         => 'api_paygate',
    'basePath'   => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'aliases'    => [
        '@bower'       => '@vendor/bower-asset',
        '@npm'         => '@vendor/npm-asset',
        '@v0'          => '@app/modules/v0',
        '@healthCheck' => '@app/components/healthCheck',
    ],
    'bootstrap'  => ['syslog', 'log'],
    'components' => [
        // Yii2 components
        'assetManager' => [
            'appendTimestamp' => true,
        ],

        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'errorHandler' => [
            'class' => 'yii\web\ErrorHandler',
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    // 'basePath' => '@app/messages',
                    // 'sourceLanguage' => 'en-US',
                    // 'fileMap' => [
                    //  'app'       => 'app.php',
                    //     'app/error' => 'error.php',
                    // ],
                ],
            ],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error'],
                    'logFile' => '@runtime/logs/error.log',
                    'except'  => [
                        'yii\web\HttpException:404',
                    ],
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:404',
                    ],
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'request' => [
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'response' => [
            'format'       => \yii\web\Response::FORMAT_JSON,
            'on afterSend' => function ($sender) {
                $lastException = \Yii::$app->errorHandler->exception;
                if ($lastException instanceof \Exception) {
                    if (!$lastException instanceof \yii\web\HttpException) {
                        $lastException = new \yii\web\BadRequestHttpException('Internal Error', 255, $lastException);
                    }
                    \Yii::$app->syslog->sendLog($lastException);
                } else {
                    \Yii::$app->syslog->sendLog();
                }
            },
        ],

        'urlManager' => [
            'enableStrictParsing' => true,
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'rules'               => require __DIR__ . '/urlRules.php',
        ],

        // Databases
        'db'         => $db['dbMain'],

        // Other components and services
        'syslog'     => [
            'class'       => 'app\components\syslog\Syslog',
            'identity'    => getenv('SYSLOG_IDENTITY'),
            'enableDebug' => (int)getenv('YII_DEBUG') === 1,
        ],
    ],

    'modules' => [
        'v0' => [
            'class'             => 'v0\Module',
            'networkTimeout'    => getenv('MY_NETWORK_TIMEOUT'),
            'processingTimeout' => getenv('MY_PROCESSING_TIMEOUT'),
            'enableDebug'       => (int)getenv('YII_DEBUG') === 1,
        ],
    ],

    'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];

    $config['components']['log']['targets'][] = [
        'class'   => 'yii\log\FileTarget',
        'levels'  => ['warning'],
        'logFile' => '@runtime/logs/warning.log',
    ];
    $config['components']['log']['targets'][] = [
        'class'   => 'yii\log\FileTarget',
        'levels'  => ['info', 'trace', 'profile'],
        'logFile' => '@runtime/logs/other.log',
    ];
}

return $config;
