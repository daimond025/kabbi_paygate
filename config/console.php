<?php

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

\Yii::setAlias('@tests', __DIR__ . '/../tests/codeception');

$db = require __DIR__ . '/db.php';

$config = [
    'id'                  => 'basic-console',
    'basePath'            => dirname(__DIR__),
    'vendorPath'          => dirname(__DIR__) . '/vendor',
    'aliases'             => [
        '@v0'     => '@app/modules/v0',
        '@yandex' => '@v0/modules/yandex',
        '@bower'  => '@vendor/bower-asset',
        '@npm'    => '@vendor/npm-asset',
    ],
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'app\commands',
    'components'          => [
        // Yii2 components
        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'db' => $db['dbMain'],

        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:404',
                    ],
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        // Other components and services
        'syslog' => [
            'class'    => 'app\components\syslog\Syslog',
            'identity' => getenv('SYSLOG_IDENTITY'),
        ],
    ],

    'modules' => [
        'v0' => [
            'class'             => 'v0\Module',
            'networkTimeout'    => getenv('MY_NETWORK_TIMEOUT'),
            'processingTimeout' => getenv('MY_PROCESSING_TIMEOUT'),
        ],
    ],

    'on closeConnection' => function () {
        $syslog = \Yii::$app->get('syslog', false);
        if ($syslog instanceof \app\components\syslog\Syslog) {
            $syslog->reset();
        }

        $db = \Yii::$app->get('db', false);
        if ($db instanceof \yii\db\Connection) {
            $db->close();
        }

        $mailerLog = \Yii::$app->get('mailer', false);
        if ($mailerLog instanceof \yii\swiftmailer\Mailer) {
            $mailerLog->getTransport()->stop();
        }
    },

    'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
