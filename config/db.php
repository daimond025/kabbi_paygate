<?php

return [
    'dbMain'     => [
        'class'               => 'yii\db\Connection',
        'charset'             => 'utf8',
        'dsn'                 => getenv('DB_MAIN_DSN'),
        'username'            => getenv('DB_MAIN_USERNAME'),
        'password'            => getenv('DB_MAIN_PASSWORD'),
        'enableSchemaCache'   => getenv('DB_MAIN_SCHEMA_CACHE_ENABLE') ? true : false,
        'schemaCacheDuration' => (int)getenv('DB_MAIN_SCHEMA_CACHE_DURATION'),

    ],

    'dbTest'     => [
        'class'    => 'yii\db\Connection',
        'charset'  => 'utf8',
        'dsn'      => getenv('DB_MAIN_TEST_DSN'),
        'username' => getenv('DB_MAIN_TEST_USERNAME'),
        'password' => getenv('DB_MAIN_TEST_PASSWORD'),
    ],
    'redisCache' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_MAIN'),
    ],


];