<?php

return [
    'rabbitMQ.host'        => getenv('RABBITMQ_MAIN_HOST'),
    'rabbitMQ.virtualHost' => getenv('RABBITMQ_MAIN_VIRTUAL_HOST'),
    'rabbitMQ.port'        => getenv('RABBITMQ_MAIN_PORT'),
    'rabbitMQ.user'        => getenv('RABBITMQ_MAIN_USERNAME'),
    'rabbitMQ.password'    => getenv('RABBITMQ_MAIN_PASSWORD'),

    'apiCurrency.url' => getenv('API_CURRENCY_URL'),
    'my.externalUrl'  => getenv('MY_EXTERNAL_URL'),

    'version' => '1.13.2',
];
