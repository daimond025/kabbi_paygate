<?php

return [
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => ['v0/profile'],
        'only'          => ['create', 'update', 'delete', 'index', 'view'],
        'extraPatterns' => [
            'POST'                 => 'create',
            'PUT {paymentName}'    => 'update',
            'DELETE {paymentName}' => 'delete',
            'GET {paymentName}'    => 'view',
            'GET'                  => 'index',
        ],
        'tokens'        => ['{paymentName}' => '<paymentName:[\\w\\d]*>'],
    ],
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => ['v0/card'],
        'only'          => ['index', 'create', 'delete', 'check'],
        'extraPatterns' => [
            'POST {paymentName}/{clientId}'         => 'create',
            'DELETE {paymentName}/{clientId}/{pan}' => 'delete',
            'GET {paymentName}/{clientId}'          => 'index',
            'PUT {paymentName}/{clientId}'          => 'check',
        ],
        'tokens'        => [
            '{paymentName}' => '<paymentName:[\\w\\d]*>',
            '{clientId}'    => '<clientId:[\\w\\d]*>',
            '{pan}'         => '<pan:[-|*\\w\\d]*>',
        ],
    ],
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => ['v0/payment'],
        'only'          => ['deposit', 'refund', 'status'],
        'extraPatterns' => [
            'POST {paymentName}/{clientId}'           => 'deposit',
            'POST {paymentName}/{clientId}/{orderId}' => 'refund',
            'GET {paymentName}/{clientId}/{orderId}'  => 'status',
        ],
        'tokens'        => [
            '{paymentName}' => '<paymentName:[\\w\\d]*>',
            '{clientId}'    => '<clientId:[\\w\\d]*>',
            '{orderId}'     => '<orderId:[-\\w\\d]*>',
        ],
    ],

    'v0/yandex/order/failed' => 'v0/yandex/order/get-failed',
    'v0/yandex/order/check'  => 'v0/yandex/order/check',
    'v0/yandex/order/aviso'  => 'v0/yandex/order/aviso',

    'GET v0/stripe/account/authorize'                                           => 'v0/stripe/account/authorize',
    'POST v0/stripe/<paymentName:[\\w\\d]+>/account'                            => 'v0/stripe/account/get-authorize-url',
    'DELETE v0/stripe/<paymentName:[\\w\\d]+>/account/<accountId:[\\w\\d]+>'    => 'v0/stripe/account/de-authorize',
    'v0/stripe/<paymentName:[\\w\\d]+>/<clientId:[\\w\\d]+>/<locale:[\\w\\d]+>' => 'v0/stripe/registry/card',
    'v0/stripe/success/<locale:[\\w\\d]+>'                                      => 'v0/stripe/registry/success',
    'v0/stripe/fail/<locale:[\\w\\d]+>'                                         => 'v0/stripe/registry/fail',

    'v0/paycom/card/<paymentName:[\\w\\d]+>/<clientId:[\\w\\d]+>' => 'v0/paycom/card/index',
    'v0/paycom/card/save'                                         => 'v0/paycom/card/save',
    'v0/paycom/card/success'                                      => 'v0/paycom/card/success',
    'v0/paycom/card/fail'                                         => 'v0/paycom/card/fail',

    'v0/kkb/card/index'      => 'v0/kkb/kkb/card-index',
    'v0/kkb/card/success'    => 'v0/kkb/kkb/card-success',
    'v0/kkb/card/fail'       => 'v0/kkb/kkb/card-fail',
    'v0/kkb/payment/success' => 'v0/kkb/kkb/payment-success',

    'bepaid/card/register/<paymentName:[\\w\\d]+>/<clientId:[\\w\\d]+>' => 'v0/bepaid/card/register',
    'bepaid/card/success/<locale:[\\w\\d]+>'                            => 'v0/bepaid/card/success',
    'bepaid/card/fail/<locale:[\\w\\d]+>'                               => 'v0/bepaid/card/fail',

    'version' => '/api/version',
    'status'  => '/api/status',
];