<?php
$db = require __DIR__ . '/db.php';
return [
    'id'         => 'api_paygate_test',
    'aliases'    => [
        '@bower'  => '@vendor/bower-asset',
        '@npm'    => '@vendor/npm-asset',
        '@v0'     => '@app/modules/v0',
        '@yandex' => '@v0/modules/yandex',
    ],
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => ['syslog', 'log'],
    'components' => [
        // Yii2 components
        'cache' => [
            'class'      => 'yii\caching\DbCache',
            'cacheTable' => 'cache',
        ],

        'errorHandler' => [
            'class' => 'yii\web\ErrorHandler',
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error'],
                    'logFile' => '@runtime/logs/error.log',
                    'except'  => [
                        'yii\web\HttpException:404',
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],

        'request' => [
            'cookieValidationKey'  => 'test',
            'enableCsrfValidation' => false,
            'parsers'              => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON,
        ],

        'urlManager' => [
            'enableStrictParsing' => true,
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'rules'               => require __DIR__ . '/urlRules.php',
        ],

        // Databases
        'db'         => $db['dbTest'],

        // Other components and services
        'syslog'     => [
            'class'    => 'app\components\syslog\Syslog',
            'identity' => getenv('SYSLOG_IDENTITY'),
            'enable'   => false,
        ],
    ],

    'modules' => [
        'v0' => [
            'class'             => 'v0\Module',
            'networkTimeout'    => getenv('MY_NETWORK_TIMEOUT'),
            'processingTimeout' => getenv('MY_PROCESSING_TIMEOUT'),
        ],
    ],

    'params' => require __DIR__ . '/params.php',
];