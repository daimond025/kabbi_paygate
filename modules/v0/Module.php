<?php

namespace v0;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * v0 module definition class
 */
class Module extends \yii\base\Module
{

    const NAME = 'v0';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'v0\controllers';

    /**
     * Timeout of a request curl
     * @var integer
     */
    public $networkTimeout = 10;

    /**
     * Max time of request processing
     * @var int
     */
    public $processingTimeout = 30;

    /**
     * @var boolean Enable debug mode
     */
    public $enableDebug;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->components = [
            'payment'          => 'v0\components\payment\Gate',
            'curl'             => [
                'class'   => 'v0\components\curl\Curl',
                'timeout' => $this->networkTimeout,
            ],
            'converter'        => 'v0\components\converter\ConverterInterface',
            'currency_service' => function () {
                $url = \Yii::$app->params['apiCurrency.url'];

                return new \v0\components\currency\CurrencyService($url);
            },
        ];
        $this->params = [
            'cache' => [
                'expires' => [
                    'card' => 1200, // 20 minutes
                ],
            ],
            'card'  => [
                'defaultAmount' => 100,
                'description'   => 'Test order for add new card',
                'prefix'        => [
                    'replicate' => 'replicate_',
                ],
            ],
        ];
        $this->aliases = [
            '@yandex'   => '@v0/modules/yandex',
            '@sberbank' => '@v0/modules/sberbank',
            '@stripe'   => '@v0/modules/stripe',
            '@paycom'   => '@v0/modules/paycom',
            '@kkb'      => '@v0/modules/kkb',
            '@payture'  => '@v0/modules/payture',
            '@bepaid'   => '@v0/modules/bepaid',
        ];

        $this->modules = [
            'yandex'   => [
                'class'             => 'yandex\Module',
                'networkTimeout'    => $this->networkTimeout,
                'processingTimeout' => $this->processingTimeout,
                'externalUrl'       => \Yii::$app->params['my.externalUrl'],
                'syslog'            => $this->get('syslog'),
                'enableDebug'       => $this->enableDebug,
            ],
            'sberbank' => 'sberbank\Module',
            'stripe'   => 'stripe\Module',
            'paycom'   => [
                'class'          => 'paycom\Module',
                'networkTimeout' => $this->networkTimeout,
                'syslog'         => $this->get('syslog'),
            ],
            'kkb'      => [
                'class'          => 'kkb\Module',
                'networkTimeout' => $this->networkTimeout,
                'externalUrl'    => \Yii::$app->params['my.externalUrl'],
                'syslog'         => $this->get('syslog'),
                'enableDebug'    => $this->enableDebug,
            ],
            'payture'  => [
                'class'          => 'payture\Module',
                'networkTimeout' => $this->networkTimeout,
                'syslog'         => $this->get('syslog'),
                'userIp'         => $this->getUserIp(),
            ],
            'bepaid'   => [
                'class'           => 'bepaid\Module',
                'networkTimeout'  => $this->networkTimeout,
                'syslog'          => $this->get('syslog'),
                'currencyService' => $this->get('currency_service'),
                'url'             => \Yii::$app->params['my.externalUrl'],
            ],
        ];

        \Yii::$container->set('v0\components\converter\cbr\Cbr', 'v0\components\converter\cbr\CachedCbr');
        \Yii::$container->set('v0\components\converter\ConverterInterface', 'v0\components\converter\cbr\CbrConverter');
    }

    public function beforeAction($action)
    {
        /* @var $response \yii\web\Response */
        $response = Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_JSON;
        $response->charset = 'UTF-8';

        return parent::beforeAction($action);
    }

    public static function getComponent($id, $throwException = true)
    {
        if (Yii::$app->hasModule(self::NAME)) {
            $module = Yii::$app->getModule(self::NAME);
            return $module->get($id, $throwException);
        } else {
            Yii::error('No module: ' . self::NAME, __METHOD__);
            throw new Exception('No module: ' . self::NAME);
        }
    }

    public static function setComponent($id, $definition)
    {
        if (Yii::$app->hasModule(self::NAME)) {
            $module = Yii::$app->getModule(self::NAME);

            return $module->set($id, $definition);
        } else {
            Yii::error('No module: ' . self::NAME, __METHOD__);
            throw new Exception('No module: ' . self::NAME);
        }
    }

    public static function getParam($key)
    {
        if (\Yii::$app->hasModule(self::NAME)) {
            $module = \Yii::$app->getModule(self::NAME);
            $param = ArrayHelper::getValue($module->params, $key);
            if (is_null($param)) {
                $param = ArrayHelper::getValue($module->module->params, $key);
            }

            return $param;
        }
    }

    public function get($id, $throwException = true)
    {
        if ($this->has($id)) {
            return parent::get($id);
        } else {
            // parent module
            return $this->module->get($id, $throwException);
        }
    }

    /**
     * @return string
     */
    private function getUserIp()
    {
        return isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : '';
    }
}
