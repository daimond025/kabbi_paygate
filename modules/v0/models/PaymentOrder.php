<?php

namespace v0\models;

use Yii;
use v0\Module as V0;
use yii\base\Exception;
use yii\base\Model;
use app\components\syslog\Syslog;
use app\components\exception\ServiceErrorHttpException;
use app\components\exception\CardErrorHttpException;

class PaymentOrder extends Model
{
    public $amount;

    public $orderNumber;

    public $currency;

    public $params;

    public $description;

    public $destination;

    protected $_card;

    public function rules()
    {
        return [
            [['pan'], 'required', 'on' => 'deposit'],
            [['amount', 'orderNumber'], 'required'],
            [
                ['amount'],
                'integer',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'min'      => 100,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
            [['pan', 'orderNumber'], 'string', 'max' => 20, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['currency'], 'default', 'value' => 643],
            [
                ['currency'],
                'integer',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 999,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 1,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
            [['params'], 'safe'],
            [['description'], 'string', 'max' => 255, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['destination'], 'string'],
        ];
    }

    public function fields()
    {
        return [
            'pan',
            'amount',
            'orderNumber',
            'currency',
            'params',
            'description',
            'destination',
            'jsonParams' => 'params',
        ];
    }

    public function attributeLabels()
    {
        return [
            'pan'         => Yii::t('app', 'pan'),
            'amount'      => Yii::t('app', 'amount'),
            'orderNumber' => Yii::t('app', 'orderNumber'),
            'currency'    => Yii::t('app', 'currency'),
            'params'      => Yii::t('app', 'params'),
            'description' => Yii::t('app', 'description'),
            'destination' => Yii::t('app', 'destination'),
        ];
    }

    public function getPan()
    {
        if ($this->_card) {
            return $this->_card->pan;
        } else {
            return '';
        }
    }

    public function setPan($value)
    {
        if ($this->_card) {
            $this->_card->pan = $value;
        } else {
            $this->_card = new Card([
                'pan' => $value,
            ]);
        }
    }

    public function deposit()
    {
        /* @var $paymentGate \v0\components\payment\types\Payment */
        $paymentGate = V0::getComponent('payment');

        if (($binding = $this->_card->getBinding()) === false) {
            throw new CardErrorHttpException('Not found card');
        }


        $dataOrder = $paymentGate->register($this, $binding);

        $orderId = $dataOrder['order'];
        $paymentGate->payment($orderId, $binding);

        return $orderId;
    }

    /**
     * @param string $orderId
     *
     * @return bool
     * @throws ServiceErrorHttpException
     * @throws Exception
     */
    public static function refund($orderId)
    {
        /* @var $paymentGate \v0\components\payment\types\Payment */
        $paymentGate = V0::getComponent('payment');
        $status = $paymentGate->status($orderId);

        if ($status['actionCode'] === 0) {
            return $paymentGate->refund($orderId, $status['amount']);
        }

        throw new ServiceErrorHttpException('Unable to refund payment (incorrect payment status)');
    }

    public static function status($orderId)
    {
        /* @var $paymentGate \v0\components\payment\types\Payment */
        $paymentGate = V0::getComponent('payment');

        return $paymentGate->status($orderId);
    }
}