<?php

namespace v0\models;

use Yii;
use v0\Module as V0;
use yii\base\Object;
use app\components\exception\InvalidArgumentException;
use app\components\exception\CardErrorHttpException;
use app\components\exception\NotFoundHttpException;
use app\components\exception\ServiceErrorHttpException;

class Card extends Object
{
    public $pan;

    private $_binding;

    /**
     * @param integer
     * @return array Fields id and url
     */
    public static function add($currency = null)
    {
        $paymentOrder = new PaymentOrder([
            'orderNumber' => V0::getParam('card.prefix.replicate') . V0::getComponent('security')->generateRandomString(8),
            'amount' => V0::getParam('card.defaultAmount'),
            'description' => V0::getParam('card.description'),
            'currency' => $currency,
        ]);


        $payment = V0::getComponent('payment')->register($paymentOrder);

        // add temp card in cache 
        $id = V0::getComponent('security')->generateRandomString(32);

        V0::getComponent('cache')
            ->set($id, $payment['order'], V0::getParam('cache.expires.card'));
        return [
            'orderId' => $id,
            'url' => $payment['url']
        ];
    }

    public static function check($pan)
    {
        try {
            $card = new Card(['pan' => $pan]);
            $card->getBinding();

            return  ($card->_binding) ?  true :  false;  // ['check' => true] : ['check' => false];

        } catch (\Exception $ignore) {}
    }

    public static function check_transaction($id)
    {
        // get cache data for add new card
        $cache = V0::getComponent('cache');
        if (($order = $cache->get($id)) === false) {
           // throw new NotFoundHttpException("orderId='$id' don't exists on cache");
        }


        $paymentGate = V0::getComponent('payment');
        $paymentGate->setUseBindingProfile();

        $status = $paymentGate->status($order);
        if ($status['actionCode'] !== 0) {
            throw new ServiceErrorHttpException('It wasn\'t succeeded to make payment');
        }

        // if get binding_id then reject order in payment gateway
        $amount = isset($status['amount']) ? $status['amount'] : V0::getParam('card.defaultAmount');
        $paymentGate->refund($order, $amount);
        if ($status['client_id'] && $status['client_id'] != $paymentGate->getClientId()) {
            throw new InvalidArgumentException('clientId', $status['client_id'], 'no equ clientId on a bank');
        }

        if ($status['binding_id']) {
            return true;
        } else {
            throw new CardErrorHttpException(Yii::t('app/error', 'Card parameters are incorrectly entered'));
        }
    }

    public static function listCard()
    {
        try {
            $cards = V0::getComponent('payment')->bindings();
            array_walk($cards, function (&$item) {
                $item = key_exists('pan', $item) ? $item['pan'] : $item;
            });
            return $cards;
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
            return array();
        }
    }
    /**
     *
     * @param string $pan Masked pan
     * @throws CardException
     * @return boolean True - success
     */
    public static function unbind($pan)
    {
        try {
            $card = new Card(['pan' => $pan]);
            if ($card->getBinding()) {
                V0::getComponent('payment')->unbind($card->getBinding());
            }
        } catch (\Exception $ignore) {}
    }

    public function getBinding()
    {
        if (is_null($this->_binding)) {
            try {
                $pan = $this->pan;
                $card = array_filter(
                    V0::getComponent('payment')->bindings(),
                    function ($item) use ($pan) {
                        return key_exists('pan', $item) && $item['pan'] == $pan;
                    }
                );
            } catch (\Exception $e) {
                throw new CardErrorHttpException('binding', $e);
            }
            $card = array_shift($card);
            if (empty($card)) {
                $this->_binding = false;
            } else {
                $this->_binding = $card['binding'];
            }
        }
        return $this->_binding;
    }
}