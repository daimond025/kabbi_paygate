<?php

namespace v0\models;

use Yii;
use v0\Module as V0;
use app\components\syslog\Syslog;
use app\components\exception\InvalidArgumentException;

/**
 * This is the model class for table "payment_gate_profile".
 *
 * @property integer                    $id
 * @property string                     $paymentName
 * @property integer                    $typeId
 * @property string                     $username
 * @property string                     $password
 * @property string                     $url
 * @property float                      $commission
 * @property string                     $returnUrl
 * @property string                     $failUrl
 * @property string                     $cardBindingSum
 * @property string                     $currency
 *
 * @property \v0\models\PaymentGateType $type
 */
class ProfileDAO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_gate_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'typeId'], 'required'],
            [['typeId', 'cardBindingSum'], 'integer', 'message' => Syslog::YII_MESSAGE_TO_INTEGER],
            [['paymentName'], 'string', 'max' => 32, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['username', 'password'], 'string', 'max' => 32],
            [['url', 'returnUrl', 'failUrl'], 'string', 'max' => 255, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['paymentName'], 'unique', 'message' => Syslog::YII_MESSAGE_TO_UNIQUE],
            [['commission'], 'default', 'value' => 0],
            [['returnUrl'], 'default', 'value' => 'payment_success_ru.html'],
            [['failUrl'], 'default', 'value' => 'errors_ru.html'],
            [['cardBindingSum'], 'default', 'value' => 1],
            ['currency', 'string', 'length' => 3],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
            [
                ['typeId'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PaymentGateType::className(),
                'targetAttribute' => ['typeId' => 'id'],
                'message'         => Syslog::YII_MESSAGE_TO_EXISTS,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'paymentName'    => Yii::t('app', 'paymentName'),
            'typeId'         => Yii::t('app', 'typeId'),
            'username'       => Yii::t('app', 'username'),
            'password'       => Yii::t('app', 'password'),
            'url'            => Yii::t('app', 'url'),
            'commission'     => Yii::t('app', 'commission'),
            'returnUrl'      => Yii::t('app', 'returnUrl'),
            'failUrl'        => Yii::t('app', 'failUrl'),
            'cardBindingSum' => Yii::t('app', 'cardBindingSum'),
            'currency'       => Yii::t('app', 'currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'username',
            'url',
            'typeId',
            'commission',
            'returnUrl',
            'failUrl',
            'cardBindingSum',
            'currency',
        ];
    }

}