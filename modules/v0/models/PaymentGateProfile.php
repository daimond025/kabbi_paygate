<?php

namespace v0\models;

use Yii;
use v0\Module as V0;

class PaymentGateProfile extends ProfileDAO
{

    public static function getNewPaymentGateProfile($type)
    {
        foreach (self::getAllPaymentGateProfile() as $models) {
            if ($type == $models->typeId) {
                return $models;
            }
        }

        return new PaymentGateProfile();
    }

    private static function getAllPaymentGateProfile()
    {
        return [
            V0::getInstance()->getModule('yandex')->get('paymentGateProfile'),
            V0::getInstance()->getModule('sberbank')->get('paymentGateProfile'),
            V0::getInstance()->getModule('stripe')->get('paymentGateProfile'),
            V0::getInstance()->getModule('paycom')->get('paymentGateProfile'),
            V0::getInstance()->getModule('kkb')->get('paymentGateProfile'),
            V0::getInstance()->getModule('payture')->get('paymentGateProfile'),
            V0::getInstance()->getModule('bepaid')->get('paymentGateProfile'),
            new ProfileDAO(),
        ];
    }

    public static function findOne($condition)
    {
        $models = self::getAllPaymentGateProfile();
        foreach ($models as $m) {
            $result = $m->findOne($condition);
            if (isset($result)) {
                return $result;
            }
        }
    }

}
