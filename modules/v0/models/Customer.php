<?php

namespace v0\models;

use Yii;
use yii\base\Model;
use app\components\syslog\Syslog;

class Customer extends Model
{
    const CLIENT_TYPE_WORKER = 'WORKER';
    const CLIENT_TYPE_CLIENT = 'CLIENT';

    public $clientId;

    public function rules()
    {
        return [
            [['clientId'], 'required'],
            [
                ['clientId'],
                'string',
                'max'     => 32,
                'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'clientId' => Yii::t('app', 'clientId'),
        ];
    }

    /**
     * @return string
     */
    public function getClientType()
    {
        if (mb_stripos($this->clientId, 'worker') !== false) {
            return self::CLIENT_TYPE_WORKER;
        }

        return self::CLIENT_TYPE_CLIENT;
    }
}