<?php

namespace v0\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $code
 * @property integer $oldCode
 * @property string $description
 */
class Currency extends \yii\db\ActiveRecord
{
    const CODE_RUB = 643;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'oldCode'], 'required'],
            [['code' ,'oldCode'], 'integer', 'message' => "{attribute}='{value}' must be an integer"],
            [
                ['description'],
                'string',
                'max' => 5,
                'tooLong' => "{attribute}='{value}' should contain at most {max, number} {max, plural, one{character} other{characters}}"
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'code'),
            'oldCode' => Yii::t('app', 'oldCode'),
            'description' => Yii::t('app', 'description'),
        ];
    }
}
