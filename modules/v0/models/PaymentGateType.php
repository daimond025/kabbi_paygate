<?php

namespace v0\models;

use Yii;

/**
 * This is the model class for table "payment_gate_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $classname
 *
 * @property PaymentGateProfile[] $paymentGateProfiles
 */
class PaymentGateType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_gate_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'classname'], 'required'],
            [['classname'], 'string', 'max' => 60],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'name' => Yii::t('app', 'Name'),
            'classname' => Yii::t('app', 'classname'),
        ];
    }
    
    public function fields()
    {
        return [
            'id',
            'name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentGateProfiles()
    {
        return $this->hasMany(PaymentGateProfile::className(), ['typeId' => 'id']);
    }
}
