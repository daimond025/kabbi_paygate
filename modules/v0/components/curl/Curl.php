<?php
namespace v0\components\curl;

use linslin\yii2\curl\Curl as Yii2Curl;
use app\components\exception\ServerErrorHttpException;

class Curl extends \yii\base\Object
{
    /**
     * only URL without get param
     * @var string
     */
    public $url;
    
    public $data = [];
    
    public $timeout = 10;
    
    private $_curl;
    
    public function init()
    {
        parent::init();
        $this->_curl = new Yii2Curl();
    }

    /**
     * @return string JsonObject
     * @throws \app\components\exception\UnknowException
     */
    public function get()
    {
        try {
            $result = $this->prepare()->get($this->url . '?' . $this->getData());
            
            \Yii::info($result, 'request');
            $this->addDebugLog();
            
            return $result;
        } catch (\Exception $e) {
            throw new ServerErrorHttpException('Internal error', $e);
        }
    }
    
    /**
     * @return string JsonObject
     * @throws \app\components\exception\UnknowException
     */
    public function post()
    {
        try {
            $result = $this->prepare()
                ->setOption(CURLOPT_POSTFIELDS, $this->getData())
                ->post($this->url);
            
            \Yii::info($result, 'request');
            $this->addDebugLog();
            
            return $result;
        } catch (\Exception $e) {
            throw new ServerErrorHttpException('Internal error', $e);
        }
    }
    
    /**
     * @return \linslin\yii2\curl\Curl
     */
    protected function prepare() 
    {
        return $this->_curl
            ->reset()
            ->setOption(CURLOPT_TIMEOUT, $this->timeout)// timeout  
            ->setOption(CURLOPT_FOLLOWLOCATION, true)   // folow redirect 
            ->setOption(CURLOPT_MAXREDIRS, 10)          // max redirect 10
            ->setOption(CURLOPT_RETURNTRANSFER, true)   // get result 
            ->setOption(CURLOPT_COOKIEJAR, '-');        // use session cookie (Set-Cookie: ... this save)
    }
    
    /**
     * @return string
     */
    protected function getData()
    {
        return http_build_query($this->data);
    }
    
    public function getLastContent()
    {
        return $this->_curl->response;
    }
    
    protected function addDebugLog()
    {
        try {
            /* @var $logger \v0\components\syslog\Syslog */
            $logger = \v0\Module::getComponent('syslog');
            
            $message = sprintf('%s %s -> %s', $this->url, 
                    json_encode($this->data), $this->getLastContent());
            $logger->addDebugLog($message);
        } catch (\yii\base\Exception $ignore) {}
    }
}