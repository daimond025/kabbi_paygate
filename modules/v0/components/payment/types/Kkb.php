<?php

namespace v0\components\payment\types;

use kkb\components\KkbService;
use v0\models\PaymentOrder;
use v0\Module as V0;
use v0\models\Customer;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Object;
use v0\models\Locale;

/**
 * Class Kkb
 * @package v0\components\payment\types
 */
class Kkb extends Object implements Payment
{
    use Locale;

    /**
     * @var string
     */
    public $paymentName;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @var KkbService
     */
    public $kkbService;


    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $module = V0::getInstance()->getModule('kkb');

        $this->kkbService = $module->get('kkbService');
        $this->kkbService->setProfile($this->paymentName);
    }

    /**
     * Registering and approval amount with client's card
     *
     * @param PaymentOrder $paymentOrder Order number in our system
     * @param string $bindingId
     *
     * @return array Field order and url
     *
     * @throws InvalidParamException
     */
    public function register($paymentOrder, $bindingId = null)
    {
        if ($bindingId) {
            $orderId = $this->kkbService->repeatPayment($this->customer->clientId, $paymentOrder->orderNumber,
                $bindingId, $paymentOrder->amount, $paymentOrder->currency);

            return ['url' => null, 'order' => $orderId];
        }

        return $this->kkbService->getRegisterCardUrlAndOrder($this->customer,
            $this->kkbService->filterLocale($this->locale));
    }

    /**
     * Finished pay current order on this amount
     *
     * @param string $order
     * @param integer $amount
     *
     * @return boolean True - success
     */
    public function deposit($order, $amount)
    {
    }

    /**
     * Cancel all amount for current order
     *
     * @param string $order
     *
     * @return boolean True - success
     */
    public function refund($order, $amount)
    {
        return $this->kkbService->refundPayment($order);
    }

    /**
     * Get status order
     *
     * @param string $order
     *
     * @return array Fields pan, client_id, binding_id, amount, actionCode and currency
     */
    public function status($order)
    {
        $payment = $this->kkbService->getPaymentInfo($order);

        return [
            'pan'        => $payment['pan'],
            'amount'     => $payment['amount'],
            'actionCode' => in_array((string)$payment['status'], ['0', '1'], true) ? 0 : 1,
            'currency'   => $payment['currency'],
            'client_id'  => $payment['clientId'],
            'binding_id' => $payment['pan'],
        ];
    }

    /**
     * @return array Array include arrays with field binding and pan
     */
    public function bindings()
    {
        return $this->kkbService->getCards($this->customer->clientId);
    }

    /**
     *
     * @param string $bindingId
     *
     * @return boolean True - success
     */
    public function unbind($bindingId)
    {
        $this->kkbService->removeCard($this->customer->clientId, $bindingId);
    }

    /**
     * @param string $order
     * @param string $binding
     *
     * @return boolean True - success
     */
    public function payment($order, $binding)
    {
    }
}