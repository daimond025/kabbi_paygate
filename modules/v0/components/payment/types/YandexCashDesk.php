<?php

namespace v0\components\payment\types;

use v0\models\Customer;
use v0\Module as V0;
use v0\models\Locale;

class YandexCashDesk extends \yii\base\Object implements Payment
{
    use Locale;

    // user on payment gateway
    public $username;

    // password on payment gateway
    public $password;

    // return on this url on success pay
    public $returnUrl = 'finish.html';

    // return on this url on failed pay
    public $failUrl;

    // url for method
    public $url;


    /**
     * @var Customer
     */
    public $customer;

    public $cardBindingSum;

    /**
     * @var \yandex\components\yandex\YandexCashDesk
     */
    private $_payment;

    public function __construct($config = [])
    {
        $this->_payment = V0::getInstance()->getModule('yandex')->get('payment');
        parent::__construct($config);
    }

    public function setPaymentName($value)
    {
        $this->_payment->setProfile($value);
    }

    public function getPaymentName()
    {
        return $this->_payment->getProfile()->paymentName;
    }

    public function register($paymentOrder, $bindingId = null)
    {
        $paymentOrder->amount /= 100;
        if ($bindingId) {
            $orderId = $this->_payment->repeatCardPayment($this->customer, $paymentOrder->orderNumber, $bindingId,
                $paymentOrder->amount, $paymentOrder->destination, $paymentOrder->params);

            return [
                'url'   => '',
                'order' => $orderId,
            ];
        } else {
            $url = $this->_payment->registerOrder($this->customer, $paymentOrder->orderNumber, $this->cardBindingSum);

            return [
                'url'   => $url,
                'order' => $paymentOrder->orderNumber,
            ];
        }
    }

    public function deposit($order, $amount)
    {
        // TODO not use
    }

    public function refund($order, $amount)
    {
        $data = $this->_payment->statusOrder($this->customer, $order);
        $this->_payment->returnPayment($data['invoiceId'], $amount / 100, $data['orderSumCurrencyPaycash']);
    }

    public function status($order)
    {
        $data = $this->_payment->statusOrder($this->customer, $order);
        if ($data) {
            return [
                'pan'        => $order,
                'client_id'  => $data['customerNumber'],
                'binding_id' => $data['invoiceId'],
                'amount'     => $data['orderSumAmount'],
                'actionCode' => $data['paid'] == 'true' ? 0 : 116,
                'currency'   => $data['orderSumCurrencyPaycash'],
            ];
        } else {
            return [
                'pan'        => null,
                'amount'     => null,
                'actionCode' => null,
                'currency'   => null,
            ];
        }
    }

    public function bindings()
    {
        $bindingManager = V0::getInstance()->getModule('yandex')->get('bindingManager');
        $bindings = $bindingManager->getBindings($this->getShopId(), $this->customer->clientId);

        foreach ($bindings as $key => $oneBinding) {
            $bindings[$key] = [
                'pan'     => $oneBinding['pan'],
                'binding' => $oneBinding['invoiceId'],
            ];
        }

        return $bindings;
    }

    public function unbind($bindingId)
    {
        $bindingManager = V0::getInstance()->getModule('yandex')->get('bindingManager');
        $bindingManager->deleteBinding($this->getShopId(), $bindingId);
    }

    public function payment($order, $binding)
    {
        // TODO dont execute
    }

    /**
     * @return int
     */
    private function getShopId()
    {
        $profile = $this->_payment->getProfile();

        return (bool)$profile->secureDeal && $this->customer->getClientType() !== Customer::CLIENT_TYPE_WORKER
            ? $profile->secureDealShopId : $profile->shopId;
    }
}
