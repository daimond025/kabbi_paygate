<?php

namespace v0\components\payment\types;

use v0\models\Customer;
use V0\Module as V0;
use v0\models\Currency;
use v0\models\Locale;

/**
 * Сбербанк
 * @package v0\components\payment\types
 */
class SberBank extends \yii\base\Object implements Payment
{
    use Locale;

    // return on this url on success pay
    public $returnUrl = 'finish.html';

    // return on this url on failed pay
    public $failUrl;

    // url for method
    public $url;

    /**
     * @var Customer
     */
    public $customer;

    public $isBinding = false;

    /**
     * @var \sberbank\components\payment\Sberbank
     */
    private $_payment;

    public function __construct($config = [])
    {
        $this->_payment = V0::getInstance()->getModule('sberbank')->get('payment');
        parent::__construct($config);
    }

    public function setPaymentName($value)
    {
        $this->_payment->setProfile($value);
    }

    public function getPaymentName()
    {
        return $this->_payment->getProfile()->paymentName;
    }

    public function register($paymentOrder, $bindingId = null)
    {
        if ($paymentOrder->validate(['currency']) &&
            $paymentOrder->currency != Currency::CODE_RUB
        ) {
            $converter = V0::getComponent('converter');
            $paymentOrder->amount = $converter->convert($paymentOrder->currency, Currency::CODE_RUB, $paymentOrder->amount);
            $paymentOrder->currency = Currency::CODE_RUB;
        }

        if ($bindingId) {
            $order = $this->_payment->charge(
                $paymentOrder->amount,
                $paymentOrder->currency,
                $this->customer->clientId,
                $bindingId,
                $paymentOrder->description,
                $paymentOrder->orderNumber
            );
            return [
                'url'   => '',
                'order' => $order,
            ];
        } else {
            return $this->_payment->getUrlAndOrderIdForRegistryCard($this->customer->clientId);
        }

        return parent::register($paymentOrder, $bindingId);
    }

    public function deposit($order, $amount)
    {
        // TODO not use
    }

    public function refund($order, $amount)
    {
        $this->_payment->refundCharge($order, $amount, $this->isBinding);
    }

    public function status($order)
    {
        return $this->_payment->getStatusCharge($order, $this->isBinding);
    }

    public function bindings()
    {
        return $this->_payment->getCardList($this->customer->clientId);
    }

    public function unbind($bindingId)
    {
        return $this->_payment->deleteCard($bindingId);
    }

    public function payment($order, $binding)
    {
        // not execute
    }
}