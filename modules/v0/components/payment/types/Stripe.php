<?php

namespace v0\components\payment\types;

use v0\models\Customer;
use v0\Module as V0;
use v0\models\Locale;


class Stripe extends \yii\base\Object implements Payment
{
    use Locale;

    /**
     * @var Customer
     */
    public $customer;


    /**
     * @var \stripe\components\StripeFacade
     */
    private $_payment;

    public function setPaymentName($value)
    {
        $this->_payment->setProfile($value);
    }

    public function getPaymentName()
    {
        return $this->_payment->getProfile()->paymentName;
    }

    public function __construct($config = [])
    {
        $this->_payment = V0::getInstance()->getModule('stripe')->get('payment');

        parent::__construct($config);


    }

    /**
     * Registering and approval amount with client's card
     *
     * @param \v0\models\PaymentOrder $paymentOrder Order number in our system
     * @param string $bindingId
     *
     * @return array Field order and url
     * @throws \stripe\exceptions\NotFoundCustomerException
     */
    public function register($paymentOrder, $bindingId = null)
    {
        if ($bindingId) {
            /* @var $currencyService \v0\components\currency\CurrencyService */
            $currencyService = V0::getComponent('currency_service');
            $cur = $currencyService->getCurrency($paymentOrder->currency);

            $stripeAccount = !empty($paymentOrder->params['stripeAccount'])
                ? (string)$paymentOrder->params['stripeAccount'] : null;
            $order = $this->_payment->charge(
                $paymentOrder->orderNumber,
                $paymentOrder->amount,
                strtolower($cur->code),
                $this->customer->clientId,
                $bindingId,
                $paymentOrder->description,
                $stripeAccount
            );
            return [
                'url'   => '',
                'order' => $order,
            ];
        } else {
            return [
                'url'   => $this->_payment->getUrlForRegistryCard($this->customer->clientId, $this->locale),
                'order' => null,
            ];
        }
    }

    /**
     * Finished pay current order on this amount
     * @param string $order
     * @param integer $amount
     * @return boolean True - success
     */
    public function deposit($order, $amount)
    {
        // TODO not use
    }

    /**
     * Cancel all amount for current order
     * @param string $order
     * @return boolean True - success
     */
    public function refund($order, $amount)
    {
        if (isset($order)) { // Stubs for checkCard
            $this->_payment->refundEntireCharge($this->customer->clientId, $order);
        }
    }

    /**
     * Get status order
     *
     * @param string $order
     * @return array Fields pan, client_id, binding_id, amount, actionCode and currency
     */
    public function status($order)
    {
        if (isset($order)) {
            $charge = $this->_payment->getStatusCharge($this->customer->clientId, $order);
            return [
                'pan'        => $charge->getCard()->maskedPan,
                'amount'     => $charge->amount,
                'actionCode' => $charge->captured && !$charge->refunded ? 0 : 116,
                'currency'   => $charge->currency,
                'client_id'  => $this->customer->clientId,
                'binding_id' => $charge->getCard()->stripeToken,
            ];
        } else { // Stubs for checkCard
            return [
                'pan'        => null,
                'amount'     => null,
                'actionCode' => 0,
                'currency'   => null,
                'client_id'  => $this->customer->clientId,
                'binding_id' => 'exists for check card',
            ];
        }
    }

    /**
     *
     * @return array Array include arrays with field binding and pan
     */
    public function bindings()
    {
        $cards = $this->_payment->getCardList($this->customer->clientId);
        return array_map(function ($card) {
            /* @var $card \stripe\models\Card */
            return [
                'pan'     => $card->maskedPan,
                'binding' => $card->stripeToken,
            ];
        }, $cards);
    }

    /**
     *
     * @param string $bindingId
     * @return boolean True - success
     */
    public function unbind($bindingId)
    {
        $this->_payment->deleteCard($this->customer->clientId, $bindingId);
        return true;
    }

    /**
     *
     * @param string $order
     * @param string $binding
     * @return boolean True - success
     */
    public function payment($order, $binding)
    {
        // dont execute
    }

}