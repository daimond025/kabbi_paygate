<?php
namespace v0\components\payment\types;

use v0\components\currency\CurrencyService;
use v0\models\Customer;
use yii\base\Object;
use v0\Module as V0;
use app\components\exception\ServiceErrorHttpException;
use app\components\exception\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use app\components\exception\NotEnoughMoneyHttpException;
use v0\models\Locale;

/**
 *
 * @property string url
 */
abstract class BasicPayment extends Object implements Payment
{
    use Locale;

    const ACTION_CODE_SUCCESS_PAYMENT = 0;
    const ACTION_CODE_NOT_ENOUGH_MONEY = 116;

    // name profile
    public $paymentName;

    // user on payment gateway
    public $username;

    // password on payment gateway
    public $password;

    // return on this url on success pay
    public $returnUrl = 'finish.html';

    // return on this url on failed pay
    public $failUrl;

    // url for method
    public $url;

    public $currency;

    public $cardBindingSum;

    // extra request params for payment by bindingId
    public $extraPaymentParams = [];

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var Customer
     */
    public $customer;

    public function __construct(array $config = [])
    {
        $this->currencyService = V0::getComponent('currency_service');

        parent::__construct($config);
    }

    public function register($paymentOrder, $bindingId = null)
    {
        if (!empty($this->currency) && empty($paymentOrder->currency)) {
            $paymentOrder->currency = (int)$this->currency;
        }

        $data = $paymentOrder->toArray([
            'orderNumber',
            'amount',
            'description',
            'currency',
        ]);
        $data = ArrayHelper::merge($data, [
            'returnUrl' => $this->returnUrl,
            'failUrl'   => $this->failUrl,
            'clientId'  => $this->customer->clientId,
        ]);
        if ($bindingId) {
            $data['bindingId'] = $bindingId;
            $data = ArrayHelper::merge($data, $this->extraPaymentParams);
        } elseif ($this->cardBindingSum !== null && $data['currency'] !== null) {
            $data['amount'] = $this->getMinorUnitAmount($this->cardBindingSum, $data['currency']);
        }

        $result = $this->send('register.do', $this->preparePaymentData($data));

        return [
            'url'   => array_key_exists('formUrl', $result) ? $result['formUrl'] : null,
            'order' => array_key_exists('orderId', $result) ? $result['orderId'] : null,
        ];
    }

    private function getMinorUnitAmount($amount, $currencyCode)
    {
        $currency = $this->currencyService->getCurrency($currencyCode);
        if ($currency) {
            $minorUnit = empty($currency->minorUnit) ? 0 : $currency->minorUnit;
            $amount *= 10 ** $minorUnit;
        }

        return $amount;
    }

    protected function preparePaymentData($data)
    {
        return $data;
    }

    public function deposit($orderId, $amount)
    {
        try {
            $this->send('deposit.do', [
                'orderId'   => $orderId,
                'amount'    => $amount
            ]);
        } catch (InvalidArgumentException $e) {
            return false;
        }

        return true;
    }

    public function refund($orderId, $amount)
    {
        $this->send('refund.do', [
            'orderId'   => $orderId,
            'amount'    => $amount,
        ]);
        return true;
    }

    public function status($orderId)
    {
        $result = $this->send('getOrderStatusExtended.do', [
            'orderId'   => $orderId
        ]);
        if (array_key_exists('bindingInfo', $result)) {
            $bindingInfo = $result['bindingInfo'];
            $return = [
                'client_id' => array_key_exists('clientId', $bindingInfo) ? $bindingInfo['clientId'] : null,
                'binding_id' => array_key_exists('bindingId', $bindingInfo) ? $bindingInfo['bindingId'] : null,
            ];
        } else {
            $return = array();
        }
        return array_merge($return, [
            'pan' => array_key_exists('pan', $result) ? $result['Pan'] : null,
            'amount' => $result['amount'],
            'currency' => $result['currency'],
            'actionCode' => array_key_exists('actionCode', $result) ? $result['actionCode'] : null,
        ]);
    }

    public function bindings()
    {
        $result = $this->send('getBindings.do', [
            'clientId' => $this->customer->clientId,
        ]);
        if (array_key_exists('bindings', $result)) {
            $yeld = array();
            foreach ($result['bindings'] as $binding) {
                if (array_key_exists('bindingId', $binding) && array_key_exists('maskedPan', $binding)) {
                    $yeld[] = [
                        'binding' => $binding['bindingId'],
                        'pan' => $binding['maskedPan']
                    ];
                }
            }
            return $yeld;
        } else {
            throw new ServiceErrorHttpException('No key bindings in response');
        }
    }

    public function unbind($bindingId)
    {
        $result = $this->send('unBindCard.do', [
            'bindingId' => $bindingId
        ]);
        return true;
    }

    public function payment($order, $binding)
    {
        $this->send('paymentOrderBinding.do', [
            'mdOrder' => $order,
            'bindingId' => $binding,
        ]);
        $actionCode = $this->status($order)['actionCode'];
        switch ($actionCode) {
            case self::ACTION_CODE_SUCCESS_PAYMENT:
                break;
            case self::ACTION_CODE_NOT_ENOUGH_MONEY:
                throw new NotEnoughMoneyHttpException('on card');
            default:
                throw new ServiceErrorHttpException("Processing error [actionCode={$actionCode}]");
        }
        return true;
    }

    protected function send($method, $data)
    {
        /* @var $curl \v0\components\curl\Curl */
        $curl = V0::getComponent('curl');
        \Yii::configure($curl, [
            'url' => $this->url . $method,
            'data' => array_merge([
                'userName' => $this->username,
                'password' => $this->password
            ], $data)
        ]);
        $resultJson = $curl->post();
        if (empty($result = json_decode($resultJson, true))) {
            throw new ServiceErrorHttpException();
        }
        $resultLow = array_change_key_case($result);
        if (array_key_exists('errorcode', $resultLow)) {

            $msg = array_key_exists('errormessage', $resultLow) ? $resultLow['errormessage'] : null;
            if($resultLow['errorcode'] != 0) {
                throw new ServiceErrorHttpException($msg);
            }

        }
        return $result;
    }
}
