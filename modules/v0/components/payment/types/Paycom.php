<?php

namespace v0\components\payment\types;

use paycom\components\PaycomService;
use paycom\exceptions\PaycomException;
use v0\models\PaymentOrder;
use v0\Module as V0;
use v0\models\Customer;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Object;
use v0\models\Locale;

/**
 * Class Paycom
 * @package v0\components\payment\types
 */
class Paycom extends Object implements Payment
{
    use Locale;

    /**
     * @var string
     */
    public $paymentName;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var string
     */
    public $password;

    /**
     * @var PaycomService
     */
    public $paycomService;



    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $module = V0::getInstance()->getModule('paycom');

        $this->paycomService = $module->get('paycomService');
        $this->paycomService->setProfile($this->paymentName);
    }

    /**
     * Registering and approval amount with client's card
     *
     * @param PaymentOrder $paymentOrder Order number in our system
     * @param string       $bindingId
     *
     * @return array Field order and url
     * @throws PaycomException
     *
     * @throws InvalidParamException
     */
    public function register($paymentOrder, $bindingId = null)
    {
        if ($bindingId) {
            $receiptId = $this->paycomService->createReceipt($this->customer, $paymentOrder->orderNumber, $bindingId,
                (int)$paymentOrder->amount, $paymentOrder->description);

            return ['url' => null, 'order' => $receiptId];
        }

        return [
            'url'   => $this->paycomService->getUrlForRegistryCard($this->customer),
            'order' => null,
        ];
    }

    /**
     * Finished pay current order on this amount
     *
     * @param string  $order
     * @param integer $amount
     *
     * @return boolean True - success
     */
    public function deposit($order, $amount)
    {
        // TODO: Implement deposit() method.
    }

    /**
     * Cancel all amount for current order
     *
     * @param string $order
     *
     * @return boolean True - success
     * @throws PaycomException
     */
    public function refund($order, $amount)
    {
        if (empty($order)) {
            return false;
        }

        return $this->paycomService->cancelReceipt($order);
    }

    /**
     * Get status order
     *
     * @param string $order
     *
     * @return array Fields pan, client_id, binding_id, amount, actionCode and currency
     * @throws PaycomException
     */
    public function status($order)
    {
        $receipt = $this->paycomService->getReceipt($order);

        return [
            'pan'        => $receipt['pan'],
            'amount'     => $receipt['amount'],
            'actionCode' => $receipt['state'] <= 4 ? 0 : $receipt['state'],
            'currency'   => null,
            'client_id'  => $receipt['client_id'],
            'binding_id' => $receipt['token'],
        ];
    }

    /**
     * @return array Array include arrays with field binding and pan
     * @throws PaycomException
     */
    public function bindings()
    {
        return $this->paycomService->getCards($this->customer);
    }

    /**
     *
     * @param string $bindingId
     *
     * @return boolean True - success
     * @throws PaycomException
     */
    public function unbind($bindingId)
    {
        $this->paycomService->removeCard($this->customer, $bindingId);
    }

    /**
     * @param string $order
     * @param string $binding
     *
     * @return boolean True - success
     * @throws PaycomException
     */
    public function payment($order, $binding)
    {
        return $this->paycomService->payReceipt($order, $binding);
    }
}