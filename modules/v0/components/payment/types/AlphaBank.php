<?php

namespace v0\components\payment\types;

use v0\models\Currency;

class AlphaBank extends BasicPayment
{
    public function init()
    {
        $this->extraPaymentParams = ['features' => 'AUTO_PAYMENT'];
    }

    public function preparePaymentData($data)
    {
        if (isset($data['currency'])) {
            $currency = Currency::findOne([
                'code' => $data['currency'],
            ]);
            if ($currency !== null) {
                $data['currency'] = $currency->oldCode;
            }
        }

        return $data;
    }
}