<?php
namespace v0\components\payment\types;

interface Payment
{
   /**
    * Registering and approval amount with client's card
    * @param \v0\models\PaymentOrder $paymentOrder Order number in our system
    * @param string $bindingId
    * @return array Field order and url
    */
   public function register($paymentOrder, $bindingId = null); // USE 1 step payment
   
   /**
    * Finished pay current order on this amount
    * @param string $order
    * @param integer $amount
    * @return boolean True - success
    */
   public function deposit($order, $amount); 
   
   /**
    * Cancel all amount for current order
    * @param string $order
    * @return boolean True - success
    */
   public function refund($order, $amount); // TODO
     
   /**
    * Get status order
    * @param string $order
    * @return array Fields pan, client_id, binding_id, amount, actionCode and currency
    */
   public function status($order);
   
   /**
    * @return array Array include arrays with field binding and pan
    */
   public function bindings();
   
   /**
    * 
    * @param string $bindingId
    * @return boolean True - success
    */
   public function unbind($bindingId);
   
   /**
    * 
    * @param string $order
    * @param string $binding
    * @return boolean True - success
    */
   public function payment($order, $binding);
}