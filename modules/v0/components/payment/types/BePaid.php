<?php

namespace v0\components\payment\types;

use bepaid\components\BePaidService;
use bepaid\exceptions\NotFoundProfileException;
use v0\models\Customer;
use v0\models\Locale;
use v0\models\PaymentOrder;
use v0\Module as V0;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Object;

/**
 * Class BePaid
 * @package v0\components\payment\types
 */
class BePaid extends Object implements Payment
{
    use Locale;

    /**
     * @var string
     */
    public $paymentName;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var float
     */
    public $cardBindingSum;

    /**
     * @var BePaidService
     */
    private $bePaidService;

    /**
     * @throws NotFoundProfileException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function init()
    {
        $module = V0::getInstance()->getModule('bepaid');

        $this->bePaidService = $module->get('bePaidService');
        $this->bePaidService->setProfile($this->paymentName);
        $this->bePaidService->setLanguage($this->locale);
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @param null $bindingId
     *
     * @return array
     * @throws NotFoundProfileException
     */
    public function register($paymentOrder, $bindingId = null)
    {
        if ($bindingId) {
            $uid = $this->bePaidService->pay($this->customer->clientId, $bindingId, $paymentOrder->orderNumber,
                (int)$paymentOrder->amount, $paymentOrder->currency, $paymentOrder->description);

            return ['url' => null, 'order' => $uid];
        }

        return [
            'url'   => $this->bePaidService->getUrlForBindingCard(
                $this->customer->clientId, $paymentOrder->orderNumber, $paymentOrder->description),
            'order' => null,
        ];
    }

    public function deposit($order, $amount)
    {
    }

    /**
     * @param string $order
     * @param int $amount
     *
     * @return bool
     * @throws NotFoundProfileException
     */
    public function refund($order, $amount)
    {
        if (empty($order)) {
            return false;
        }

        $this->bePaidService->refund($this->customer->clientId, $order, $amount);
    }

    /**
     * @param string $order
     *
     * @return array
     * @throws NotFoundProfileException
     */
    public function status($order)
    {
        $payment = $this->bePaidService->status($this->customer->clientId, $order);
        $card = $payment->getCard();

        return [
            'pan'        => $card !== null ? $card->getPan() : '',
            'amount'     => $payment->getAmount(),
            'actionCode' => $payment->isSuccessful() && !$payment->isRefunded() ? 0 : 116,
            'currency'   => $payment->getCurrency(),
            'client_id'  => $payment->getClientId(),
            'binding_id' => $card !== null ? $card->getToken() : '',
        ];
    }

    public function bindings()
    {
        return $this->bePaidService->getCards($this->customer->clientId);
    }

    public function unbind($bindingId)
    {
        $this->bePaidService->removeCard($this->customer->clientId, $bindingId);
    }

    public function payment($order, $binding)
    {
    }
}