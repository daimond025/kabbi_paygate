<?php

namespace v0\components\payment\types;

use app\components\exception\ServerErrorHttpException;
use payture\components\api\exceptions\RequestException;
use payture\components\exceptions\PaytureException;
use payture\components\PaytureService;
use payture\exceptions\NotFoundProfileException;
use v0\components\currency\CurrencyService;
use v0\models\Customer;
use v0\models\Locale;
use v0\models\PaymentOrder;
use v0\Module as V0;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Object;

/**
 * Class Payture
 * @package v0\components\payment\types
 */
class Payture extends Object implements Payment
{
    use Locale;

    /**
     * @var string
     */
    public $paymentName;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var string
     */
    public $password;

    /**
     * @var PaytureService
     */
    public $paytureService;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var string
     */
    public $currency;

    /**
     * @throws ServerErrorHttpException
     * @throws InvalidConfigException
     * @throws NotFoundProfileException
     * @throws Exception
     */
    public function init()
    {
        $module = V0::getInstance()->getModule('payture');

        $this->currencyService = V0::getComponent('currency_service');

        $this->paytureService = $module->get('paytureService');
        $this->paytureService->setProfile($this->paymentName);
        $this->paytureService->setLanguage($this->locale);
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @param string|null $bindingId
     *
     * @return array
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function register($paymentOrder, $bindingId = null)
    {
        if ($bindingId) {
            $this->paytureService->pay($this->customer->clientId, $bindingId, $paymentOrder->orderNumber,
                (int)$paymentOrder->amount, $paymentOrder->currency);

            return ['url' => null, 'order' => $paymentOrder->orderNumber];
        }

        return [
            'url'   => $this->paytureService->getUrlForBindingCard($this->customer->clientId),
            'order' => null,
        ];
    }

    public function deposit($order, $amount)
    {
    }

    /**
     * @param string $order
     * @param int $amount
     *
     * @return bool|void
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function refund($order, $amount)
    {
        $this->paytureService->refund($this->customer->clientId, $order, $amount);
    }

    /**
     * @param string $order
     *
     * @return array
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function status($order)
    {
        $payment = $this->paytureService->status($this->customer->clientId, $order);
        $card = $payment->getCard();

        return [
            'pan'        => $card !== null ? $card->getPan() : '',
            'amount'     => $payment->getAmount(),
            'actionCode' => $payment->isCharged() ? 0 : 116,
            'currency'   => $payment->getCurrency(),
            'client_id'  => $payment->getClientId(),
            'binding_id' => $card !== null ? $card->getId() : '',
        ];
    }

    /**
     * @return array
     *
     * @throws NotFoundProfileException
     * @throws RequestException
     */
    public function bindings()
    {
        return $this->paytureService->getCards($this->customer->clientId);
    }

    /**
     * @param string $bindingId
     *
     * @return bool|void
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function unbind($bindingId)
    {
        $this->paytureService->removeCard($this->customer->clientId, $bindingId);
    }

    public function payment($order, $binding)
    {
    }

    /**
     * @param int $amount
     * @param string $currencyCode
     *
     * @return int
     * @throws ServerErrorHttpException
     */
    private function getMinorUnitAmount($amount, $currencyCode)
    {
        $currency = $this->currencyService->getCurrency($currencyCode);
        if ($currency) {
            $minorUnit = empty($currency->minorUnit) ? 0 : $currency->minorUnit;

            return $amount * (10 ** $minorUnit);
        }

        throw new ServerErrorHttpException("Currency not found: code={$currencyCode}");
    }
}