<?php

namespace v0\components\payment;

use v0\Module as V0;
use yii\base\Object;
use app\components\exception\ServerErrorHttpException;

class Gate extends Object
{
    const NAMESPACE_TYPES = 'v0\components\payment\types';

    private $_payment;

    public function setPayment(
        \yii\base\Model $paygateProfile,
        \v0\models\Customer $customer,
        $locale
    ) {
        $params = $paygateProfile->toArray([
            'paymentName',
            'username',
            'url',
            'returnUrl',
            'failUrl',
            'currency',
            'cardBindingSum',
        ]);

        if (isset($paygateProfile->password)) {
            $params['password'] = $paygateProfile->password;
        }
        $params['customer'] = $customer;
        $params['class'] = self::NAMESPACE_TYPES . '\\' . $paygateProfile->type->classname;
        $params['locale'] = $locale;


        try {
            $this->_payment = \Yii::createObject($params);
        } catch (\ReflectionException $e) {
            throw new ServerErrorHttpException(\Yii::t('app/error', 'Error classname'), $e);
        } catch (\yii\base\Exception $e) {
            throw new ServerErrorHttpException(\Yii::t('app/error', 'Error config type of a payment gate'), $e);
        }
    }

    /**
     * @return \v0\components\payment\Payment
     * @throws ServerErrorHttpException
     */
    public function getPayment()
    {
        if ($this->_payment) {
            return $this->_payment;
        } else {
            throw new ServerErrorHttpException(
                \Yii::t('app/error', 'No set payment gate in the component')
            );
        }
    }

    public function __call($name, $params)
    {
        return call_user_func_array([$this->getPayment(), $name], $params);
    }

    public function getClientId()
    {
        return $this->_payment->customer->clientId;
    }

    public function setUseBindingProfile()
    {
        if (property_exists($this->_payment, 'isBinding')) {
            $this->_payment->isBinding = true;
        }
    }
}