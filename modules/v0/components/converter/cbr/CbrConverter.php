<?php

namespace v0\components\converter\cbr;

use app\components\exception\InvalidArgumentException;
use v0\components\converter\cbr\Rate;
use v0\components\converter\ConverterInterface;
use yii\base\Object;

/**
 * Класс конвертации валюты Центробанка РФ
 * @package v0\components\converter
 */
class CbrConverter extends Object implements ConverterInterface
{
    const CODE_RUB = 643;

    private $cbr;

    /**
     * @param Cbr $cbr
     * @param [] $config
     */
    public function __construct(Cbr $cbr, $config = [])
    {
        $this->cbr = $cbr;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function convert($from, $to, $amount)
    {
        if ($from === $to) {
            return $amount;
        } elseif ($to !== self::CODE_RUB) {
            throw new InvalidArgumentException("to='$to' Supported only conversion into russian rubles");
        }

        /* @var $rate Rate */
        $rate = $this->cbr->getCurrencyRate($from);

        return floor($amount * $rate->value / $rate->nominal);
    }

}