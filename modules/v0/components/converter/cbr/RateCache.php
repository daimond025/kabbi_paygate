<?php

namespace v0\components\converter\cbr;

/**
 * Class RateCache
 * @package v0\components\converter\cbr
 */
class RateCache
{
    /**
     * @var array
     */
    public $rates;

    /**
     * @var integer
     */
    public $time;

    public function __construct(array $rates, $time)
    {
        $this->rates = $rates;
        $this->time  = $time;
    }

}