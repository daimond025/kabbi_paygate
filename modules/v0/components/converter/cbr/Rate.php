<?php

namespace v0\components\converter\cbr;

/**
 * Class Rate
 * @package v0\components\converter\cbr
 */
class Rate
{
    /**
     * @var int
     */
    public $nominal;

    /**
     * @var float
     */
    public $value;

    /**
     * Rate constructor.
     *
     * @param int   $nominal
     * @param float $value
     */
    public function __construct($nominal, $value)
    {
        $this->nominal = $nominal;
        $this->value   = $value;
    }

}