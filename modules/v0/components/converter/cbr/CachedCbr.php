<?php

namespace v0\components\converter\cbr;

use app\components\exception\ServerErrorHttpException;
use v0\Module as V0;
use yii\caching\Cache;

/**
 * Класс обертка для получения кэшированных результатов
 * @package v0\components\converter\cbr
 */
class CachedCbr extends Cbr
{
    const CACHE_KEY = 'cbr.rates';

    public $cacheDuration = 12 * 60 * 60;
    public $refreshDuration = 3 * 60 * 60;

    /* @var Cache */
    private $cache;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->cache = V0::getComponent('cache');

        parent::init();
    }

    /**
     * Saving rates to cache
     *
     * @param $rateCache
     */
    private function saveRatesToCache($rateCache)
    {
        $this->cache->set(self::CACHE_KEY, $rateCache, $this->cacheDuration);
    }

    /**
     * Getting rates information from cache
     * @return RateCache|false
     */
    private function getRatesFromCache()
    {
        return $this->cache->get(self::CACHE_KEY);
    }

    /**
     * @inheritdoc
     */
    public function getCurrencyRates()
    {
        $rateCache    = $this->getRatesFromCache();
        $isValidCache = $rateCache instanceof RateCache;

        if (!$isValidCache || ($rateCache->time + $this->refreshDuration < time())) {
            try {
                $rates     = parent::getCurrencyRates();
                $rateCache = new RateCache($rates, time());

                $this->saveRatesToCache($rateCache);
            } catch (ServerErrorHttpException $ex) {
                if (!$isValidCache) {
                    throw $ex;
                }
            }
        }

        return $rateCache->rates;
    }

}