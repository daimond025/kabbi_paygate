<?php

namespace v0\components\converter\cbr;

use app\components\exception\InvalidArgumentException;
use app\components\exception\ServerErrorHttpException;
use v0\Module as V0;
use yii\base\Object;

/**
 * Класс для запроса курса валют от Центробанка РФ
 * @package v0\components\converter\cbr
 */
class Cbr extends Object
{
    const MESSAGE_PARSING_ERROR = 'Cbr response parsing error';
    const MESSAGE_INVALID_CURRENCY = 'Exchange rate can not be found';

    public $url = 'cbr.ru/scripts/XML_daily.asp';
    public $timeOffset = '+3 hour';

    /**
     * Получить курс валюты по отношению к рублю
     *
     * @param string $currency
     *
     * @return Rate
     * @throws InvalidArgumentException
     */
    public function getCurrencyRate($currency)
    {
        $rates = $this->getCurrencyRates();

        if (!isset($rates[$currency])) {
            throw new InvalidArgumentException("currency='$currency' " 
                    . self::MESSAGE_INVALID_CURRENCY);
        }

        return $rates[$currency];
    }

    /**
     * Парсинг ответа банка
     *
     * @param string $data
     *
     * @return Rate[]
     * @throws ServerErrorHttpException
     */
    public function getParsedRates($data)
    {
        $rates = [];
        try {
            $xmlObj = simplexml_load_string($data);

            foreach ($xmlObj->Valute as $item) {
                $code    = (string)$item->NumCode;
                $nominal = (int)$item->Nominal;
                $rate    = (float)str_replace(',', '.', $item->Value);

                $rates[$code] = new Rate($nominal, $rate);
            }
        } catch (\Exception $e) {
            throw new ServerErrorHttpException(self::MESSAGE_PARSING_ERROR, $e);
        }

        return $rates;
    }

    /**
     * Получить список курсов валют по отношению к рублю с Центробанка
     *
     * @return Rate[]
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     */
    public function getCurrencyRates()
    {
        $date = date('d/m/Y', strtotime($this->timeOffset));

        /* @var $curl \v0\components\curl\Curl */
        $curl = V0::getComponent('curl');
        \Yii::configure($curl, ['url' => $this->url, 'data' => ['date_req' => $date]]);

        $data = $curl->get();

        return $this->getParsedRates($data);
    }
}