<?php

namespace v0\components\converter;

/**
 * Интерфейс для конвертации валюты
 * @package converter
 */
interface ConverterInterface
{
    /**
     * Конвертация валюты
     *
     * @param int $from
     * @param int $to
     * @param int $amount
     *
     * @return int
     */
    function convert($from, $to, $amount);
}