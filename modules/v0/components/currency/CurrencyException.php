<?php

namespace v0\components\currency;

class CurrencyException extends \RuntimeException
{
    
    public function __construct($message, $previous = null)
    {
        parent::__construct($message, 255, $previous);
    }
    
}