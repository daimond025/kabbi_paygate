<?php

namespace v0\components\currency;

use yii\base\Object;
use yii\base\InvalidValueException;

class CurrencyService extends Object
{
    
    /**
     * 
     * @var \GuzzleHttp\Client
     */
    private $httpClient = null;
    
    /**
     * 
     * @param string|\GuzzleHttp\Client $client
     * @param array $config
     * @throws \InvalidArgumentException
     */
    public function __construct($client, $config = [])
    {
        if ($client instanceof \GuzzleHttp\Client) {
            $this->httpClient = $client;
        } elseif(is_string($client)) {
            $this->httpClient = new \GuzzleHttp\Client(['base_uri' => $client]);
        } else {
            throw new InvalidValueException('client must be of string or instance of \GuzzleHttp\Client');
        }
    }
    
    public function getCurrency($code)
    {
        $jsonData = $this->request('GET', sprintf('/currencies/%s', $code));
        
        $data = $this->parse($jsonData);
        $data['minorUnit'] = key_exists('minor_unit', $data) ? $data['minor_unit'] : null;
        $data['countries'] = key_exists('countries_list', $data) ? $data['countries_list'] : null;
        
        $cur = new Currency();
        $cur->setAttributes($data);
        if (! $cur->validate()) {
             throw new CurrencyException('Invalid request params of currency');
        }
        return $cur;
    }
    
    private function request($method, $uri, $options = [])
    {
        try {
            return $this->httpClient->request($method, $uri, $options)->getBody();
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new CurrencyException('Fail connect to uri', $e);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw new CurrencyException('Response status code is 4xx', $e);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new CurrencyException('Response status code is 5xx', $e);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw new CurrencyException('Illegal error on request', $e);
        }
    }
    
    /**
     * 
     * @param string $jsonData
     * @throws CurrencyException
     * @return array
     */
    private function parse($jsonData)
    {
        $data = json_decode($jsonData, true);
        if (is_null($data)) {
            throw new CurrencyException('Result is not json scheme');
        }
        return $data;
    }
    
}