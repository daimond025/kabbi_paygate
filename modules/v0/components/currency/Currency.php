<?php

namespace v0\components\currency;

use yii\base\Model;

class Currency extends Model
{
    
    public $code;
    
    public $number;
    
    public $name;
    
    public $minorUnit;
    
    public $countries;
    
    public $symbol;
    
    public function rules()
    {
        return [
            [['code', 'number', 'minorUnit', 'name'], 'required'],
            [['code', 'number'], 'string', 'max' => 3],
            [['name', 'symbol'], 'string'],
            [['countries'], 'safe'],
        ];
    }
    
}