<?php

namespace v0\actions\payment;

use v0\actions\DomainAction;

class Refund extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            [
                'class' => '\v0\handlers\base\SendSyslogHandler',
                'message' => 'Refund order is successful'
            ],
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetPaygateHandler',
            '\v0\handlers\payment\RefundOrderHandler'
        ];
    }

}