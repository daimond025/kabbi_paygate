<?php

namespace v0\actions\payment;

use v0\actions\DomainAction;

class Deposit extends DomainAction
{
    protected function getConfigOperations()
    {
        return [
            [
                'class'   => '\v0\handlers\base\SendSyslogHandler',
                'message' => function (\yii\di\Container $container) {
                    /* @var $request \yii\web\Request */
                    $request = $container->get('request');

                    return sprintf(
                        "Deposit order number='%s' on amount='%s', currency='%s' with pan='%s' (destination='%s') is successful",
                        $request->post('orderNumber'),
                        $request->post('amount'),
                        $request->post('currency', 643),
                        $request->post('pan'),
                        $request->post('destination')
                    );
                },
            ],
            '\v0\handlers\base\ValidateBodyParamsHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetPaygateHandler',
            [
                'class'    => '\v0\handlers\base\SetOrderHandler',
                'scenario' => 'deposit',
            ],
            '\v0\handlers\payment\DepositOrderHandler',
        ];
    }
}