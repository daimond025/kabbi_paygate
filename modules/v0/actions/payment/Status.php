<?php

namespace v0\actions\payment;

use v0\actions\DomainAction;

class Status extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            '\v0\handlers\base\SendSyslogHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetPaygateHandler',
            '\v0\handlers\payment\ResponseStatusOrderHandler',
        ];
    }
    
}