<?php

namespace v0\actions;

use Yii;
use v0\Module as V0;
use yii\base\Action;
use yii\di\Container;


abstract class DomainAction extends Action
{
    
    protected $configEmptyOperation = [
        'class' => '\v0\handlers\base\EmptyHandler',
    ];
    
    
    /**
     * Execute domain operations
     */
    final public function run()
    {

        $container = new Container();
        $container->set('request', V0::getComponent('request'));
        $container->set('response', V0::getComponent('response'));


        $this->getOperations()->run($container);
        
        return $container->get('response');
    }
    
    /**
     * @return \v0\operations\Runnable
     */
    protected function getOperations()
    {
        $config = (array) $this->getConfigOperations();
        $config = array_reverse($config);

        $object = Yii::createObject($this->configEmptyOperation);
        foreach ($config as $configObject) {
            $object = Yii::createObject($configObject, [$object]);
        }
        return $object;
    }
    
    /**
     * @return array
     */
    abstract protected function getConfigOperations();
    
}