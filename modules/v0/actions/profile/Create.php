<?php

namespace v0\actions\profile;

use v0\actions\DomainAction;

class Create extends DomainAction
{
    public $params;
    
    protected function getConfigOperations()
    {
        return [
            [
                'class' => '\v0\handlers\base\SendSyslogHandler',
                'message' => function (\yii\di\Container $container)
                {
                    $paygateName = $container->get('paygateProfile')->paymentName;
                    return "Create profile='{$paygateName}' on DB is successful";
                }
            ],
            '\v0\handlers\base\ValidateBodyParamsHandler',
            '\v0\handlers\profile\SetNewPaygateProfileHandler',
            '\v0\handlers\profile\UpdatePaygateProfileHandler'
        ];
    }
}