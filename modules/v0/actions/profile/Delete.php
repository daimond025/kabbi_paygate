<?php

namespace v0\actions\profile;

use v0\actions\DomainAction;

class Delete extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            '\v0\handlers\base\SendSyslogHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\profile\DeletePaygateProfileHandler',
        ];
    }
}