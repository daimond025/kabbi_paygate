<?php

namespace v0\actions\profile;

use v0\actions\DomainAction;

class View extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            '\v0\handlers\base\SendSyslogHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            [
                'class' => '\v0\handlers\base\ParamToResponseHandler', 
                'paramName' => 'paygateProfile',
            ]
        ];
    }

}