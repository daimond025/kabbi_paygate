<?php

namespace v0\actions\profile;

use v0\actions\DomainAction;

class Update extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            [
                'class' => '\v0\handlers\base\SendSyslogHandler',
                'message' => function (\yii\di\Container $container)
                    {
                    /* @var $request \yii\web\Request */
                    $request = $container->get('request');

                    $paramsString = []; 
                    foreach ($request->post() as $param => $value) {
                        if (strlen($value) > 255 || stristr($param, 'password')) {
                            $paramsString[] = "$param";
                            continue;
                        }
                        $paramsString[] = "$param='$value'";
                    }
                    $paramsString = $paramsString ? implode(', ', $paramsString) : 'empty';
                    return "Update params: $paramsString on the profile is successful";
                }
            ],
            // '\v0\handlers\base\ValidateBodyParamsHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\profile\UpdatePaygateProfileHandler'
        ];
    }
    
}