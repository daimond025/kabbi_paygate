<?php

namespace v0\actions\card;

use v0\actions\DomainAction;

class Check extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            [
                'class' => '\v0\handlers\base\SendSyslogHandler',
                'message' => function (\yii\di\Container $container)
                {
                    $orderId = $container->get('request')->post('orderId');
                    return "Validate card with orderId='{$orderId}' is successful";
                }
            ],
            '\v0\handlers\base\ValidateBodyParamsHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetPaygateHandler',
            '\v0\handlers\card\CheckNewCardHandler'
        ];
    }

    
}