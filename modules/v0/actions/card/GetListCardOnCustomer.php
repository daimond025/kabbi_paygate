<?php

namespace v0\actions\card;

use v0\actions\DomainAction;

class GetListCardOnCustomer extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            '\v0\handlers\base\SendSyslogHandler',
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetPaygateHandler',
            '\v0\handlers\card\ResponseListCardOnCustomerHandler',
        ];
    }
    
}