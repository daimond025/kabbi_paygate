<?php

namespace v0\actions\card;

use v0\actions\DomainAction;

class Create extends DomainAction
{
    
    protected function getConfigOperations()
    {
        return [
            [
                'class' => '\v0\handlers\base\SendSyslogHandler',
                'message' => function (\yii\di\Container $container)
                {
                    $orderId = $container->get('response')->data['orderId'];
                    return "Create card with orderId='{$orderId}' is successful";
                }
            ],
            '\v0\handlers\profile\SetPaygateProfileHandler',
            '\v0\handlers\base\SetCustomerHandler',
            '\v0\handlers\base\SetLocaleHandler',
            '\v0\handlers\base\SetPaygateHandler',
            '\v0\handlers\card\AddNewCardHandler'
        ];
    }
    
}