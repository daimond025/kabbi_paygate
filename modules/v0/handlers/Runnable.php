<?php

namespace v0\handlers;


interface Runnable 
{
    
    public function run(\yii\di\Container $container);
    
}