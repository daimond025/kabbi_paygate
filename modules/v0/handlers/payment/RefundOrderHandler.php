<?php

namespace v0\handlers\payment;

use v0\handlers\base\BaseHandler;
use v0\models\PaymentOrder;

class RefundOrderHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        $orderId = $request->get('orderId');
        
        PaymentOrder::refund($orderId);
        
        parent::run($container);
    }
}