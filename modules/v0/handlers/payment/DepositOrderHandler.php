<?php

namespace v0\handlers\payment;

use v0\handlers\base\BaseHandler;

class DepositOrderHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        /* @var $order \v0\models\PaymentOrder */
        $order = $container->get('order');

        $container->get('response')->data = [
            'orderId' => $order->deposit(),
        ];

        parent::run($container);
    }
}