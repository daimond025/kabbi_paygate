<?php

namespace v0\handlers\payment;

use v0\handlers\base\BaseHandler;
use v0\models\PaymentOrder;

class ResponseStatusOrderHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        parent::run($container);
        
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        $orderId = $request->get('orderId');
        
        /* @var $response \yii\web\Response */
        $response = $container->get('response');
        $response->data = PaymentOrder::status($orderId);
    }
}