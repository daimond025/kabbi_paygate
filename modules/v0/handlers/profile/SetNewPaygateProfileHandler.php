<?php

namespace v0\handlers\profile;

use v0\handlers\base\BaseHandler;
use v0\models\PaymentGateProfile;

class SetNewPaygateProfileHandler extends BaseHandler
{
    
    /**
     * (non-PHPdoc)
     * @see \v0\handlers\BaseHandler::run()
     * 
     * @throws \app\components\exception\NotFoundHttpException;
     */
    public function run(\yii\di\Container $container)
    {
        $type = $container->get('request')->post('typeId');
        $container->set('paygateProfile', PaymentGateProfile::getNewPaymentGateProfile($type));
        parent::run($container);
    }
}