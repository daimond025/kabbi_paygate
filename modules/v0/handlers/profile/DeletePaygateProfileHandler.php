<?php

namespace v0\handlers\profile;

use v0\handlers\base\BaseHandler;
use app\components\exception\ServerErrorHttpException;

class DeletePaygateProfileHandler extends BaseHandler
{
    
    public function run(\yii\di\Container $container)
    {
        if ($container->has('paygateProfile')) {
            $paygateProfile = $container->get('paygateProfile');
            /* @var $paygateProfile \v0\models\PaymentGateProfile */
            $paygateProfile->delete();
        } else {
            throw new ServerErrorHttpException('No set paygateProfile for deleting');
        }
        parent::run($container);
    }
}