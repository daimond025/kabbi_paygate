<?php

namespace v0\handlers\profile;

use v0\handlers\base\BaseHandler;
use app\components\exception\ServerErrorHttpException;
use app\components\exception\InvalidArgumentException;

class UpdatePaygateProfileHandler extends BaseHandler
{
    
    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        
        if ($container->has('paygateProfile')) {
            /* @var $paygateProfile \yii\db\ActiveRecord */
            $paygateProfile = $container->get('paygateProfile');
            
            if (  $request->post('typeId') &&
                (!$paygateProfile->isNewRecord) &&
                  $request->post('typeId') != $paygateProfile->typeId
            ) {
                throw new InvalidArgumentException('typeId can\'t be updated');
            }
            
            $paygateProfile->load($request->post(), '');
            if ($paygateProfile->validate()) {
                if (! $paygateProfile->save()) {
                    throw new ServerErrorHttpException('No save profile at the payment gate');
                }
            } else {
                $errors = $paygateProfile->getFirstErrors();
                throw new InvalidArgumentException(array_shift($errors));
            }
        }
        parent::run($container);
    }
}