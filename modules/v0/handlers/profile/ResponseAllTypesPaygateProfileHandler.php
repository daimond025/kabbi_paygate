<?php

namespace v0\handlers\profile;

use v0\handlers\base\BaseHandler;
use v0\models\PaymentGateType;

class ResponseAllTypesPaygateProfileHandler extends BaseHandler
{
    
    public function run(\yii\di\Container $container)
    {
        $container->get('response')->data = PaymentGateType::find()->all();
        parent::run($container);
    }
}