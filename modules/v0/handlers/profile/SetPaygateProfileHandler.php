<?php

namespace v0\handlers\profile;

use v0\handlers\base\BaseHandler;
use v0\models\PaymentGateProfile;
use app\components\exception\NotFoundHttpException;
use yii\web\Request;

class SetPaygateProfileHandler extends BaseHandler
{
    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');

        $paygateName = $request->get('paymentName', 'null');


        $paygateProfile = PaymentGateProfile::findOne(['paymentName' => $paygateName]);


        if (! $paygateProfile) {
            throw new NotFoundHttpException("profile PG='$paygateName' don't exists in DB");
        }

        $container->set('paygateProfile', $paygateProfile);
        
        parent::run($container);
    }
}