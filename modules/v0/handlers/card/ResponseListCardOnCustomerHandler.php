<?php

namespace v0\handlers\card;

use \v0\handlers\base\BaseHandler;
use v0\models\Card;
use yii\web\NotFoundHttpException;


class ResponseListCardOnCustomerHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        $response = $container->get('response');
        try {
            $response->data = Card::listCard();
        } catch (NotFoundHttpException $e) {
            $response->data = [];
        }

        parent::run($container);
    }
}