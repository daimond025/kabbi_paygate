<?php

namespace v0\handlers\card;

use \v0\handlers\base\BaseHandler;
use v0\models\Card;


class DeleteCardOnPaygateHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        
        $pan = $request->get('pan');
        Card::unbind($pan);
        
        parent::run($container);
    }
}