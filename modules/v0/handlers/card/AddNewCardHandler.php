<?php

namespace v0\handlers\card;

use \v0\handlers\base\BaseHandler;
use v0\models\Card;


class AddNewCardHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        $result = Card::add($request->post('currency'));
        parent::run($container);

        $container->get('response')->data = $result;
    }
}