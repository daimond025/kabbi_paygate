<?php

namespace v0\handlers\card;

use \v0\handlers\base\BaseHandler;
use v0\models\Card;
use yandex\models\Profile;
use yii\web\NotFoundHttpException;

class CheckNewCardHandler extends BaseHandler
{

    public function run(\yii\di\Container $container)
    {


        // without first payment refund
        if ($container->get('paygateProfile') instanceof Profile
            || $container->get('paygateProfile') instanceof \paycom\models\Profile
            || $container->get('paygateProfile') instanceof \bepaid\models\Profile) {
            $container->get('response')->setStatusCode(202);
        } else {

            $response = $container->get('response');
            try {

                $orderId = $container->get('request')->post('orderId');
                $pan= $container->get('request')->post('pan');

                $param = isset($pan) ? $pan : $orderId;

                $response->data = Card::check($param);

            } catch (NotFoundHttpException $e) {
                $response->data = false;
            }
        }

        parent::run($container);
    }
}