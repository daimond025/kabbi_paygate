<?php

namespace v0\handlers\base;

use v0\Module as V0;

class SendSyslogHandler extends BaseHandler
{
    
    public $message = '';
    
    public function run(\yii\di\Container $container)
    {
        /* @var $syslog \v0\components\syslog\Syslog */
        $syslog = V0::getComponent('syslog');
        try {
            parent::run($container);
            if ($this->message instanceof \Closure) {
                $message = call_user_func($this->message, $container);

                $syslog->sendLog($message);
            } else {
                $syslog->sendLog($this->message);
            }
        } catch (\yii\web\HttpException $e) {
            $syslog->sendLog($e);
            throw $e;
        }
    }
    
    
}