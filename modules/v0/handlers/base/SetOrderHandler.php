<?php

namespace v0\handlers\base;

use v0\models\PaymentOrder;
use app\components\exception\InvalidArgumentException;

class SetOrderHandler extends BaseHandler
{
    
    public $scenario;
    
    public function run(\yii\di\Container $container)
    {
        $order = new PaymentOrder();
        if ($this->scenario) {
            $order->scenario = $this->scenario;
        }
        
        $order->load($container->get('request')->post(), '');

        if (! $order->validate()) {
            $errors = $order->getFirstErrors();
            throw new InvalidArgumentException(array_shift($errors));
        }
        
        $container->set('order', $order);
        
        parent::run($container);
    }
}