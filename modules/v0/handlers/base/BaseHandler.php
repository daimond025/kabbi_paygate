<?php

namespace v0\handlers\base;

use yii\base\Object;
use v0\handlers\Runnable;

class BaseHandler extends Object implements Runnable
{
    /**
     * Parent decorator
     * @var Runnable
     */
    protected $_parent;
    
    public function __construct(Runnable $parent, $config = [])
    {
        $this->_parent = $parent;
        parent::__construct($config);
    }
    
    public function run(\yii\di\Container $container)
    {
        $this->_parent->run($container);
    }
}