<?php

namespace v0\handlers\base;

use v0\models\Customer;
use app\components\exception\InvalidArgumentException;

class SetCustomerHandler extends BaseHandler
{
    public function run(\yii\di\Container $container)
    {
        /* @var $request yii\web\Request */
        $request = $container->get('request');
        
        $client = new Customer();
        $client->clientId = $request->get('clientId');

        if (! $client->validate()) {
            throw new InvalidArgumentException($client->getFirstError('clientId'));
        }

        $container->set('customer', $client);
        
        parent::run($container);
    }
}