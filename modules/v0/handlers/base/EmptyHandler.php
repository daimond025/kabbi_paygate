<?php

namespace v0\handlers\base;

class EmptyHandler extends BaseHandler
{
    
    public function __construct($config = [])
    {}

    public function run(\yii\di\Container $container) 
    {}
}