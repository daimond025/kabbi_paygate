<?php

namespace v0\handlers\base;

use v0\Module as V0;
use yii\di\Container;
use yii\web\Request;

/**
 * Class SetLocaleHandler
 * @package v0\handlers\base
 */
class SetLocaleHandler extends BaseHandler
{
    const AVAILABLE_LANGUAGES = ['en', 'ru', 'de'];

    public function run(Container $container)
    {
        /* @var $request Request */
        $request = $container->get('request');
        $locale = $this->getLocale($request);
        $container->set('locale', function () use ($locale) {
            return $locale;
        });

        parent::run($container);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getlocale($request)
    {
        $language = explode('-', $request->getPreferredLanguage(self::AVAILABLE_LANGUAGES));

        return current($language);
    }
}