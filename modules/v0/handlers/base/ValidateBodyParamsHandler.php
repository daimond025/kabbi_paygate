<?php

namespace v0\handlers\base;

use app\components\exception\NotAcceptableHttpException;

class ValidateBodyParamsHandler extends BaseHandler
{
    public function run(\yii\di\Container $container)
    {
        $bodyParams = $container->get('request')->post();
        if (empty($bodyParams)) {
            throw new NotAcceptableHttpException('Body request is empty');
        }
        
        parent::run($container);
    }
}