<?php

namespace v0\handlers\base;

use v0\Module as V0;
use app\components\exception\ServerErrorHttpException;
use yii\di\Container;

/**
 * Class SetPaygateHandler
 * @package v0\handlers\base
 */
class SetPaygateHandler extends BaseHandler
{
    const DEFAULT_LANG = "en";

    public function run(Container $container)
    {
        /* @var $paygateProfile \v0\models\PaymentGateProfile */
        $paygateProfile = $container->get('paygateProfile');
        if (!$paygateProfile) {
            throw new ServerErrorHttpException('No set paygate profile on container');
        }

        /* @var $customer \v0\models\Customer */
        $customer = $container->get('customer');
        if (!$customer) {
            throw new ServerErrorHttpException('No set customer on container');
        }

        $locale = self::DEFAULT_LANG;
        if ($container->has('locale')) {
            /* @var $locale string */
            $locale = $container->get('locale');
        }

        V0::getComponent('payment')->setPayment($paygateProfile, $customer, $locale);

        parent::run($container);
    }

}