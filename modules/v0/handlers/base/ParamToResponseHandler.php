<?php

namespace v0\handlers\base;

use app\components\exception\ServerErrorHttpException;

class ParamToResponseHandler extends BaseHandler
{
    
    public $paramName;

    public function run(\yii\di\Container $container) 
    {
        if ($this->paramName && $container->has($this->paramName)) {
            $container->get('response')->data = $container->get($this->paramName);
        } else {
            throw new ServerErrorHttpException("No exists param='{$this->paramName}'");
        }
    }
}