<?php

namespace stripe\assets;

use yii\web\AssetBundle;


class StripeAssets extends AssetBundle
{
    public $sourcePath = '@stripe/assets';

    public $js = [
        'https://js.stripe.com/v3/',
        'source/custom_stripe.js',
    ];

    public $css = [
        'source/style.css',
        'custom/styles/css/main.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}