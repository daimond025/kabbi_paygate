function stripeInit(token, locale) {

    // Create a Stripe client
    var stripe = Stripe(token);

// Create an instance of Elements
    var elements = stripe.elements({
        locale: locale || 'auto'
    });

    var elementStyles = {
        base: {
            color: '#000',
            fontWeight: 400,
            fontFamily: 'sans-serif',
            fontSize: '16px'
        }
    };

    var cardNumber = elements.create('cardNumber', {
        style: elementStyles,
       // hideIcon: false
    });
    cardNumber.mount('#card-number');

    var cardExpiry = elements.create('cardExpiry', {
        style: elementStyles,
        hideIcon: true
    });
    cardExpiry.mount('#card-expiry');

    var cardCvc = elements.create('cardCvc', {
        style: elementStyles,
        hideIcon: true
    });
    cardCvc.mount('#card-cvc');


// Handle real-time validation errors from the card Element.

    function showError(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    }


    cardNumber.addEventListener('change', showError);
    cardExpiry.addEventListener('change', showError);
    cardCvc.addEventListener('change', showError);

// Handle form submission
    var processing = false;

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        if (!processing) {
            processing = true;

            stripe.createToken(cardNumber).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;

                    processing = false;
                } else {
                    // Send the token to your server
                    stripeTokenHandler(result.token);
                }
            });
        }
    });


    function stripeTokenHandler(token) {
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }
}