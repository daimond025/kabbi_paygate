<?php

namespace stripe\components;

use stripe\models\Charge;
use stripe\models\Payment;
use yii\base\Object;
use stripe\models\Profile;
use stripe\models\Customer;
use stripe\exceptions\NotFoundProfileException;
use stripe\exceptions\NotFoundCustomerException;
use yii\log\Logger;

class StripeFacade extends Object
{

    private $_profile;
    /**
     * @var PaymentManager
     */
    private $paymentManager;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(PaymentManager $paymentManager, Logger $logger, Profile $profile = null, $config = [])
    {
        $this->_profile = $profile;
        $this->paymentManager = $paymentManager;
        $this->logger = $logger;
    }

    public function setProfile($paymentName)
    {
        $this->_profile = Profile::findOne(['paymentName' => $paymentName]);
    }

    /**
     *
     * @throws NotFoundProfileException
     * @return Profile
     */
    public function getProfile()
    {
        if ($this->_profile) {
            return $this->_profile;
        } else {
            throw new NotFoundProfileException('No set profile');
        }
    }

    public function getUrlForRegistryCard($clientId, $locale)
    {
        $url = \Yii::$app->params['my.externalUrl'];
        return sprintf('%sv0/stripe/%s/%s/%s', $url, $this->getProfile()->paymentName, $clientId, $locale);
    }

    /**
     *
     * @param string $orderNumber
     * @param integer $amount
     * @param string $currency Symbolic code
     * @param string $clientId
     * @param string $stripeToken
     * @param string $description
     *
     * @param null $stripeAccount
     * @return string
     */
    public function charge(
        $orderNumber,
        $amount,
        $currency,
        $clientId,
        $stripeToken,
        $description,
        $stripeAccount = null
    ) {
        /*@var $customer \stripe\models\Customer */
        if ($customer = Customer::get($clientId, $this->getProfile())) {
            $card = $customer->getCard($stripeToken);


            $charge = $customer->createCharge($card);
            $charge->setAttributes([
                'orderNumber'   => $orderNumber,
                'amount'        => $amount,
                'currency'      => $currency,
                'description'   => $description,
                'stripeAccount' => $stripeAccount,
            ]);

            $paymentId = $charge->pay();
            $this->savePayment($charge, $paymentId);

            return $paymentId;
        }
        throw new NotFoundCustomerException();
    }

    public function refundEntireCharge($clientId, $orderId)
    {
        /*@var $customer \stripe\models\Customer */
        if ($customer = Customer::get($clientId, $this->getProfile())) {
            $stripeAccount = $this->getStripeAccount($orderId);

            $charge = $customer->getCharge($orderId, $stripeAccount);

            $refund = $charge->createRefund();
            $refund->reason = 'requested_by_customer';
            $refund->refund();

            return true;
        }

        return false;
    }

    public function getStatusCharge($clientId, $orderId)
    {
        /*@var $customer \stripe\models\Customer */
        if ($customer = Customer::get($clientId, $this->getProfile())) {

            $stripeAccount = $this->getStripeAccount($orderId);
            return $customer->getCharge($orderId, $stripeAccount);
        }
    }

    public function getCardList($clientId)
    {
        /*@var $customer \stripe\models\Customer */
        if ($customer = Customer::get($clientId, $this->getProfile())) {
            return $customer->getCardList();
        }

        return [];
    }

    public function deleteCard($clientId, $cardToken)
    {
        /*@var $customer \stripe\models\Customer */
        if ($customer = Customer::get($clientId, $this->getProfile())) {
            $card = $customer->getCard($cardToken);
            $card->delete();
        }

        return true;
    }

    public function getAuthorizeUrl($state = null)
    {
        $api = $this->getProfile()->getApi();

        return $api->getAuthorizeUrl($state);
    }

    public function getAccountId($code)
    {
        $api = $this->getProfile()->getApi();

        return $api->getAccountId($code);
    }

    public function deAuthorize($accountId)
    {
        $api = $this->getProfile()->getApi();
        $api->deAuthorize($accountId);
    }

    private function savePayment(Charge $charge, $paymentId)
    {
        try {
            $payment = new Payment($this->_profile->paymentName, $charge->getCustomer()->clientId, $charge->orderNumber,
                $paymentId, $charge->stripeAccount, $charge->amount, $charge->currency);
            $this->paymentManager->savePayment($payment);

        } catch (\Exception $e) {
            $this->logger->log("Save payment error: paymentName=\"{$this->_profile->paymentName}\", clientId=\"{$charge->getCustomer()->clientId}\", orderNumber=\"{$charge->orderNumber}\", paymentId=\"{$paymentId}\", error={$e->getMessage()}",
                Logger::LEVEL_ERROR);
        }
    }

    private function getStripeAccount($paymentId)
    {
        try {

            $payment = $this->paymentManager->getPaymentById($paymentId);
            return $payment->getStripeAccount();
        } catch (\Exception $e) {
            $this->logger->log("Get stripe account error: paymentId={$paymentId}", Logger::LEVEL_ERROR);
        }
    }
}