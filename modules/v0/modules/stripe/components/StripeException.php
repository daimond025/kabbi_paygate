<?php
namespace stripe\components;

class StripeException extends \yii\web\HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        if (is_null($message)) {
            $message = 'The payment gate internal error';
        }
        parent::__construct(424, $message, 4, $previous);
    }
    
    public function getName()
    {
        return 'Stripe Exception';
    }
    
}