<?php

namespace stripe\components;

use stripe\exceptions\PaymentException;
use stripe\models\Payment;
use stripe\models\PaymentDAO;

/**
 * Class PaymentManager
 * @package stripe\components
 */
class PaymentManager
{
    /**
     * @param string $paymentName
     * @param string $clientId
     * @param string $orderNumber
     *
     * @return Payment
     * @throws PaymentException
     */
    public function getPayment($paymentName, $clientId, $orderNumber)
    {
        $model = PaymentDAO::find()->where([
            'paymentName' => $paymentName,
            'clientId'    => $clientId,
            'orderNumber' => $orderNumber,
        ])->one();
        if (empty($model)) {
            throw new PaymentException("Payment not found: paymentName={$paymentName}, clientId={$clientId}, orderNumber={$orderNumber}");
        }

        return $this->createModel($model);
    }

    /**
     * @param string $paymentId
     *
     * @return Payment
     * @throws PaymentException
     */
    public function getPaymentById($paymentId)
    {
        $model = PaymentDAO::find()->where(['paymentId' => $paymentId])->one();
        if (empty($model)) {
            throw new PaymentException("Payment not found: paymentId={$paymentId}");
        }

        return $this->createModel($model);
    }

    /**
     * @param Payment $payment
     *
     * @throws PaymentException
     */
    public function savePayment(Payment $payment)
    {
        $model = new PaymentDAO([
            'paymentName'   => $payment->getPaymentName(),
            'clientId'      => $payment->getClientId(),
            'orderNumber'   => $payment->getOrderNumber(),
            'paymentId'     => $payment->getPaymentId(),
            'stripeAccount' => $payment->getStripeAccount(),
            'amount'        => $payment->getAmount(),
            'currency'      => $payment->getCurrency(),
        ]);

        if (!$model->save()) {
            throw new PaymentException('Save payment to DB error: error="' . implode('; ',
                    $model->getFirstErrors()) . '"');
        }
    }

    /**
     * @param PaymentDAO $model
     *
     * @return Payment
     */
    private function createModel($model)
    {
        return new Payment($model->paymentName, $model->clientId, $model->orderNumber, $model->paymentId,
            $model->stripeAccount, $model->amount, $model->currency);
    }
}