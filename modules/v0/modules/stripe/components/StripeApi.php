<?php

namespace stripe\components;

use Stripe\Error\OAuth\InvalidClient;
use stripe\models\Customer;
use stripe\models\Refund;
use Stripe\OAuth;
use Stripe\Stripe;
use Stripe\Token;
use yii\base\Object;
use stripe\models\Charge;
use app\components\exception\ServerErrorHttpException;

class StripeApi extends Object
{
    private $_profile;

    public function __construct(\stripe\models\Profile $profile, $config = [])
    {
        $this->_profile = $profile;
        parent::__construct($config);
    }

    public function init()
    {

        parent::init();
        $this->stripeRequest(function ($key) {
            \Stripe\Stripe::setApiKey($key);
            //\Stripe\Stripe::setApiVersion("2017-08-15");
            \Stripe\Stripe::setApiVersion("2019-12-03");

        }, [$this->_profile->privateKey]);
    }

    public function createCustomer(Customer $customer)
    {
        return $this->stripeRequest(function ($params) {

            return \Stripe\Customer::create($params);
        }, [
            [
               // 'business_vat_id' => $customer->clientId,
                "description"     => "Customer #{$customer->clientId} for {$this->_profile->paymentName}",
            ],
        ]);
    }

    public function addCardToCustomer(\stripe\models\Card $card, Customer $customer)
    {
        $this->stripeRequest(function ($params) {
            list ($customerId, $sourceToken) = $params;

            \Stripe\Customer::retrieve($customerId)->sources->create([
                'source' => $sourceToken,
            ]);
        }, [
            [
                $customer->stripeCustomerId,
                $card->stripeToken,
            ],
        ]);
    }

    /**
     * @param Customer $customer
     *
     * @return \stripe\models\Card[]
     */
    public function getCardList(Customer $customer)
    {
        $cardList = $this->stripeRequest(function ($customerId) {

            return \Stripe\Customer::retrieve($customerId)->sources->all([
                'object' => 'card',
            ]);
        }, [$customer->stripeCustomerId])->data;

        $result = [];
        foreach ($cardList as $stripeCard) {
            $card = new \stripe\models\Card($customer);
            $card->setAttributes([
                'stripeToken' => $stripeCard->id,
                'clientId'    => $customer->clientId,
                'maskedPan'   => sprintf('****%s', $stripeCard->last4),
            ]);
            $result[] = $card;
        }


        return $result;
    }

    public function deleteCard(\stripe\models\Card $card, Customer $customer)
    {
        $this->stripeRequest(function ($params) {
            list ($customerId, $sourceToken) = $params;
            $cus = \Stripe\Customer::retrieve($customerId);

            $cus->sources->retrieve($sourceToken)
                ->delete();
        }, [
            [
                $customer->stripeCustomerId,
                $card->stripeToken,
            ],
        ]);
    }

    /**
     *
     * @param \stripe\models\Charge $charge
     *
     * @return string
     */
    public function createCharge(\stripe\models\Charge $charge)
    {
       /* $params = [
            'amount'      => $charge->amount,
            'currency'    => $charge->currency,
            'source'      => $charge->getCard()->stripeToken,
            'description' => $charge->description,
            'metadata'    => [
                'orderNumber'   => $charge->orderNumber,
                'stripeAccount' => $charge->stripeAccount,
            ],
        ];
        $options = [
            'idempotency_key' => md5($charge->orderNumber),
        ];

        if ($charge->stripeAccount === null) {
            $params['customer'] = $charge->getCustomer()->stripeCustomerId;
        } else {
           // $token = $this->createToken($charge);
            $params['source'] = $charge->getCard()->stripeToken; // $token;
            $options['stripe_account'] = $charge->stripeAccount;

            //$params['customer'] = $charge->getCustomer()->stripeCustomerId;
        }*/

        // TODO NEW API
        $new_params = [
            // описание платежа
            'amount'      => $charge->amount,
            'currency'    => $charge->currency,

            // источник платежа
            'customer' => $charge->getCustomer()->stripeCustomerId,
            'description' => $charge->description,

            'source' =>  $charge->getCard()->stripeToken,
            'confirm' => true,

            'metadata'    => [
                'orderNumber'   => $charge->orderNumber,
            ],
        ];

        if($charge->stripeAccount){
            $new_params = array_merge($new_params, [

                'on_behalf_of' => $charge->stripeAccount,
                'transfer_data' => [
                    'destination' => $charge->stripeAccount,
                ],

                'metadata'    => [
                    'stripeAccount' => $charge->stripeAccount,
                    'orderNumber'   => $charge->orderNumber,
                ],
            ]);
        }

        $options = [];
       // $options['idempotency_key'] = md5($charge->orderNumber);



        $charge = $this->stripeRequest(function ($params, $options = null) {


           $response = \Stripe\OAuth::token([
                'grant_type' => 'authorization_code',
                'code' => 'ac_GrQgvs8nKFWVwuhjhii1lk0QNsjmHLOb',
            ]);



            $payment_intent = \Stripe\PaymentIntent::create($params, $options);
            /*
            $payment_intent->confirm([
                'source' => $options['source'],
            ],[
              //  'idempotency_key' => $options['key'],
            ]);*/



            $payment_intent = \Stripe\PaymentIntent::create($params, ['stripe_account' => $options['stripe_account']]);

            $pay = $payment_intent->confirm([
                'source' => $options['source'],
                ], [
                //'idempotency_key' => $options['key'],
            ]);

            return $pay;


            //return \Stripe\Charge::create($params, $options);
        }, [$new_params, $options]);

        return $charge->id;
    }

    /**
     * @param Customer $customer
     * @param $id
     * @param null $stripeAccount
     *
     * @return Charge
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function retrieveCharge(Customer $customer, $id, $stripeAccount = null)
    {
        $options = [];
        if ($stripeAccount !== null) {
            $options['stripe_account'] = $stripeAccount;
        }

        $payIntent = $this->stripeRequest(function ($id, $options) {
            return \Stripe\PaymentIntent::retrieve($id );
            //return \Stripe\Charge::retrieve($id, $options);
        }, [$id, $options]);


        $charge = (count($payIntent->charges->data) > 0) ? current($payIntent->charges->data) : [];


        //$card = $customer->getCard($charge->source->id, $charge->source->last4);
        $card = $customer->getCard($id, $charge->source->last4);

        $chargeModel = new Charge($customer, $card);
        $chargeModel->setAttributes([
            'id'            => $payIntent->id,
            'amount'        => $charge->amount,
            'currency'      => $charge->currency,
            'description'   => $charge->description,
            'captured'      => $charge->captured,
            'refunded'      => $charge->refunded,
            'stripeAccount' => $stripeAccount,
        ]);


        return $chargeModel;
    }

    /**
     * @param Refund $refund
     *
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function createRefund(Refund $refund)
    {
        $params = [
            //'charge' => $refund->getCharge()->id,
            'payment_intent' => $refund->getCharge()->id,
            'reason' => $refund->reason,
        ];
        if ($refund->amount) {
            $params['amount'] = $refund->amount;
        }

        $options = [];
        if ($refund->getCharge()->stripeAccount !== null) {
            $options['stripe_account'] = $refund->getCharge()->stripeAccount;
        }


        $this->stripeRequest(function ($params, $options) {
           // \Stripe\Refund::create($params);

            \Stripe\Refund::create($params, $options);
        }, [$params, $options]);
    }

    /**
     * @param Charge $charge
     *
     * @return string
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function createToken(Charge $charge)
    {
        $params = ['customer' => $charge->getCustomer()->stripeCustomerId];
        $options = ['stripe_account' => $charge->stripeAccount];

        /** @var Token $token */
        $token = $this->stripeRequest(function ($params, $options) {
            return Token::create($params, $options);
        }, [$params, $options]);

        return $token->id;
    }

    /**
     * @param string|null $state\Stripe\Account
     *
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function getAuthorizeUrl($state = null)
    {
        $params = [
            'client_id'     => $this->_profile->clientId,
            'response_type' => 'code',
            'scope'         => 'read_write',
        ];

        if ($state !== null) {
            $params['state'] = $state;
        }

        $url = $this->stripeRequest(function ($params, $options = null) {
            return OAuth::authorizeUrl($params, $options);
        }, [$params]);

        return $url;
    }

    /**
     * @param string $code
     *
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function getAccountId($code)
    {
        $token = $this->stripeRequest(function ($code) {
            return OAuth::token([
                'grant_type' => 'authorization_code',
                'code'       => $code,
            ]);
        }, [$code]);

        return $token->stripe_user_id;
    }

    /**
     * @param string $accountId
     *
     * @throws ServerErrorHttpException
     * @throws StripeException
     */
    public function deAuthorize($accountId)
    {
        $this->stripeRequest(function ($accountId) {
            return OAuth::deauthorize([
                'client_id'      => $this->_profile->clientId,
                'stripe_user_id' => $accountId,
            ]);
        }, [$accountId]);
    }

    /**
     * @param $callback
     * @param array $params
     *
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws StripeException
     * @throws InvalidAccountException
     */
    private function stripeRequest($callback, $params = [])
    {
        try {
            return call_user_func_array($callback, $params);
        } catch (InvalidClient $e) {
            throw new InvalidAccountException("Invalid account: error={$e->getMessage()}");
        } catch (\Stripe\Error\Card $e) {
            throw new StripeException("Card error on stripe: error={$e->getMessage()}", $e);
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            throw new StripeException("Repeat request later: error={$e->getMessage()}", $e);
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            throw new ServerErrorHttpException("Error params on request: error={$e->getMessage()}", $e);
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            throw new StripeException("Error on token stripe: error={$e->getMessage()}", $e);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            throw new StripeException("Not connect to stripe server: error={$e->getMessage()}", $e);
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            throw new ServerErrorHttpException("Inner error on stripe: error={$e->getMessage()}", $e);
        } catch (\Exception $e) {
            // Something else happened, completely unrelated to Stripe
            throw new ServerErrorHttpException("Illegal error on stripe: error={$e->getMessage()}", $e);
        }
    }
}