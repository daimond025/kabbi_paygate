<?php

namespace stripe\components;

class InvalidAccountException extends \yii\web\HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        parent::__construct(400, $message, 4, $previous);
    }
}