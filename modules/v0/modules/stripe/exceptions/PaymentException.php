<?php

namespace stripe\exceptions;

use yii\web\HttpException;

class PaymentException extends HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        if (null === $message) {
            $message = 'Stripe payment exception';
        }
        parent::__construct(500, $message, 2, $previous);
    }

    public function getName()
    {
        return 'Stripe payment exception';
    }

}