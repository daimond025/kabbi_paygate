<?php
namespace stripe\exceptions;

class NotFoundProfileException extends \yii\web\HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        if (is_null($message)) {
            $message = 'Not found profile';
        }
        parent::__construct(400, $message, 2, $previous);
    }
    
    public function getName()
    {
        return 'Not found profile';
    }
    
}