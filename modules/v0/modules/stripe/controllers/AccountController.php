<?php

namespace stripe\controllers;

use app\components\exception\InvalidArgumentException;
use League\Uri\Modifiers\MergeQuery;
use League\Uri\Schemes\Http;
use stripe\components\StripeFacade;
use stripe\Module as Stripe;
use stripe\Module;
use yii\base\InvalidConfigException;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class AccountController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }

    public function actionAuthorize()
    {
        $code = \Yii::$app->request->get('code', '');
        $state = \Yii::$app->request->get('state');

        try {
            $data = json_decode(base64_decode($state, true), true);
            $payment = $this->getPayment($data['paymentName']);
            $url = isset($data['redirectUrl']) ? $data['redirectUrl'] : '';
            $accountId = $payment->getAccountId($code);

            return $this->redirect($this->getRedirectUrl($url, $accountId));
        } catch (\Exception $e) {
            \Yii::error("Stripe authorize: error=\"{$e->getMessage()}\", state={$state}");
        }
    }

    /**
     * @param string $paymentName
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionGetAuthorizeUrl($paymentName)
    {
        $payment = $this->getPayment($paymentName);
        $redirectUrl = \Yii::$app->request->post('redirectUrl');
        $state = base64_encode(json_encode(compact('paymentName', 'redirectUrl')));

        return ['url' => $payment->getAuthorizeUrl($state)];
    }

    /**
     * @param string $paymentName
     * @param string $accountId
     *
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function actionDeAuthorize($paymentName, $accountId)
    {
        $payment = $this->getPayment($paymentName);
        $payment->deAuthorize($accountId);

        \Yii::$app->response->statusCode = 204;
    }

    /**
     * @param string $url
     * @param string $accountId
     *
     * @return string
     */
    private function getRedirectUrl($url, $accountId)
    {
        $parsedUrl = Http::createFromString($url);
        $query = new MergeQuery("accountId={$accountId}");

        return (string)$query($parsedUrl);
    }

    /**
     * @param string $paymentName
     * @return StripeFacade
     *
     * @throws InvalidConfigException
     */
    private function getPayment($paymentName)
    {
        /** @var StripeFacade $payment */
        $payment = Module::getInstance()->get('payment');
        $payment->setProfile($paymentName);

        return $payment;
    }
}