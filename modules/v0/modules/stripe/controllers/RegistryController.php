<?php

namespace stripe\controllers;

use stripe\Module as Stripe;
use yii\base\Exception;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;
use stripe\models\Profile;

class RegistryController extends Controller
{

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    '*/*' => Response::FORMAT_HTML,
                ],
            ],
        ];
    }

    public function actionCard($paymentName, $clientId, $locale)
    {
        $profile = Profile::get($paymentName);
        $customer = $profile->getCustomer($clientId);

        $locale = $this->filterLocale($locale);
        \Yii::$app->language = $locale;

        if ($card = $customer->createCard(\Yii::$app->request->post())) {

            try {
                $customer->addCard($card);

                return $this->redirect("/v0/stripe/success/$locale");
            } catch (Exception $ex) {
                \Yii::error("Create card error (stripe): error=\"{$ex->getMessage()}\"");
            }

            return $this->redirect("/v0/stripe/fail/$locale");
        }

        return $this->render('start', [
            'publicToken' => $profile->publicKey,
            'locale'      => $locale === Stripe::LOCALE_RU ? Stripe::LOCALE_DEFAULT : $locale,
        ]);
    }

    public function actionSuccess($locale)
    {
        $locale = $this->filterLocale($locale);
        \Yii::$app->language = $locale;

        return $this->render('success_complete');
    }

    public function actionFail($locale)
    {
        $locale = $this->filterLocale($locale);
        \Yii::$app->language = $locale;

        return $this->render('fail_complete');
    }

    private function filterLocale($locale)
    {
        return in_array($locale, Stripe::AVAILABLE_LOCALES, true) ? $locale : Stripe::LOCALE_DEFAULT;
    }
}