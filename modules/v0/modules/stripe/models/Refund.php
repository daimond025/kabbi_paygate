<?php

namespace stripe\models;

class Refund extends \yii\base\Model
{
    /**
     * Default is entire charge (api)
     * @var integer
     */
    public $amount;
    
    public $reason;
    
    private $_charge;
    
    public function __construct(Charge $charge)
    {
        $this->_charge = $charge;
    }
    
    public function getCharge()
    {
        return $this->_charge;
    }
    
    public function rules()
    {
        return [
            ['charge', 'required'],
            ['amount', 'integer'],
            ['reason', 'string']
        ];
    }
    
    public function refund()
    {
        /* @var $api \stripe\components\StripeApi */
        $api = $this->_charge->customer->profile->getApi();
        $api->createRefund($this);
    }
    
}