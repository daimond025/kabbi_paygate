<?php

namespace stripe\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "stripe_payment".
 *
 * @property integer $id
 * @property string $paymentName
 * @property string $clientId
 * @property string $orderNumber
 * @property string $paymentId
 * @property string $stripeAccount
 * @property integer $amount
 * @property string $currency
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class PaymentDAO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stripe_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'clientId', 'orderNumber', 'paymentId'], 'required'],
            [['amount', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'clientId', 'orderNumber', 'stripeAccount'], 'string', 'max' => 32],
            [['paymentId', 'currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'paymentName'   => 'Payment Name',
            'clientId'      => 'Client ID',
            'orderNumber'   => 'Order Number',
            'paymentId'     => 'Payment ID',
            'stripeAccount' => 'Stripe Account',
            'amount'        => 'Amount',
            'currency'      => 'Currency',
            'createdAt'     => 'Created At',
            'updatedAt'     => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }
}
