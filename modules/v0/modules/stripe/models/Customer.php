<?php

namespace stripe\models;

class Customer extends CustomerDAO
{

    public static function get($id, Profile $profile)
    {
        return self::findOne([
            'clientId'  => $id,
            'profileId' => $profile->id,
        ]);
    }

    public static function create($id, Profile $profile)
    {
        $customer = new Customer([
            'clientId'  => $id,
            'profileId' => $profile->id,
        ]);
        $customer->stripeCustomerId = $profile->getApi()->createCustomer($customer)->id;
        $customer->save();
        return $customer;
    }

    /**
     *
     * @param array $params
     * @return \stripe\models\Card
     */
    public function createCard($params = [])
    {
        $card = new Card($this);

        if ($card->load($params, '') && $card->validate()) {
            return $card;
        }
    }

    public function addCard(Card $card)
    {
        $this->profile->getApi()->addCardToCustomer($card, $this);
    }

    /**
     * @return \stripe\models\Card[]
     */
    public function getCardList()
    {
        return $this->profile->getApi()->getCardList($this);
    }

    // TODO: This check exists on profile?
    public function getCard($stripeToken, $maskedPan = 'XXXX')
    {
        $card = new Card($this);
        $card->setAttributes([
            'stripeToken' => $stripeToken,
            'maskedPan'   => $maskedPan
        ]);
        return $card;
    }

    public function createCharge(Card $card)
    {
        return new Charge($this, $card);
    }

    public function getCharge($id, $stripeAccount = null)
    {
        /* @var $api \stripe\components\StripeApi */
        $api = $this->profile->getApi();
        return $api->retrieveCharge($this, $id, $stripeAccount);
    }

}