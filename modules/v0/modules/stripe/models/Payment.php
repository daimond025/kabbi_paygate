<?php

namespace stripe\models;

class Payment
{
    /**
     * @var string
     */
    private $paymentName;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var string
     */
    private $stripeAccount;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    public function __construct($paymentName, $clientId, $orderNumber, $paymentId, $stripeAccount, $amount, $currency)
    {
        $this->paymentName = $paymentName;
        $this->clientId = $clientId;
        $this->orderNumber = $orderNumber;
        $this->paymentId = $paymentId;
        $this->stripeAccount = $stripeAccount;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getStripeAccount()
    {
        return $this->stripeAccount;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}