<?php

namespace stripe\models;

use stripe\exceptions\NotFoundProfileException;
use stripe\components\StripeApi;

class Profile extends ProfileDAO
{
    
    /**
     * 
     * @param string $clientId
     * @return \stripe\models\Customer
     */
    public function getCustomer($clientId)
    {
        $customer = Customer::get($clientId, $this);
        if ($customer) {
            return $customer;
        }
        return Customer::create($clientId, $this);
    }
    
    /**
     * 
     * @return \stripe\components\StripeApi
     */
    public function getApi()
    {
        return new StripeApi($this);
    }
    
    /**
     * 
     * @param string $paymentName
     * @throws NotFoundProfileException
     * @return \stripe\models\Profile
     */
    public static function get($paymentName)
    {
        $profile = self::findOne(['paymentName' => $paymentName]);
        if (! $profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }
        return $profile;
    } 
}