<?php

namespace stripe\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;

/**
 *
 * @property integer $id
 * @property integer $typeId Value is 5
 * @property string $paymentName
 * @property string $publicKey
 * @property string $privateKey
 * @property string $clientId
 * @property float $commission
 *
 * @property PaymentGateType $type
 *
 */
class ProfileDAO extends \yii\db\ActiveRecord
{

    const TYPE_ID = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stripe_payment_gate_profile';
    }

    public function rules()
    {
        return [
            [['paymentName', 'publicKey', 'privateKey'], 'required'],
            [['privateKey', 'publicKey', 'clientId'], 'string', 'max' => 255],
            [['commission'], 'default', 'value' => 0],
            [
                'commission',
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    public function setTypeId()
    {
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'paymentName',
            'typeId',
            'publicKey',
            'privateKey',
            'clientId',
            'commission',
        ];
    }

}