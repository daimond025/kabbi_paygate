<?php

namespace stripe\models;

class Card extends \yii\base\Model
{
    
    public $maskedPan; 
    
    public $stripeToken;
    
    private $_customer;
    
    public function __construct(Customer $customer)
    {
        $this->_customer = $customer;
    }
    
    public function getCustomer()
    {
        return $this->_customer;
    }
    
    public function rules()
    {
        return [
            [['stripeToken'], 'required'],
            [['stripeToken', 'maskedPan'], 'string']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'stripeToken' => 'Stripe Token',
            'maskedPan' => 'Masked Pan',
        ];
    }
    
    public function getCharge()
    {
        return new Charge($this);
    }
    
    public function delete()
    {
        /* @var $api \stripe\components\StripeApi */
        $api = $this->_customer->profile->getApi();
        $api->deleteCard($this, $this->getCustomer());
    }
}