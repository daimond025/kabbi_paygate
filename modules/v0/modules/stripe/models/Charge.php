<?php

namespace stripe\models;

class Charge extends \yii\base\Model
{

    public $id;

    public $orderNumber;

    public $amount;

    public $currency;

    public $description;

    public $captured;

    public $refunded;

    public $stripeAccount;

    /**
     *
     * @var \stripe\models\Customer
     */
    private $_customer;

    /**
     *
     * @var \stripe\models\Card
     */
    private $_card;

    public function __construct(Customer $customer, Card $card)
    {
        $this->_customer = $customer;
        $this->_card = $card;
    }

    public function rules()
    {
        return [
            [['orderNumber', 'customer', 'card', 'amount', 'currency'], 'required'],
            [['amount'], 'integer'],
            ['currency', 'string', 'length' => 3],
            [['orderNumber', 'description', 'id', 'stripeAccount'], 'string'],
            [['captured', 'refunded'], 'safe'],
        ];
    }

    public function getCard()
    {
        return $this->_card;
    }

    public function getCustomer()
    {
        return $this->_customer;
    }

    public function pay()
    {
        /* @var $api \stripe\components\StripeApi */
        $api = $this->_customer->profile->getApi();
        return $api->createCharge($this);
    }

    public function createRefund()
    {
        return new Refund($this);
    }
} 