<?php

namespace stripe\models;

/**
 * 
 * @property integer $id
 * @property integer $profileId
 * @property string $clientId
 * @property string $stripeCustomerId
 *
 * @property \stripe\models\Profile $profile
 */
class CustomerDAO extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stripe_customer';
    }
    
    public function rules()
    {
        return [
            [['profileId', 'clientId', 'stripeCustomerId'], 'required'],
            [['id', 'profileId'], 'integer'],
            [['clientId', 'stripeCustomerId'], 'string'],
        ];
    }
    
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profileId']);
    }
    
}