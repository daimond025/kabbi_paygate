<?php

namespace stripe\models;

class CardForm extends Card
{
    
    public $number;
    
    public $expMonth;
    
    public $expYear;
    
    public $cvc;
    
    public function rules()
    {
        return [
            [['number', 'clientId', 'stripeToken'], 'required'],
            ['number', 'string', 'max' => 19, 'min' => 12],
            [['expMonth', 'expYear'], 'string', 'length' => 2],
            ['cvc', 'string', 'length' => 3],
            [['clientId', 'stripeToken'], 'string'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'number' => 'Card number',
            'expMonth' => 'Expirience month',
            'expYear' => 'Expirience year',
            'cvc' => 'CVC',
        ];
    }
}