<?php

use stripe\assets\StripeAssets;
use yii\helpers\Url;
use stripe\Module as Stripe;

/*@var $this \yii\web\View */
/*@var $publicToken string */
/*@var $locale string */

StripeAssets::register($this);

?>


    <div class="card-logos">
        <img src="/images/icons.svg">
        <p><?= Stripe::t('form', 'The payment is safe. No data is saved in the app.'); ?></p>
    </div>
    <div class="form">
        <form action="<?= Url::current() ?>" method="post" id="payment-form">
            <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->csrfToken ?>"/>
            <div class="form__card">
                <label><?= Stripe::t('form', 'Card Number'); ?></label>
                <div id="card-number" class="fake-input"></div>
            </div>
            <div class="form__cols">
                <div class="form__col">
                    <label><?= Stripe::t('form', 'Card Expiry'); ?></label>
                    <div id="card-expiry" class="fake-input"></div>
                </div>
                <div class="form__col">
                    <label><?= Stripe::t('form', 'CVC'); ?></label>
                    <div id="card-cvc" class="fake-input"></div>
                </div>
            </div>
            <div class="form__submit">
                <button type="submit"><?= Stripe::t('form', 'Next'); ?></button>
                <div id="card-errors" role="alert"></div>
                <p><?= Stripe::t('form',
                        'By clicking on "Next" you allow the bank the access to personal information.'); ?></p>
            </div>
        </form>
    </div>

<?php $this->registerJs("stripeInit('$publicToken', '$locale');", \yii\web\View::POS_END) ?>