<?php

use stripe\Module as Stripe;
use stripe\assets\StripeAssets;

StripeAssets::register($this);

?>

<div class="success">
    <img src="/images/success.svg">
    <p><?= Stripe::t('page', 'Your card was successfully added and you can use it for paying!') ?></p>
</div>