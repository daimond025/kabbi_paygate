<?php

use stripe\Module as Stripe;
use stripe\assets\StripeAssets;

StripeAssets::register($this);

?>

<div class="error">
    <img src="/images/error.svg">
    <p><?= Stripe::t('page', 'Error') ?>:<br>
        <?= Stripe::t('page',
            'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.') ?></p>
</div>