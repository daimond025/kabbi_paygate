<?php

namespace stripe;

use yii\i18n\PhpMessageSource;

class Module extends \yii\base\Module
{
    const LOCALE_DEFAULT = 'en';
    const LOCALE_RU = 'ru';
    const AVAILABLE_LOCALES = ['en', 'ru', 'de'];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'stripe\controllers';

    public function init()
    {
        parent::init();

        $this->components = [
            'paymentGateProfile' => 'stripe\models\ProfileDAO',
            'payment'            => 'stripe\components\StripeFacade',
        ];
        $this->layout = 'stripe';
        \Yii::$app->layout = false;

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['stripe/*'] = [
            'class'          => PhpMessageSource::class,
            'sourceLanguage' => 'en-US',
            'basePath'       => '@stripe/messages',
            'fileMap'        => [
                'stripe/form' => 'form.php',
                'stripe/page' => 'page.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('stripe/' . $category, $message, $params, $language);
    }

}