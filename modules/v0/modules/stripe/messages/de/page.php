<?php

return [
    'Error'                                                                                             => 'Es ist ein Fehler aufgetreten',
    'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.' => 'Die Daten der Kreditkarte wurden falsch eingegeben oder der Service steht momentan nicht zur Verfügung. Überprüfe deine Eingaben oder versuche es später noch einmal.',
    'Your card was successfully added and you can use it for paying!'                                   => 'Deine Kreditkarte wurde erfolgreich hinzugefügt und kann nun zur Bezahlung genutzt werden!',
];