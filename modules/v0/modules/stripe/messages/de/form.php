<?php

return [
    'The payment is safe. No data is saved in the app.'                            => 'Die Zahlung läuft sicher ab. Es werden keine Daten in der App gespeichert.',
    'Card Number'                                                                  => 'Kartennummer',
    'Card Expiry'                                                                  => 'Gültig bis',
    'CVC'                                                                          => 'CVC',
    'Next'                                                                         => 'Weiter',
    'By clicking on "Next" you allow the bank the access to personal information.' => 'Mit einem Knopfdruck auf „Weiter“ erlaubst du der Bank das Bearbeiten von persönlichen Angaben.',
];