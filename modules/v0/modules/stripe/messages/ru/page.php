<?php

return [
    'Error'                                                                                             => 'Произошла ошибка',
    'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.' => 'Данные карты введены неправильно или сервис временно не работает. Проверьте данные или попробуйте позже.',
    'Your card was successfully added and you can use it for paying!'                                   => 'Ваша карта была успешно добавлена и вы можете использовать её для оплаты!',
];