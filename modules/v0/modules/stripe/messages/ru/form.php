<?php

return [
    'The payment is safe. No data is saved in the app.'                            => 'Совершение платежа безопасно. Данные карты не сохраняются в приложении.',
    'Card Number'                                                                  => 'Номер карты',
    'Card Expiry'                                                                  => 'Срок действия',
    'CVC'                                                                          => 'CVC',
    'Next'                                                                         => 'Продолжить',
    'By clicking on "Next" you allow the bank the access to personal information.' => 'Нажимая кнопку "Продолжить", вы даёте согласие банку на обработку персональных данных.',
];