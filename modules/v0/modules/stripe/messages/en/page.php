<?php

return [
    'Error'                                                                                             => 'Error',
    'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.' => 'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.',
    'Your card was successfully added and you can use it for paying!'                                   => 'Your card was successfully added and you can use it for paying!',
];