<?php

return [
    'The payment is safe. No data is saved in the app.'                            => 'The payment is safe. No data is saved in the app.',
    'Card Number'                                                                  => 'Card number',
    'Card Expiry'                                                                  => 'Card expiry',
    'CVC'                                                                          => 'CVC',
    'Next'                                                                         => 'Next',
    'By clicking on "Next" you allow the bank the access to personal information.' => 'By clicking on "Next" you allow the bank the access to personal information.',
];