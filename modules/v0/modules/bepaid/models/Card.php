<?php

namespace bepaid\models;

/**
 * Class Card
 * @package bepaid\models
 */
class Card
{
    /**
     * @var string
     */
    private $pan;

    /**
     * @var string
     */
    private $token;

    /**
     * Card constructor.
     *
     * @param string $pan
     * @param string $token
     */
    public function __construct($pan, $token)
    {
        $this->pan = $pan;
        $this->token = $token;
    }

    public static function generatePan($first1, $last4)
    {
        return "$first1****$last4";
    }

    /**
     * @return string
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}