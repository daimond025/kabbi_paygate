<?php

namespace bepaid\models;

class Payment
{
    const STATUS_SUCCESSFUL = 'successful';

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $order;

    /**
     * @var string
     */
    private $status;

    /**
     * @var bool
     */
    private $isRefunded;

    /**
     * @var Card|null
     */
    private $card;

    /**
     * @var int
     */
    private $amount;
    /**
     * @var int
     */
    private $currency;

    public function __construct($clientId, $order, $status, $isRefunded, $card, $amount, $currency)
    {
        $this->clientId = $clientId;
        $this->order = $order;
        $this->status = $status;
        $this->isRefunded = $isRefunded;
        $this->card = $card;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isRefunded()
    {
        return $this->isRefunded;
    }

    /**
     * @return null|Card
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->status === self::STATUS_SUCCESSFUL;
    }
}