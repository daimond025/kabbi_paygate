<?php

namespace bepaid\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bepaid_payment".
 *
 * @property integer $id
 * @property string $paymentName
 * @property string $clientId
 * @property string $orderNumber
 * @property string $uid
 * @property integer $amount
 * @property integer $currency
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class PaymentDAO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bepaid_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'clientId', 'orderNumber', 'uid'], 'required'],
            [['amount', 'currency', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'clientId', 'orderNumber'], 'string', 'max' => 32],
            [['uid'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'clientId'    => 'Client ID',
            'orderNumber' => 'Order Number',
            'uid'         => 'Uid',
            'amount'      => 'Amount',
            'currency'    => 'Currency',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }
}
