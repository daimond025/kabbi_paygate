<?php

namespace bepaid\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "bepaid_card".
 *
 * @property integer $id
 * @property string $paymentName
 * @property string $clientId
 * @property string $pan
 * @property string $token
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class CardDAO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bepaid_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'clientId', 'pan', 'token'], 'required'],
            [['createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'clientId', 'pan'], 'string', 'max' => 32],
            [['token'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'clientId'    => 'Client ID',
            'pan'         => 'Pan',
            'token'       => 'Token',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }
}
