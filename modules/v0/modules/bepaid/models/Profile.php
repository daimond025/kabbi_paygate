<?php

namespace bepaid\models;

use bepaid\exceptions\NotFoundProfileException;

/**
 * Class Profile
 * @package payture\models
 */
class Profile extends ProfileDAO
{
    const URL_GATEWAY = 'https://gateway.bepaid.by';
    const URL_CHECKOUT = 'https://checkout.bepaid.by';

    /**
     * @param string $paymentName
     *
     * @return Profile
     * @throws NotFoundProfileException
     */
    public static function get($paymentName)
    {
        $profile = self::findOne(['paymentName' => $paymentName]);
        if (!$profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }

        return $profile;
    }

    /**
     * @return string
     */
    public function getGatewayUrl()
    {
        return self::URL_GATEWAY;
    }

    /**
     * @return string
     */
    public function getCheckoutUrl()
    {
        return self::URL_CHECKOUT;
    }
}