<?php

namespace bepaid\models;

class DbPayment
{
    /**
     * @var string
     */
    private $paymentName;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $uid;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    public function __construct($paymentName, $clientId, $orderNumber, $uid, $amount, $currency)
    {
        $this->paymentName = $paymentName;
        $this->clientId = $clientId;
        $this->orderNumber = $orderNumber;
        $this->uid = $uid;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'paymentName' => $this->paymentName,
            'clientId'    => $this->clientId,
            'orderNumber' => $this->orderNumber,
            'uid'         => $this->uid,
            'amount'      => $this->amount,
        ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}