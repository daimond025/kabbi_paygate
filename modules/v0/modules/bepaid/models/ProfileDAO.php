<?php

namespace bepaid\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "bepaid_profile".
 *
 * @property integer $id
 * @property string $paymentName
 * @property integer $shopId
 * @property string $shopKey
 * @property integer $isDebug
 * @property double $commission
 * @property integer $cardBindingSum
 * @property string $currency
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class ProfileDAO extends \yii\db\ActiveRecord
{
    const TYPE_ID = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bepaid_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'shopId', 'shopKey', 'currency'], 'required'],
            [['shopId', 'isDebug', 'cardBindingSum', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName'], 'string', 'max' => 32],
            [['shopKey'], 'string', 'max' => 255],
            [['currency'], 'string', 'max' => 3],
            [['paymentName'], 'unique'],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'paymentName'    => 'Payment Name',
            'shopId'         => 'Shop ID',
            'shopKey'        => 'Shop Key',
            'isDebug'        => 'Is Debug',
            'commission'     => 'Commission',
            'cardBindingSum' => 'Card Binding Sum',
            'currency'       => 'Currency',
            'createdAt'      => 'Created At',
            'updatedAt'      => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    public function setTypeId($value)
    {
    }

    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'typeId',
            'paymentName',
            'shopId',
            'isDebug',
            'commission',
            'cardBindingSum',
            'currency'
        ];
    }
}
