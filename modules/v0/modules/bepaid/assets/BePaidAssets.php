<?php

namespace bepaid\assets;

use yii\web\AssetBundle;

class BePaidAssets extends AssetBundle
{
    public $sourcePath = '@bepaid/assets';

    public $js = [
    ];

    public $css = [
        'style.css',
    ];

    public $depends = [
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}