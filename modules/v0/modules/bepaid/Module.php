<?php

namespace bepaid;

use app\components\syslog\Syslog;
use bepaid\components\BePaidService;
use bepaid\components\CardManager;
use bepaid\components\DbPaymentManager;
use bepaid\models\Profile;
use v0\components\currency\CurrencyService;
use yii\i18n\PhpMessageSource;

/**
 * Class Module
 * @package payture
 */
class Module extends \yii\base\Module
{
    const LOCALE_DEFAULT = 'en';
    const LOCALE_RU = 'ru';
    const AVAILABLE_LOCALES = ['en', 'ru', 'de'];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'bepaid\controllers';

    /**
     * @var int
     */
    public $networkTimeout;

    /**
     * @var Syslog
     */
    public $syslog;

    /**
     * @var string
     */
    public $url;

    /**
     * @var CurrencyService
     */
    public $currencyService;

    public function init()
    {
        parent::init();

        $dbPaymentManager = new DbPaymentManager($this->syslog);
        $cardManager = new CardManager();
        $service = new BePaidService($dbPaymentManager, $cardManager, $this->currencyService, $this->url);
        $this->layout = 'bepaid';
        \Yii::$app->layout = false;

        $this->registerTranslations();

        $this->components = [
            'paymentGateProfile' => Profile::class,
            'bePaidService'      => $service,
            'cardManager'        => $cardManager,
        ];
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['bepaid/*'] = [
            'class'          => PhpMessageSource::class,
            'sourceLanguage' => 'en-US',
            'basePath'       => '@bepaid/messages',
            'fileMap'        => [
                'bepaid/page' => 'page.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('bepaid/' . $category, $message, $params, $language);
    }
}