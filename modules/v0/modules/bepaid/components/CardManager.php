<?php

namespace bepaid\components;

use bepaid\exceptions\BePaidException;
use bepaid\models\Card;
use bepaid\models\CardDAO;
use yii\db\StaleObjectException;

class CardManager
{
    /**
     * @param $paymentName
     * @param $clientId
     *
     * @return array
     */
    public function getCards($paymentName, $clientId)
    {
        $cards = [];
        $models = CardDAO::find()->where([
            'paymentName' => $paymentName,
            'clientId'    => $clientId,
        ])->all();

        if (!empty($models)) {
            $cards = array_map(function ($model) {
                return new Card($model->pan, $model->token);
            }, $models);
        }

        return $cards;
    }

    public function getCard($paymentName, $clientId, $pan)
    {
    }

    /**
     * @param string $paymentName
     * @param string $clientId
     * @param string $pan
     * @param string $token
     */
    public function saveCard($paymentName, $clientId, $pan, $token)
    {
        $model = new CardDAO([
            'paymentName' => $paymentName,
            'clientId'    => $clientId,
            'pan'         => $pan,
            'token'       => $token,
        ]);

        if (!$model->save()) {
            $error = implode('; ', $model->getFirstErrors());
            throw new BePaidException("Save card error: paymentName=$paymentName, clientId=$clientId, pan=$pan, token=$token, error=$error");
        }
    }

    /**
     * @param string $paymentName
     * @param string $clientId
     * @param string $token
     *
     * @throws StaleObjectException
     */
    public function removeCard($paymentName, $clientId, $token)
    {
        $model = CardDAO::find()->where([
            'paymentName' => $paymentName,
            'clientId'    => $clientId,
            'token'       => $token,
        ])->one();

        if (empty($model)) {
            return;
        }

        if (!$model->delete()) {
            throw new BePaidException("Delete card error: paymentName=$paymentName, clientId=$clientId, pan=$pan");
        }
    }
}