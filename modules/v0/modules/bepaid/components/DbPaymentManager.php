<?php

namespace bepaid\components;

use app\components\syslog\Syslog;
use bepaid\exceptions\BePaidException;
use bepaid\models\PaymentDAO;
use bepaid\models\DbPayment;

class DbPaymentManager
{
    /**
     * @var Syslog
     */
    private $logger;

    public function __construct(Syslog $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     *
     * @return DbPayment
     * @throws BePaidException
     */
    public function get($paymentName, $orderNumber)
    {
        try {
            $model = $this->findPayment($paymentName, $orderNumber);
            if ($model === null) {
                throw new BePaidException("Payment not found: paymentName=$paymentName, orderNumber=$orderNumber");
            }

            return new DbPayment($model->paymentName, $model->clientId, $model->orderNumber, $model->uid,
                $model->amount, $model->currency);
        } catch (\Exception $ex) {
            throw new BePaidException("Get payment error: error=\"{$ex->getMessage()}\"");
        }
    }

    /**
     * @param DbPayment $payment
     */
    public function save(DbPayment $payment)
    {
        try {
            $model = $this->findPayment($payment->getPaymentName(), $payment->getOrderNumber());
            if ($model === null) {
                $model = new PaymentDAO();
            }

            $model->paymentName = $payment->getPaymentName();
            $model->clientId = $payment->getClientId();
            $model->orderNumber = $payment->getOrderNumber();
            $model->uid = $payment->getUid();
            $model->amount = $payment->getAmount();
            $model->currency = $payment->getCurrency();

            if (!$model->save()) {
                throw new BePaidException(implode('; ', $model->getFirstErrors()));
            }
        } catch (\Exception $ex) {
            $this->logger->addDebugLog("Save payment error: error=\"{$ex->getMessage()}\", paymentData={$payment}");
        }
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     *
     * @return PaymentDAO|null
     */
    private function findPayment($paymentName, $orderNumber)
    {
        $model = PaymentDAO::find()->where(['paymentName' => $paymentName, 'orderNumber' => $orderNumber])->one();

        return empty($model) ? null : $model;
    }
}