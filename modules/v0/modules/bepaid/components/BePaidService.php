<?php

namespace bepaid\components;

use bepaid\exceptions\NotFoundProfileException;
use bepaid\models\Card;
use bepaid\models\DbPayment;
use bepaid\models\Payment;
use bepaid\models\Profile;
use bepaid\exceptions\BePaidException;
use v0\components\currency\CurrencyService;

class BePaidService
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string|null
     */
    private $language;

    /**
     * @var DbPaymentManager
     */
    private $dbPaymentManager;

    /**
     * @var CardManager
     */
    private $cardManager;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var string
     */
    private $url;

    public function __construct($dbPaymentManager, $cardManager, $currencyService, $url)
    {
        $this->dbPaymentManager = $dbPaymentManager;
        $this->cardManager = $cardManager;
        $this->currencyService = $currencyService;
        $this->url = $url;
    }

    /**
     * @param string $paymentName
     *
     * @throws NotFoundProfileException
     */
    public function setProfile($paymentName)
    {
        $this->profile = Profile::get($paymentName);
    }

    /**
     * @return Profile
     *
     * @throws NotFoundProfileException
     */
    public function getProfile()
    {
        if ($this->profile === null) {
            throw new NotFoundProfileException('BePaid profile not found');
        }

        return $this->profile;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @param $clientId
     *
     * @return array
     */
    public function getCards($clientId)
    {
        $paymentName = $this->profile->paymentName;
        $cards = [];
        try {
            foreach ($this->cardManager->getCards($paymentName, $clientId) as $card) {
                /** @var Card $card */
                $cards[] = ['pan' => $card->getPan(), 'binding' => $card->getToken()];
            }

            return $cards;
        } catch (\Exception $e) {
            $error = $e->getMessage();
            throw new BePaidException("Getting client cards error (bepaid): paymentName=$paymentName, clientId=$clientId, error=$error");
        }
    }

    /**
     * @param string $clientId
     * @param string $token
     */
    public function removeCard($clientId, $token)
    {
        $paymentName = $this->profile->paymentName;
        try {
            $this->cardManager->removeCard($paymentName, $clientId, $token);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            throw new BePaidException("Remove client card error (bepaid): paymentName=$paymentName, clientId=$clientId, token=$token, error=$error");
        }
    }

    /**
     * @param string $clientId
     * @param string $orderNumber
     * @param string $description
     *
     * @return mixed
     * @throws NotFoundProfileException
     */
    public function getUrlForBindingCard($clientId, $orderNumber, $description = '')
    {
        $profile = $this->getProfile();
        $this->loadSettings($profile);

        $transaction = new \BeGateway\GetPaymentToken();
        $transaction->money->setAmount($profile->cardBindingSum);
        $transaction->money->setCurrency($this->getCurrencyCode($profile->currency));
        $transaction->setTokenizationTransactionType();

        $transaction->setLanguage($this->language);
        $transaction->setTrackingId($orderNumber);
        $transaction->setDescription($description);
        $transaction->setTestMode((bool)$profile->isDebug);

        $transaction->setNotificationUrl("{$this->url}bepaid/card/register/$profile->paymentName/$clientId");
        $transaction->setSuccessUrl("{$this->url}bepaid/card/success/$this->language");
        $transaction->setFailUrl("{$this->url}bepaid/card/fail/$this->language");
        $transaction->setCancelUrl("{$this->url}bepaid/card/fail/$this->language");
        $transaction->setDeclineUrl("{$this->url}bepaid/card/fail/$this->language");

        $response = $transaction->submit();
        if (!$response->isSuccess()) {
            throw new BePaidException("Getting card binding url error: error={$response->getMessage()}");
        }

        return $response->getRedirectUrl();
    }

    /**
     * @param string $clientId
     * @param string $cardToken
     * @param string $orderNumber
     * @param int $amount
     * @param int $currency
     * @param string $description
     *
     * @return string
     * @throws NotFoundProfileException
     */
    public function pay($clientId, $cardToken, $orderNumber, $amount, $currency, $description = '')
    {
        $profile = $this->getProfile();
        $this->loadSettings($profile);

        $transaction = new \BeGateway\PaymentOperation();
        $transaction->money->setCents($amount);
        $transaction->money->setCurrency($this->getCurrencyCode($currency));

        $transaction->card->setCardToken($cardToken);
        $transaction->card->setSkip3D(true);

        $transaction->setLanguage($this->language);
        $transaction->setTrackingId($orderNumber);
        $transaction->setDescription($description);
        $transaction->setTestMode((bool)$profile->isDebug);

        $response = $transaction->submit();
        if ($response->isSuccess()) {
            $dbPayment = new DbPayment($profile->paymentName, $clientId, $orderNumber, $response->getUid(), $amount,
                $currency);
            $this->dbPaymentManager->save($dbPayment);

            return $response->getUid();
        }

        throw new BePaidException("Pay error: status={$response->getStatus()}, message={$response->getMessage()}, paymentName={$profile->paymentName}, clientId={$clientId}, cardToken={$cardToken}, orderNumber={$orderNumber}, amount={$amount}, currency={$currency}");
    }

    /**
     * @param string $clientId
     * @param string $uid
     * @param int $amount
     *
     * @throws NotFoundProfileException
     */
    public function refund($clientId, $uid, $amount)
    {
        $profile = $this->getProfile();
        $this->loadSettings($profile);

        $refund = new \BeGateway\RefundOperation();
        $refund->setParentUid($uid);
        $refund->money->setCents($amount);
        $refund->setReason('Refund payment');

        $response = $refund->submit();
        if ($response->isSuccess()) {
            return;
        }

        throw new BePaidException("Refund error: status={$response->getStatus()}, message={$response->getMessage()}, paymentName={$profile->paymentName}, clientId={$clientId}, uid={$uid}, amount={$amount}");
    }

    /**
     * @param string $clientId
     * @param string $uid
     *
     * @return Payment
     * @throws NotFoundProfileException
     */
    public function status($clientId, $uid)
    {
        $profile = $this->getProfile();
        $this->loadSettings($profile);

        $transaction = $this->getTransactionData($uid);
        $trackingId = isset($transaction['tracking_id']) ? $transaction['tracking_id'] : null;

        $isRefunded = $this->isRefunded($trackingId);

        $status = isset($transaction['status']) ? $transaction['status'] : null;
        $amount = isset($transaction['amount']) ? $transaction['amount'] : null;
        $currency = isset($transaction['currency']) ? $transaction['currency'] : null;

        $first1 = isset($transaction['credit_card']['first_1']) ? $transaction['credit_card']['first_1'] : '';
        $last4 = isset($transaction['credit_card']['last_4']) ? $transaction['credit_card']['last_4'] : '';
        $token = isset($transaction['credit_card']['token']) ? $transaction['credit_card']['token'] : null;
        $card = new Card(Card::generatePan($first1, $last4), $token);

        return new Payment($clientId, $trackingId, $status, $isRefunded, $card, $amount,
            $this->getCurrencyNumber($currency));
    }

    private function getCurrencyCode($currency)
    {
        $currency = $this->currencyService->getCurrency($currency);

        return $currency->code;
    }

    private function getCurrencyNumber($currency)
    {
        $currency = $this->currencyService->getCurrency($currency);

        return $currency->number;
    }

    private function getTransactionData($uid)
    {
        $query = new \BeGateway\QueryByUid();
        $query->setUid($uid);
        $response = $query->submit()->getResponseArray();

        return isset($response['transaction']) ? $response['transaction'] : [];
    }

    private function isRefunded($trackingId)
    {
        $query = new \BeGateway\QueryByTrackingId();
        $query->setTrackingId($trackingId);
        $response = $query->submit()->getResponseArray();

        foreach ($response['transactions'] as $transaction) {
            if (isset($transaction['refund']['status']) && $transaction['refund']['status'] === Payment::STATUS_SUCCESSFUL) {
                return true;
            }
        }

        return false;
    }

    private function loadSettings(Profile $profile)
    {
        \BeGateway\Settings::$shopId = $profile->shopId;
        \BeGateway\Settings::$shopKey = $profile->shopKey;
        \BeGateway\Settings::$checkoutBase = $profile->getCheckoutUrl();
        \BeGateway\Settings::$gatewayBase = $profile->getGatewayUrl();
    }
}