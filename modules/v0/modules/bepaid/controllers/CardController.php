<?php

namespace bepaid\controllers;

use bepaid\components\CardManager;
use bepaid\models\Card;
use bepaid\Module as BePaid;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class CardController extends Controller
{

    public function init()
    {
        $this->enableCsrfValidation = false;

        parent::init();
    }

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    '*/*'              => Response::FORMAT_HTML,
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionRegister($paymentName, $clientId)
    {
        $params = \Yii::$app->request->post();

        try {
            $first1 = isset($params['transaction']['credit_card']['first_1']) ? $params['transaction']['credit_card']['first_1'] : '';
            $last4 = isset($params['transaction']['credit_card']['last_4']) ? $params['transaction']['credit_card']['last_4'] : '';
            $token = isset($params['transaction']['credit_card']['token']) ? $params['transaction']['credit_card']['token'] : '';

            $pan = Card::generatePan($first1, $last4);

            /** @var CardManager $cardManager */
            $cardManager = BePaid::getInstance()->get('cardManager');
            $cardManager->saveCard($paymentName, $clientId, $pan, $token);
        } catch (\Exception $e) {
            \Yii::error("Register card error (bepaid): paymentName=$paymentName, clientId=$clientId, error={$e->getMessage()}");
        }
    }

    /**
     * @param string $locale
     *
     * @return string
     */
    public function actionSuccess($locale)
    {
        $this->loadLocale($locale);

        return $this->render('success');
    }

    /**
     * @param string $locale
     *
     * @return string
     */
    public function actionFail($locale)
    {
        $this->loadLocale($locale);

        return $this->render('fail');
    }

    /**
     * @param string $locale
     */
    private function loadLocale($locale)
    {
        $locale = $this->filterLocale($locale);
        \Yii::$app->language = $locale;
    }

    /**
     * @param string $locale
     *
     * @return string
     */
    private function filterLocale($locale)
    {
        return in_array($locale, BePaid::AVAILABLE_LOCALES, true) ? $locale : BePaid::LOCALE_DEFAULT;
    }
}