<?php

/* @var $this \yii\web\View */

/* @var $content string */

use bepaid\assets\BePaidAssets;
use yii\helpers\Html;

BePaidAssets::register($this);

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <?php $this->head() ?>
    </head>
    <body>
    <div class="wrapper">
        <?php $this->beginBody() ?>
        <?= $content ?>
        <?php $this->endBody() ?>
    </div>
    </body>
    </html>
<?php $this->endPage() ?>