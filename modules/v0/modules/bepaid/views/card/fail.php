<?php

use bepaid\assets\BePaidAssets;
use bepaid\Module as BePaid;

BePaidAssets::register($this);

?>

<div class="error">
    <img src="/images/error.svg">
    <p><?= BePaid::t('page', 'Error') ?>:<br>
        <?= BePaid::t('page',
            'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.') ?></p>
</div>