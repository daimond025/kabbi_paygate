<?php

use bepaid\assets\BePaidAssets;
use bepaid\Module as BePaid;

BePaidAssets::register($this);

?>

<div class="success">
    <img src="/images/success.svg">
    <p><?= BePaid::t('page', 'Your card was successfully added and you can use it for paying!') ?></p>
</div>