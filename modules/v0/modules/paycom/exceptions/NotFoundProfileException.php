<?php

namespace paycom\exceptions;

/**
 * Class NotFoundProfileException
 * @package paycom\exceptions
 */
class NotFoundProfileException extends \DomainException
{

}