<?php

namespace paycom\exceptions;

use yii\base\UserException;

/**
 * Class HttpClientException
 * @package paycom\exceptions
 */
class HttpClientException extends UserException
{
    public function __construct($message, $previous = null)
    {
        parent::__construct($message, 255, $previous);
    }

    public function getName()
    {
        return 'Http Client Exception';
    }
}