<?php

namespace paycom\exceptions;

use yii\web\HttpException;

/**
 * Class PaycomException
 * @package paycom\exceptions
 */
class PaycomException extends HttpException
{
    const EXCEPTION_NAME = 'Paycom exception';

    public function __construct($message = null, \Exception $previous = null)
    {
        if ($message === null) {
            $message = self::EXCEPTION_NAME;
        }
        parent::__construct(500, $message, 10, $previous);
    }

    public function getName()
    {
        return self::EXCEPTION_NAME;
    }

}