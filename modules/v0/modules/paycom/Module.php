<?php

namespace paycom;

use GuzzleHttp\Client;
use paycom\components\CardManager;
use paycom\components\PaycomApi;
use paycom\components\PaycomService;
use paycom\components\PaymentManager;
use paycom\models\Profile;

/**
 * Class Module
 * @package paycom
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'paycom\controllers';

    public $networkTimeout;
    public $syslog;

    public function init()
    {
        parent::init();

        $httpClient = new Client(['timeout' => $this->networkTimeout]);
        $paycomApi = new PaycomApi($httpClient, $this->syslog);
        $cardManager = new CardManager();
        $paymentManager = new PaymentManager();
        $paycomService = new PaycomService($paycomApi, $cardManager, $paymentManager,
            \Yii::$app->getLog()->getLogger());

        $this->components = [
            'paymentGateProfile' => Profile::class,
            'paycomService'      => $paycomService,
        ];

        $this->layout = 'main';
        \Yii::$app->layout = false;
    }

}