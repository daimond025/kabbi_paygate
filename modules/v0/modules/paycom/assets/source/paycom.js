$(function () {

    var app = {
        paymentName: null,
        clientId: null,
        apiId: null,
        url: null,
        token: null,

        csrfParam: yii.getCsrfParam(),
        csrfToken: yii.getCsrfToken(),

        init: function () {
            var $form = $('#card');

            this.paymentName = $form.data('paymentName');
            this.clientId = $form.data('clientId');
            this.apiId = $form.data('apiId');
            this.url = $form.data('url');

            this.registerCreateCardEvent();
            this.registerVerifyCardEvent();
        },

        registerCreateCardEvent: function () {
            var _this = this;

            $('#create-card, #repeat-create-card').click(function () {
                var number = $('#number').val();
                var expire = $('#month').val() + $('#year').val();

                _this.setPhone();
                _this.hideError();
                _this.create(number, expire)
                    .done(function (data) {
                        if (data && data.error) {
                            _this.showError(data.error.message);

                            return;
                        }

                        _this.token = data && data.result && data.result.card && data.result.card.token;
                        if (!_this.token) {
                            _this.showError();
                            return;
                        }

                        _this.getVerifyCode()
                            .done(function (data) {
                                var phone = data && data.result && data.result.phone;
                                _this.setPhone(phone);

                                $('#verify').show();
                                $('#create').hide();
                            })
                            .fail(function (err) {
                                _this.showError();
                                console.log(err);
                            });
                    })
                    .fail(function (err) {
                        _this.showError();
                        console.log(err);
                    });
            });
        },

        registerVerifyCardEvent: function () {
            var _this = this;

            $('#verify-card').click(function () {
                var code = $('#verify-code').val();

                _this.hideError();
                _this.verify(code)
                    .done(function (data) {
                        if (data && data.error) {
                            _this.showError(data.error.message);

                            return;
                        }

                        _this.token = data && data.result && data.result.card && data.result.card.token;

                        _this.save()
                            .done(function (data) {
                                if (data && data.url) {
                                    window.location.href = data.url;

                                    return;
                                }

                                _this.showError();
                            })
                            .fail(function (err) {
                                _this.showError();
                                console.log(err);
                            });
                    })
                    .fail(function (err) {
                        _this.showError();
                        console.log(err);
                    });
            });
        },

        setPhone: function (phone) {
            $('#phone').text(phone || '');
        },

        showError: function (error) {
            $('.error').html(error || 'Возникла ошибка при добавлении карты').show();
        },

        hideError: function () {
            $('.error').html('').hide();
        },

        create: function (number, expire) {
            var _this = this;

            return $.ajax(
                this.url, {
                    method: 'POST',
                    cache: false,
                    contentType: 'application/json',
                    dataType: 'json',
                    headers: {
                        'X-Auth': _this.apiId
                    },
                    data: JSON.stringify({
                        method: 'cards.create',
                        params: {
                            card: {number: number, expire: expire},
                            save: true
                        }
                    })
                }
            );
        },

        getVerifyCode: function () {
            var _this = this;

            return $.ajax(
                this.url, {
                    method: 'POST',
                    cache: false,
                    contentType: 'application/json',
                    dataType: 'json',
                    headers: {
                        'X-Auth': _this.apiId
                    },
                    data: JSON.stringify({
                        method: 'cards.get_verify_code',
                        params: {
                            token: _this.token
                        }
                    })
                }
            );
        },

        verify: function (code) {
            var _this = this;

            return $.ajax(
                this.url, {
                    method: 'POST',
                    cache: false,
                    contentType: 'application/json',
                    dataType: 'json',
                    headers: {
                        'X-Auth': _this.apiId
                    },
                    data: JSON.stringify({
                        method: 'cards.verify',
                        params: {
                            token: _this.token,
                            code: code
                        }
                    })
                }
            );
        },

        save: function () {
            var params = {
                paymentName: this.paymentName,
                clientId: this.clientId,
                token: this.token,
            };
            params[this.csrfParam] = this.csrfToken;


            return $.post('/v0/paycom/card/save', params);
        }
    };

    app.init();
});