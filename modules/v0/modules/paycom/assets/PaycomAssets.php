<?php

namespace paycom\assets;

use yii\web\AssetBundle;

class PaycomAssets extends AssetBundle
{
    public $sourcePath = '@paycom/assets/source';

    public $js = [
        'paycom.js',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}