<?php

namespace paycom\components;

use paycom\exceptions\PaycomException;
use paycom\models\PaycomPaymentDAO;
use paycom\models\Payment;

/**
 * Class PaymentManager
 * @package paycom\components
 */
class PaymentManager
{
    /**
     * @param string $receiptId
     *
     * @return Payment
     * @throws PaycomException
     */
    public function getPayment($receiptId)
    {
        $model = PaycomPaymentDAO::find()->where(['receiptId' => $receiptId])->one();
        if (empty($model)) {
            throw new PaycomException("Payment not found: receiptId={$receiptId}");
        }

        return new Payment($model->paymentName, $model->clientId, $model->orderNumber, $model->token, $model->receiptId,
            $model->amount);
    }

    /**
     * @param string $paymentName
     * @param string $clientId
     * @param string $orderNumber
     *
     * @return bool
     */
    public function isExistsPayment($paymentName, $clientId, $orderNumber)
    {
        return PaycomPaymentDAO::find()
            ->where([
                'paymentName' => $paymentName,
                'clientId'    => $clientId,
                'orderNumber' => $orderNumber,
            ])
            ->exists();
    }

    /**
     * @param Payment $payment
     *
     * @throws PaycomException
     */
    public function savePayment(Payment $payment)
    {
        $model = new PaycomPaymentDAO([
            'paymentName' => $payment->getPaymentName(),
            'clientId'    => $payment->getClientId(),
            'orderNumber' => $payment->getOrderNumber(),
            'token'       => $payment->getToken(),
            'receiptId'   => $payment->getReceiptId(),
            'amount'      => $payment->getAmount(),
        ]);

        if (!$model->save()) {
            throw new PaycomException('Save payment to DB error: error="' . implode('; ',
                    $model->getFirstErrors()) . '"');
        }
    }
}