<?php

namespace paycom\components;

use app\components\syslog\Syslog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use paycom\exceptions\HttpClientException;
use paycom\exceptions\PaycomException;
use paycom\models\Card;
use paycom\models\Profile;
use paycom\models\Receipt;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;
use yii\base\Exception;

/**
 * Class PaycomApi
 * @package paycom\components
 */
class PaycomApi
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Syslog
     */
    private $logger;

    /**
     * PaycomApi constructor.
     *
     * @param Client $client
     * @param Syslog $logger
     */
    public function __construct(Client $client, Syslog $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param Profile $profile
     * @param string  $token
     *
     * @return Card
     * @throws PaycomException
     */
    public function checkCard($profile, $token)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'cards.check',
            'params' => [
                'token' => $token,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }

            return new Card($response['result']['card']['number'], $response['result']['card']['token']);
        } catch (\Exception $ex) {
            throw new PaycomException("Check card error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param Profile $profile
     * @param string  $token
     *
     * @throws PaycomException
     */
    public function removeCard($profile, $token)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'cards.remove',
            'params' => [
                'token' => $token,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }
        } catch (\Exception $ex) {
            throw new PaycomException("Remove card error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param Profile $profile
     * @param string  $orderNumber
     * @param int     $amount
     * @param string  $description
     *
     * @return string
     * @throws PaycomException
     */
    public function createReceipt($profile, $orderNumber, $amount, $description)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'receipts.create',
            'params' => [
                'amount'      => $amount,
                'account'     => [
                    'order_id' => $orderNumber,
                ],
                'description' => $description,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }

            return $response['result']['receipt']['_id'];
        } catch (\Exception $ex) {
            throw new PaycomException("Create receipt error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param Profile $profile
     * @param string  $receiptId
     * @param string  $token
     *
     * @return string
     * @throws PaycomException
     */
    public function payReceipt($profile, $receiptId, $token)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'receipts.pay',
            'params' => [
                'id'    => $receiptId,
                'token' => $token,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }
        } catch (\Exception $ex) {
            throw new PaycomException("Pay receipt error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param Profile $profile
     * @param string  $receiptId
     *
     * @throws PaycomException
     */
    public function cancelReceipt($profile, $receiptId)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'receipts.cancel',
            'params' => [
                'id' => $receiptId,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }
        } catch (\Exception $ex) {
            throw new PaycomException("Pay receipt error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param Profile $profile
     * @param string  $receiptId
     *
     * @return Receipt
     * @throws PaycomException
     */
    public function getReceipt($profile, $receiptId)
    {
        $auth = "$profile->apiId:$profile->password";
        $data = [
            'id'     => Uuid::uuid4()->toString(),
            'method' => 'receipts.get',
            'params' => [
                'id' => $receiptId,
            ],
        ];

        try {
            $response = $this->request($profile->getApiUrl(), $auth, $data);
            if (isset($response['error'])) {
                throw new PaycomException($response['error']['message']);
            }

            return new Receipt($response['result']['receipt']['_id'], $response['result']['receipt']['state'],
                $response['result']['receipt']['card']['number'], $response['result']['receipt']['amount']);
        } catch (\Exception $ex) {
            throw new PaycomException("Pay receipt error: error=\"{$ex->getMessage()}\"", $ex);
        }
    }

    /**
     * @param string $url
     * @param string $auth
     * @param array  $data
     *
     * @return ResponseInterface
     * @throws HttpClientException
     * @throws \RuntimeException
     */
    private function request($url, $auth, $data)
    {
        try {
            $response = $this->client->post($url, [
                RequestOptions::JSON => $data,
                'headers'            => [
                    'X-Auth' => $auth,
                ],
            ]);

            $result = json_decode($response->getBody()->getContents(), true);
            $this->addDebugLog($url . ' ' . $auth, json_encode($data), json_encode($result));

            return $result;
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $this->addDebugLog($url, json_encode($data), $response->getBody()->getContents());

            throw new HttpClientException("Request error: error={$e->getMessage()}", $e);
        } catch (\Exception $e) {
            throw new HttpClientException("Request error: error={$e->getMessage()}", $e);
        }
    }

    /**
     * @param string $url
     * @param string $request
     * @param string $response
     */
    private function addDebugLog($url, $request, $response)
    {
        try {
            $message = sprintf('%s %s -> %s', $url, $request, $response);
            $this->logger->addDebugLog($message);
        } catch (Exception $ignore) {
        }
    }

}