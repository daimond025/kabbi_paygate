<?php

namespace paycom\components;

use paycom\exceptions\PaycomException;
use paycom\models\Card;
use paycom\models\PaycomCardDAO;
use paycom\models\Profile;
use v0\models\Customer;

/**
 * Class CardManager
 * @package paycom\components
 */
class CardManager
{
    /**
     * @param Profile  $profile
     * @param Customer $customer
     *
     * @return array
     */
    public function getCards($profile, $customer)
    {
        $cards = PaycomCardDAO::find()
            ->where([
                'paymentName' => $profile->paymentName,
                'clientId'    => $customer->clientId,
            ])
            ->all();

        return empty($cards) ? [] : array_map(function ($card) {
            /** @var PaycomCardDAO $card */
            return new Card($card->pan, $card->token);
        }, $cards);
    }

    /**
     * @param Profile  $profile
     * @param Customer $customer
     * @param string   $pan
     *
     * @return null|Card
     */
    public function getCard($profile, $customer, $pan)
    {
        $card = PaycomCardDAO::find()
            ->where([
                'paymentName' => $profile->paymentName,
                'clientId'    => $customer->clientId,
                'pan'         => $pan,
            ])
            ->one();

        return empty($card) ? null : new Card($card->pan, $card->token);
    }

    /**
     * @param Profile  $profile
     * @param Customer $customer
     * @param Card     $card
     *
     * @throws PaycomException
     */
    public function createCard($profile, $customer, $card)
    {
        $model = new PaycomCardDAO([
            'paymentName' => $profile->paymentName,
            'clientId'    => $customer->clientId,
            'pan'         => $card->getPan(),
            'token'       => $card->getToken(),
        ]);

        if (!$model->save()) {
            throw new PaycomException('Save card to DB error: error="' . implode('; ', $model->getFirstErrors()) . '"');
        }
    }

    /**
     * @param Profile  $profile
     * @param Customer $customer
     * @param string   $token
     *
     * @throws PaycomException
     */
    public function removeCard($profile, $customer, $token)
    {
        $model = PaycomCardDAO::find()
            ->where([
                'paymentName' => $profile->paymentName,
                'clientId'    => $customer->clientId,
                'token'       => $token,
            ])
            ->one();

        try {
            if (!$model->delete()) {
                throw new PaycomException(implode('; ', $model->getFirstErrors()));
            }
        } catch (\Exception $ex) {
            throw new PaycomException("Remove card from DB error: error=\"{$ex->getMessage()}\"");
        }
    }
}