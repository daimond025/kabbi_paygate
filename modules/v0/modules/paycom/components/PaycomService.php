<?php

namespace paycom\components;

use paycom\exceptions\NotFoundProfileException;
use paycom\exceptions\PaycomException;
use paycom\models\Card;
use paycom\models\Payment;
use paycom\models\Profile;
use v0\models\Customer;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\log\Logger;

/**
 * Class PaycomService
 * @package paycom\components
 */
class PaycomService
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var PaycomApi
     */
    private $api;

    /**
     * @var CardManager
     */
    private $cardManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * PaycomService constructor.
     *
     * @param PaycomApi      $api
     * @param CardManager    $cardManager
     * @param PaymentManager $paymentManager
     * @param Logger         $logger
     */
    public function __construct(
        PaycomApi $api,
        CardManager $cardManager,
        PaymentManager $paymentManager,
        Logger $logger
    ) {
        $this->api = $api;
        $this->cardManager = $cardManager;
        $this->paymentManager = $paymentManager;
        $this->logger = $logger;
    }

    /**
     * @param string $paymentName
     */
    public function setProfile($paymentName)
    {
        $this->profile = Profile::get($paymentName);
    }

    /**
     * @return Profile
     * @throws NotFoundProfileException
     */
    public function getProfile()
    {
        if ($this->profile === null) {
            throw new NotFoundProfileException('Paycom profile not found');
        }

        return $this->profile;
    }

    /**
     * @param Customer $customer
     *
     * @return string
     * @throws InvalidParamException
     */
    public function getUrlForRegistryCard($customer)
    {
        $url = substr(\Yii::$app->params['my.externalUrl'], 0, -1);

        return $url . Url::toRoute([
                '/v0/paycom/card/index',
                'paymentName' => $this->profile->paymentName,
                'clientId'    => $customer->clientId,
            ], false);
    }

    /**
     * @param Customer $customer
     * @param string   $token
     *
     * @throws \Exception
     */
    public function createCard($customer, $token)
    {
        $card = $this->api->checkCard($this->profile, $token);
        $this->cardManager->createCard($this->profile, $customer, $card);
    }

    /**
     * @param Customer $customer
     *
     * @return array
     * @throws PaycomException
     */
    public function getCards($customer)
    {
        $cards = [];
        foreach ($this->cardManager->getCards($this->profile, $customer) as $card) {
            /** @var Card $card */
            $cards[] = ['pan' => $card->getPan(), 'binding' => $card->getToken()];
        }

        return $cards;
    }

    /**
     * @param Customer $customer
     * @param string   $token
     *
     * @throws PaycomException
     */
    public function removeCard($customer, $token)
    {
        $this->api->removeCard($this->profile, $token);
        $this->cardManager->removeCard($this->profile, $customer, $token);
    }

    /**
     * @param Customer $customer
     * @param string   $orderNumber
     * @param string   $token
     * @param int      $amount
     * @param string   $description
     *
     * @return string
     * @throws PaycomException
     */
    public function createReceipt($customer, $orderNumber, $token, $amount, $description = '')
    {
        if ($this->paymentManager->isExistsPayment($this->profile->paymentName, $customer->clientId, $orderNumber)) {
            throw new PaycomException("Reciept is already exists: paymentName=\"{$this->profile->paymentName}\", clientId=\"{$customer->clientId}\", orderNumber=\"{$orderNumber}\"");
        }

        $receiptId = $this->api->createReceipt($this->profile, $orderNumber, $amount, $description);
        $payment = new Payment($this->profile->paymentName, $customer->clientId, $orderNumber, $token, $receiptId,
            $amount);

        try {
            $this->paymentManager->savePayment($payment);
        } catch (\Exception $ignore) {
            $this->logger->log("Register payment error: paymentName=\"{$payment->getPaymentName()}\", clientId=\"{$payment->getClientId()}\", orderNumber=\"{$payment->getOrderNumber()}\", token=\"{$payment->getToken()}\", receiptId=\"{$payment->getReceiptId()}\", amount=\"{$payment->getAmount()}\", error={$ignore->getMessage()}",
                Logger::LEVEL_ERROR);
        }

        return $receiptId;
    }

    /**
     * @param string $receiptId
     * @param string $token
     *
     * @return boolean
     * @throws PaycomException
     */
    public function payReceipt($receiptId, $token)
    {
        $this->api->payReceipt($this->profile, $receiptId, $token);

        return true;
    }

    /**
     * @param string $receiptId
     *
     * @return bool
     * @throws PaycomException
     */
    public function cancelReceipt($receiptId)
    {
        $this->api->cancelReceipt($this->profile, $receiptId);

        return true;
    }

    /**
     * @param string $receiptId
     *
     * @return array
     * @throws PaycomException
     */
    public function getReceipt($receiptId)
    {
        $receipt = $this->api->getReceipt($this->profile, $receiptId);
        $data = [
            'state'  => $receipt->getState(),
            'pan'    => $receipt->getPan(),
            'amount' => $receipt->getAmount(),
        ];

        try {
            $payment = $this->paymentManager->getPayment($receiptId);
            $data['client_id'] = $payment->getClientId();
            $data['token'] = $payment->getToken();
        } catch (\Exception $ignore) {
            $this->logger->log("Get payment error: receiptId=\"{$receiptId}\", error={$ignore->getMessage()}",
                Logger::LEVEL_ERROR);
        }

        return $data;
    }
}