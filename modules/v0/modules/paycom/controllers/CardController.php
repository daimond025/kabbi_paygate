<?php

namespace paycom\controllers;

use app\components\syslog\Syslog;
use paycom\components\PaycomService;
use paycom\models\Profile;
use paycom\Module;
use v0\models\Customer;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class CardController
 * @package paycom\controllers
 */
class CardController extends Controller
{
    /**
     * @var PaycomService
     */
    private $paycomService;

    /**
     * @var Syslog
     */
    private $syslog;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    '*/*' => Response::FORMAT_HTML,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $module = Module::getInstance();
        if ($module) {
            $this->paycomService = $module->get('paycomService');
            $this->syslog = $module->syslog;
        }

        parent::init();
    }

    public function actionIndex()
    {
        $request = \Yii::$app->request;
        $paymentName = $request->get('paymentName');
        $clientId = $request->get('clientId');

        try {
            $profile = Profile::get($paymentName);
            $apiId = $profile->apiId;
            $url = $profile->isDebug ? Profile::URL_API_DEBUG : Profile::URL_API;
        } catch (\Exception $ex) {
            $this->logError("Card create error (Paycom): paymentName=\"{$paymentName}\", clientId=\"{$clientId}\", error=\"{$ex->getMessage()}\"");

            return $this->redirect('/paycom/registry/fail');
        }

        return $this->render('index', compact('paymentName', 'clientId', 'apiId', 'url'));
    }


    public function actionSave()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;
        if (!$request->isAjax) {
            return;
        }

        $paymentName = $request->post('paymentName');
        $clientId = $request->post('clientId');
        $token = $request->post('token');
        $urlExternal = \Yii::$app->params['my.externalUrl'];
        try {
            $customer = new Customer(compact('clientId'));
            $this->paycomService->setProfile($paymentName);
            $this->paycomService->createCard($customer, $token);
            $url = Url::toRoute('success', false);
        } catch (\Exception $ex) {
            $this->logError("Card create error (Paycom): paymentName=\"{$paymentName}\", clientId=\"{$clientId}\", token=\"{$token}\", error=\"{$ex->getMessage()}\"");
            $url = Url::toRoute('fail', false);
        }


        return compact('url');
    }


    public function actionSuccess()
    {
        return $this->render('success');
    }


    public function actionFail()
    {
        return $this->render('fail');
    }

    /**
     * @param string $message
     */
    private function sendToSyslog($message)
    {
        if ($this->syslog !== null) {
            $this->syslog->sendLog($message);
        }
    }

    /**
     * @param string $message
     */
    private function logError($message)
    {
        $this->sendToSyslog($message);
        \Yii::error($message);
    }
}