<?php

?>

<div class="wrapper" style="max-width: 550px; padding-left: 10px; padding-right: 10px; padding-top: 30px; margin-left: auto; margin-right: auto;">
    <div class="error" style="text-align: center; font-size: 18px; line-height: 1.5; color: #984847;">
        <img src="/images/error.svg" style="display: block; width: 61px; height: 61px; margin-left: auto; margin-right: auto; margin-bottom: 31px;">
        <p>Произошла ошибка:<br>данные карты введены неправильно или сервис временно не работает. Проверьте данные или попробуйте позже.</p>
    </div>
</div>
