<?php

use paycom\assets\PaycomAssets;
use yii\helpers\Json;

PaycomAssets::register($this);

/* @var $paymentName string */
/* @var $clientId string */
/* @var $apiId string */
/* @var $url string */
?>

<div class="container">
    <div class="form1-group">
        <div class="powered-by" style="text-align: center">
            <a href="https://payme.uz" title="payme">
                <img src="https://cdn.paycom.uz/documentation_assets/payme_1.png" height="70"
                     class="pull-ri1ght" alt="payme">
            </a>
        </div>
    </div>

    <form class="form-horizontal" id="card" data-payment-name="<?= $paymentName ?>" data-client-id="<?= $clientId ?>"
          data-api-id="<?= $apiId ?>" data-url="<?= $url ?>">
        <div id="create">
            <h3 class="text-center">Введите реквизиты банковской карты</h3>

            <div class="alert alert-danger error" style="display: none"></div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="number">Номер карты:</label>
                <div class="col-sm-10">
                    <input type="tel" class="form-control" maxlength="20" id="number" placeholder="Номер карты">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="month">Срок действия (месяц/год):</label>
                <div class="col-sm-10">
                    <div class="form-inline" style="overflow: hidden;">
                        <input style="float: left; width: 49%; margin-right: 2%;" type="tel" class="form-control" maxlength="2" id="month" placeholder="месяц">
                        <input style="float: left; width: 49%;" type="tel" class="form-control" maxlength="2" id="year" placeholder="год">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label><input type="checkbox" disabled checked> Сохранить карту</label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <p class="small">*Данные вашей карты не передаются данному сервису и сохраняются на безопасной
                        территории
                        сервиса <a href="https://payme.uz">"Payme"</a></p>
                    <p class="small">*Нажимая кнопку <b>"Продолжить"</b>, вы присоединяетесь к
                        <a href="https://cdn.payme.uz/terms/main.html">условиям оферты</a></p>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" id="create-card" class="btn btn-success">Продолжить</button>
                </div>
            </div>
        </div>

        <div id="verify" style="display: none">
            <div id="verify">
                <h3>На Ваш номер телефона <span id="phone"></span> был выслан код подтверждения</h3>

                <div class="alert alert-danger error" style="display: none"></div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="verify-code">Код подтверждения:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="verify-code" placeholder="Код подтверждения">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-5">
                        <button type="button" id="verify-card" class="btn btn-success">Продолжить</button>
                        <button type="button" id="repeat-create-card" class="btn btn-default">Выслать код повторно
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>