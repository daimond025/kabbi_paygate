<?php

?>

<div class="wrapper" style="max-width: 550px; padding-left: 10px; padding-right: 10px; padding-top: 30px; margin-left: auto; margin-right: auto;">
    <div class="success" style="text-align: center; padding-left: 10px; padding-right: 10px; font-size: 18px; line-height: 1.5;">
        <img src="/images/success.svg" style="display: block; width: 81px; height: 62px; margin-left: auto; margin-right: auto; margin-bottom: 30px;">
        <p>Ваша карта была успешно добавлена и вы можете использовать её для оплаты!</p>
    </div>
</div>