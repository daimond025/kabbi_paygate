<?php

namespace paycom\models;

/**
 * Class Card
 * @package paycom\models
 */
class Card
{
    /**
     * @var string
     */
    private $pan;

    /**
     * @var string
     */
    private $token;

    /**
     * Card constructor.
     *
     * @param string $pan
     * @param string $token
     */
    public function __construct($pan, $token)
    {
        $this->pan = $pan;
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}