<?php

namespace paycom\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "paycom_card".
 *
 * @property integer $id
 * @property string  $paymentName
 * @property string  $clientId
 * @property string  $pan
 * @property string  $token
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class PaycomCardDAO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paycom_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'clientId', 'pan', 'token'], 'required'],
            [['createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'clientId', 'pan'], 'string', 'max' => 32],
            [['token'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'clientId'    => 'Client ID',
            'pan'         => 'Pan',
            'token'       => 'Token',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }
}
