<?php

namespace paycom\models;

/**
 * Class Receipt
 * @package paycom\models
 */
class Receipt
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $state;

    /**
     * @var string
     */
    private $pan;

    /**
     * @var int
     */
    private $amount;

    /**
     * Receipt constructor.
     *
     * @param string $id
     * @param int    $state
     * @param string $pan
     * @param int    $amount
     */
    public function __construct($id, $state, $pan, $amount)
    {
        $this->id = $id;
        $this->state = $state;
        $this->pan = $pan;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }
}