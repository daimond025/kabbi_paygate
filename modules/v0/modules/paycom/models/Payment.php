<?php

namespace paycom\models;

/**
 * Class Payment
 * @package paycom\models
 */
class Payment
{
    /**
     * @var string
     */
    private $paymentName;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $receiptId;

    /**
     * @var int
     */
    private $amount;

    /**
     * Payment constructor.
     *
     * @param string $paymentName
     * @param string $clientId
     * @param string $orderNumber
     * @param string $token
     * @param string $receiptId
     * @param int    $amount
     */
    public function __construct($paymentName, $clientId, $orderNumber, $token, $receiptId, $amount)
    {
        $this->paymentName = $paymentName;
        $this->clientId = $clientId;
        $this->orderNumber = $orderNumber;
        $this->token = $token;
        $this->receiptId = $receiptId;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

}