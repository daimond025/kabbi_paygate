<?php

namespace paycom\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "paycom_profile".
 *
 * @property integer $id
 * @property string  $paymentName
 * @property string  $apiId
 * @property string  $password
 * @property integer $isDebug
 * @property double  $commission
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class ProfileDAO extends ActiveRecord
{
    const TYPE_ID = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paycom_profile';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'typeId'], 'required'],
            [['isDebug', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'apiId', 'password'], 'string', 'max' => 255],
            [['paymentName'], 'unique'],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'apiId'       => 'Api ID',
            'password'    => 'Password',
            'isDebug'     => 'Is Debug',
            'commission'  => 'Commission',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    public function setTypeId($value)
    {
    }

    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'paymentName',
            'typeId',
            'apiId',
            'password',
            'commission',
            'isDebug',
        ];
    }
}
