<?php

namespace paycom\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "paycom_receipt".
 *
 * @property integer $id
 * @property string  $paymentName
 * @property string  $clientId
 * @property string  $orderNumber
 * @property string  $token
 * @property string  $receiptId
 * @property integer $amount
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class PaycomPaymentDAO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paycom_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'clientId', 'orderNumber', 'token', 'receiptId'], 'required'],
            [['amount', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'clientId', 'orderNumber'], 'string', 'max' => 32],
            [['token', 'receiptId'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'clientId'    => 'Client ID',
            'orderNumber' => 'Order Number',
            'token'       => 'Token',
            'receiptId'   => 'Receipt ID',
            'amount'      => 'Amount',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }
}
