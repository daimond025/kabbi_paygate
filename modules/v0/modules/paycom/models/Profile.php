<?php

namespace paycom\models;

use yandex\exceptions\NotFoundProfileException;

/**
 * Class Profile
 * @package paycom\models
 */
class Profile extends ProfileDAO
{
    const URL_API = 'https://checkout.paycom.uz/api';
    const URL_API_DEBUG = 'https://checkout.test.paycom.uz/api';

    public static function get($paymentName)
    {
        $profile = self::findOne(['paymentName' => $paymentName]);
        if (!$profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }

        return $profile;
    }

    public function getApiUrl()
    {
        return (int)$this->isDebug === 1 ? self::URL_API_DEBUG : self::URL_API;
    }

}