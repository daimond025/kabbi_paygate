<?php

namespace yandex\components\httpClient;

use yandex\components\formatter\Formatter;

interface HttpClient
{
    const ERROR_MESSAGE_TIMEOUT = 'CURL Error or Timeout';

    /**
     *
     * @param string $uri
     * @param mixed  $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($uri, $data);

    /**
     *
     * @param \yandex\components\formatter\Formatter $formatter
     */
    public function setFormatter(Formatter $formatter);


}