<?php

namespace yandex\components\httpClient;

use app\components\syslog\Syslog;
use yii\base\Object;
use GuzzleHttp\Client;
use yandex\components\formatter\Formatter;
use yandex\components\formatter\NullFormatter;

class BasicHttpClient extends Object implements HttpClient
{
    /**
     *
     * @var \GuzzleHttp\Client
     */
    private $client;

    private $config = [];

    private $formatter;


    public function __construct(\GuzzleHttp\Client $client = null, $config = [])
    {
        $this->formatter = new NullFormatter();
        $this->client    = $client ? $client : new Client();
        parent::__construct($config);
    }

    public function setFormatter(Formatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function setTimeout($timeout)
    {
        $this->config['timeout'] = $timeout;
    }

    public function setAllowRedirects($value)
    {
        if ($value == false) {
            $this->config['allow_redirects'] = false;
        } elseif (array_key_exists('allow_redirects', $this->config)) {
            unset($this->config['allow_redirects']);
        }
    }

    /**
     *
     * @param string $uri
     * @param mixed  $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($uri, $data)
    {
        $config = array_merge_recursive($this->config, [
            'body'    => $this->formatter->format($data),
            'headers' => [
                'User-Agent'   => 'Mozilla/5.0 (Android; Mobile; rv:13.0) Gecko/13.0 Firefox/13.0',
                'Content-Type' => $this->formatter->getMimeType(),
            ],
        ]);

        return $this->request('POST', $uri, $config);
    }

    private function request($verb, $uri, $config)
    {
        try {
            $response = $this->client->request($verb, $uri, $config);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new HttpClientException(HttpClient::ERROR_MESSAGE_TIMEOUT, $e);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw new HttpClientException('Status code is 4xx', $e);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new HttpClientException('Status code is 5xx', $e);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw new HttpClientException('Illegal error', $e);
        }

        $this->addDebugLog($verb . ' ' . $uri, $config, $response->getBody());

        return $response;
    }

    private function addDebugLog($url, $request, $response)
    {
        try {
            /* @var $logger Syslog */
            $logger = \v0\Module::getComponent('syslog');

            // hide password on log
            if (array_key_exists('password', $request)) {
                $request[] = 'password';
                unset($request['password']);
            }

            $message = sprintf('%s %s -> %s', $url, json_encode($request), $response);
            $logger->addDebugLog($message);
        } catch (\yii\base\Exception $ignore) {
        }
    }

}