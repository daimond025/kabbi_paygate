<?php

namespace yandex\components\httpClient;

use app\components\syslog\Syslog;
use yii\base\Object;
use yandex\components\formatter\Formatter;
use GuzzleHttp\Client;
use yandex\components\formatter\NullFormatter;

class SecureHttpClient extends Object implements HttpClient
{

    private $configClient;

    private $formatter;

    public function __construct(\GuzzleHttp\Client $client = null, $config = [])
    {
        $this->configClient = $client ? $client->getConfig() : [];
        $this->formatter    = new NullFormatter();
        parent::__construct($config);
    }

    public function setFormatter(Formatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function setCertificate($path)
    {
        if (array_key_exists('cert', $this->configClient)) {
            $this->configClient['cert'][0] = $path;
        } else {
            $this->configClient['cert'] = [$path, ''];
        }
    }

    public function setCertificatePassword($pass)
    {
        if (array_key_exists('cert', $this->configClient)) {
            $this->configClient['cert'][1] = $pass;
        } else {
            $this->configClient['cert'] = ['', $pass];
        }
    }

    public function setSslKey($path)
    {
        $this->configClient['ssl_key'] = $path;
    }

    public function setBaseUri($uri)
    {
        $this->configClient['base_uri'] = $uri;
    }

    public function setVerifyCertificate($value)
    {
        $this->configClient['verify'] = (boolean)$value;
    }

    public function setTimeout($timeOnSeconds)
    {
        $this->configClient['timeout'] = $timeOnSeconds;
    }

    /**
     *
     * @param string $uri
     * @param mixed  $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($uri, $data)
    {
        $client = new Client($this->configClient);

        return $this->request($client, 'POST', $uri, [
            'body'    => $this->formatter->format($data),
            'headers' => [
                'Content-Type' => $this->formatter->getMimeType(),
            ],
        ]);
    }

    private function request($client, $verb, $uri, $config)
    {
        try {
            $response = $client->request($verb, $uri, $config);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new HttpClientException(HttpClient::ERROR_MESSAGE_TIMEOUT, $e);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw new HttpClientException('Status code is 4xx', $e);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new HttpClientException('Status code is 5xx', $e);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw new HttpClientException('Illegal error', $e);
        }

        $this->addDebugLog($verb . ' ' . $uri, $config, $response->getBody());

        return $response;
    }

    private function addDebugLog($url, $request, $response)
    {
        try {
            /* @var $logger Syslog */
            $logger = \v0\Module::getComponent('syslog');

            // hide password on log
            if (array_key_exists('password', $request)) {
                $request[] = 'password';
                unset($request['password']);
            }

            $message = sprintf('%s %s -> %s', $url, json_encode($request), $response);
            $logger->addDebugLog($message);
        } catch (\yii\base\Exception $ignore) {
        }
    }

}