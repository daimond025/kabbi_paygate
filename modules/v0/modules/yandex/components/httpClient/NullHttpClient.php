<?php

namespace yandex\components\httpClient;

use yii\base\Object;

class NullHttpClient extends Object implements HttpClient
{
    
    public function post($uri, $data)
    {
        // stubs 
    }
}