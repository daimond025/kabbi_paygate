<?php

namespace yandex\components\httpClient;

use yii\base\UserException;

class HttpClientException extends UserException
{
    
    public function __construct($message, $previous = null)
    {
        parent::__construct($message, 255, $previous);
    }
    
    public function getName()
    {
        return 'Http Client Exception';
    }
}