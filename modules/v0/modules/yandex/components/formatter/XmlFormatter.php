<?php

namespace yandex\components\formatter;

use yii\base\Object;

class XmlFormatter extends Object implements Formatter
{
    
    private $root;
    
    private $parent;
    
    public function __construct($rootName, Formatter $formatter = null, $config = [])
    {
        $this->root = $rootName;
        $this->parent = $formatter ? $formatter : new NullFormatter();
        parent::__construct($config);
    }
    
    public function format($data)
    {
        $data = $this->parent->format($data);
        return $this->asXml($data);
    }
    
    protected function asXml(array $data)
    {
        $root = "<{$this->root} />";
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>' . $root);
        foreach ($data as $key => $oneData) {
            $xml->addAttribute($key, $oneData);
        }
        return $xml->asXML();
    }
    
    public function getMimeType()
    {
        return 'application/xml';
    }
    
}