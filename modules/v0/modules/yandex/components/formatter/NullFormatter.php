<?php

namespace yandex\components\formatter;

use yii\base\Object;

class NullFormatter extends Object implements Formatter
{
    
    public function format($data)
    {
        return $data;
    }
    
    public function getMimeType()
    {
        return '';
    }
}