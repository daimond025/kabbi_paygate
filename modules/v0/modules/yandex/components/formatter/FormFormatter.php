<?php

namespace yandex\components\formatter;

use yii\base\Object;

class FormFormatter extends Object implements Formatter
{
    
    private $parent;
    
    public function __construct(Formatter $formatter = null, $config = [])
    {
        $this->parent = $formatter ? $formatter : new NullFormatter();
        parent::__construct($config);
    }
    
    public function format($data)
    {
        $data = $this->parent->format($data);
        return http_build_query($data);
    }
    
    public function getMimeType()
    {
        return 'application/x-www-form-urlencoded';
    }
}