<?php

namespace yandex\components\formatter;

interface Formatter
{
    
    /**
     * @param mixed $data
     * @return mixed
     */
    public function format($data);
    
    
    /**
     * @return string
     */
    public function getMimeType();
    
}