<?php

namespace yandex\components\formatter;

use yii\base\Object;

class CmsFormatter extends Object implements Formatter
{
    
    private $certFilePath;
    
    private $keyFilePath;
    
    private $parent;
    
    public function __construct($certFilePath, $keyFilePath, Formatter $formatter = null, $config = [])
    {
        $this->certFilePath = $certFilePath;
        $this->keyFilePath = $keyFilePath;
        $this->parent = $formatter ? $formatter : new NullFormatter();
        parent::__construct($config);  
    }
    
    
    public function format($data)
    {
        $data = $this->parent->format($data);
        return $this->encrypt($data);  
    }
    
    protected function encrypt($data)
    {
        try {
            $pipes = array();
            $process = proc_open('openssl smime -sign -signer ' . $this->certFilePath . ' -inkey ' . $this->keyFilePath . ' -nochain -nocerts -outform PEM -nodetach', array(
                array("pipe", "r"),
                array("pipe", "w"),
                array("pipe", "w")
            ), $pipes);
            if (is_resource($process)) {
                fwrite($pipes[0], $data);
                fclose($pipes[0]);
                $pkcs7 = stream_get_contents($pipes[1]);
                fclose($pipes[1]);
                proc_close($process);
                return $pkcs7;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    public function getMimeType()
    {
        return 'application/pkcs7-mime';
    }
}