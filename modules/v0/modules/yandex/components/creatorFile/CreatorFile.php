<?php

namespace yandex\components\creatorFile;

use yii\base\Object;
use Keboola\Temp\Temp;

class CreatorFile extends Object
{
    
    private $_creator;
    
    public function init()
    {
        $this->_creator = new Temp();
    }
    /**
     * @param string $content
     * @throws CreatorFileException
     * @return SplFileInfo
     */
    public function createTmpFile($content)
    {
        $fileInfo = $this->createUniqueTmpFile();
        
        $file = $this->openFileOnWrite($fileInfo);
        if ($file->fwrite($content) < strlen($content)) {
            throw new CreatorFileException('Don\'t write content to the file');
        }
        
        return $fileInfo;
    }
    
    /**
     * 
     * @throws CreatorFilesException
     * @return \SplFileInfo
     */
    protected function createUniqueTmpFile()
    {
        try {
            return $this->_creator->createTmpFile();
        } catch (\Exception $e) {
            throw new CreatorFileException('Don\'t create temp file', $e);
        }
    }
    /**
     * 
     * @param \SplFileInfo $fileInfo
     * @throws CreatorFilesException
     * @return \SplFileObject
     */
    protected function openFileOnWrite(\SplFileInfo $fileInfo)
    {
        try {
            return $fileInfo->openFile('w+');
        } catch (\RuntimeException $e) {
            throw new CreatorFileException('Don\'t open file', $e);
        }
    }
    
}