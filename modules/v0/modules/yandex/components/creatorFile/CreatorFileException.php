<?php

namespace yandex\components\creatorFile;

use yii\base\UserException;

class CreatorFileException extends UserException
{
    
    public function __construct($message, $previous)
    {
        parent::__construct($message, 255, $previous);
    }
    
    public function getName()
    {
        return 'Creator File exception';
    }
}