<?php

namespace yandex\components\queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class Consumer
 * @package yandex\components\queue
 */
class Consumer
{
    const MAX_RETRY_COUNT = 5;

    /**
     * @var MQConnection
     */
    private $connection;
    /**
     * @var CancelPaymentService
     */
    private $cancelPaymentService;

    /**
     * Consumer constructor.
     *
     * @param MQConnection         $connection
     * @param CancelPaymentService $cancelPaymentService
     */
    public function __construct(MQConnection $connection, CancelPaymentService $cancelPaymentService)
    {
        $this->connection           = $connection;
        $this->cancelPaymentService = $cancelPaymentService;
    }

    public function start()
    {
        $connection = new AMQPStreamConnection($this->connection->getHost(), $this->connection->getPort(),
            $this->connection->getUser(), $this->connection->getPassword(), $this->connection->getVirtualHost());

        $channel = $connection->channel();
        $channel->exchange_declare(CancelPaymentService::EXCHANGE_CANCEL_PAYMENT, 'fanout', false, true, false);

        $queue = CancelPaymentService::QUEUE_CANCEL_PAYMENT['name'];
        $channel->queue_declare($queue, false, true, false, false);
        $channel->queue_bind($queue, CancelPaymentService::EXCHANGE_CANCEL_PAYMENT);

        $channel->basic_consume($queue, '', false, false, false, false,
            [$this->cancelPaymentService, 'cancelPaymentCallback']);

        echo "Consumer started:\n";
        while (count($channel->callbacks)) {
            $channel->wait();
        }
        echo "Consumer stopped.\n";

        $channel->close();
        $connection->close();
    }
}