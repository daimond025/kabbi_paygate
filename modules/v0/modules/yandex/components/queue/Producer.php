<?php

namespace yandex\components\queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Class Producer
 * @package yandex\components\queue
 */
class Producer
{
    /**
     * @var MQConnection
     */
    private $connection;

    /**
     * Consumer constructor.
     *
     * @param MQConnection $connection
     */
    public function __construct(MQConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     * @param int    $attemptNumber
     * @param string $queueName
     */
    public function cancelPayment($paymentName, $orderNumber, $attemptNumber, $queueName)
    {
        $connection = new AMQPStreamConnection($this->connection->getHost(), $this->connection->getPort(),
            $this->connection->getUser(), $this->connection->getPassword(), $this->connection->getVirtualHost());

        $channel = $connection->channel();
        $channel->exchange_declare(CancelPaymentService::EXCHANGE_DEFERRED, 'direct', false, true, false);
        $channel->exchange_declare(CancelPaymentService::EXCHANGE_CANCEL_PAYMENT, 'fanout', false, true, false);

        foreach ([
                     CancelPaymentService::QUEUE_DEFERRED_1_MIN,
                     CancelPaymentService::QUEUE_DEFERRED_5_MIN,
                     CancelPaymentService::QUEUE_DEFERRED_30_MIN,
                 ] as $queue) {
            $channel->queue_declare($queue['name'], false, true, false, false, false, new AMQPTable([
                'x-dead-letter-exchange' => CancelPaymentService::EXCHANGE_CANCEL_PAYMENT,
                'x-message-ttl'          => $queue['ttl'] * 1000 * 60,
            ]));
            $channel->queue_bind($queue['name'], CancelPaymentService::EXCHANGE_DEFERRED, $queue['name']);
        }

        $data = [
            'paymentName'   => $paymentName,
            'orderNumber'   => $orderNumber,
            'attemptNumber' => $attemptNumber,
        ];

        $msg = new AMQPMessage(json_encode($data), ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $channel->basic_publish($msg, CancelPaymentService::EXCHANGE_DEFERRED, $queueName);

        $channel->close();
        $connection->close();
    }

}