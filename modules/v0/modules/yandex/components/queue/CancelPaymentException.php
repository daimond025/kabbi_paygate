<?php

namespace yandex\components\queue;

/**
 * Class CancelPaymentException
 * @package yandex\components\queue
 */
class CancelPaymentException extends \yii\web\HttpException
{
    const EXCEPTION_NAME = 'Cancel payment exception';

    public function __construct($message = null, \Exception $previous = null)
    {
        if ($message === null) {
            $message = self::EXCEPTION_NAME;
        }
        parent::__construct(500, $message, 4, $previous);
    }

    public function getName()
    {
        return self::EXCEPTION_NAME;
    }

}