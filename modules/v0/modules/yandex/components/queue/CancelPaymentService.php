<?php

namespace yandex\components\queue;

use app\components\syslog\Syslog;
use PhpAmqpLib\Message\AMQPMessage;
use yandex\components\yandex\MerchantWebService;
use yandex\models\client\response\CancelPayment;
use yandex\models\client\response\Order;
use yandex\models\client\response\ReturnPayment;
use yandex\models\Profile;
use yandex\models\YandexPayment;
use yii\base\Application;
use yii\base\InvalidConfigException;

/**
 * Class QueueManager
 * @package yandex\components\queue
 */
class CancelPaymentService
{
    const EXCHANGE_CANCEL_PAYMENT = 'EXCHANGE_CANCEL_PAYMENT';
    const EXCHANGE_DEFERRED = 'EXCHANGE_DEFERRED';

    const QUEUE_DEFERRED_1_MIN = ['name' => 'QUEUE_DEFERRED_1_MIN', 'ttl' => 1];
    const QUEUE_DEFERRED_5_MIN = ['name' => 'QUEUE_DEFERRED_5_MIN', 'ttl' => 5];
    const QUEUE_DEFERRED_30_MIN = ['name' => 'QUEUE_DEFERRED_30_MIN', 'ttl' => 30];
    const QUEUE_CANCEL_PAYMENT = ['name' => 'QUEUE_CANCEL_PAYMENT', 'ttl' => null];

    const MAX_ATTEMPT_COUNT = 7;

    /**
     * @var MQConnection
     */
    private $connection;

    /**
     * @var Application
     */
    private $application;

    /**
     * @var Syslog
     */
    private $syslog;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var MerchantWebService
     */
    private $service;


    /**
     * CancelPaymentService constructor.
     *
     * @param MQConnection $connection
     * @param Application  $application
     * @param Syslog       $syslog
     */
    public function __construct(MQConnection $connection, Application $application, Syslog $syslog)
    {
        $this->connection = $connection;
        $this->application = $application;
        $this->syslog = $syslog;
    }

    /**
     * @param int $attemptNumber
     *
     * @return array
     */
    public function getQueueByAttemptNumber($attemptNumber = 1)
    {
        switch ($attemptNumber) {
            case 1:
                return self::QUEUE_DEFERRED_1_MIN;
            case 2:
            case 3:
            case 4:
                return self::QUEUE_DEFERRED_5_MIN;
            default:
                return self::QUEUE_DEFERRED_30_MIN;
        }
    }

    /**
     * @return Consumer
     */
    public function startConsumer()
    {
        $consumer = new Consumer($this->connection, $this);
        $consumer->start();
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     * @param int    $attemptNumber
     *
     * @throws CancelPaymentException
     */
    public function publish($paymentName, $orderNumber, $attemptNumber = 1)
    {
        if ($attemptNumber > self::MAX_ATTEMPT_COUNT) {
            throw new CancelPaymentException("Publish error: paymentName=\"{$paymentName}\", orderNumber=\"{$orderNumber}\", attemptNumber=\"{$attemptNumber}\", error=\"Attempt count was exceeded\"");
        }

        $producer = new Producer($this->connection);
        $queue = $this->getQueueByAttemptNumber($attemptNumber);
        $producer->cancelPayment($paymentName, $orderNumber, $attemptNumber, $queue['name']);
    }

    /**
     * @param AMQPMessage $msg
     */
    public function cancelPaymentCallback($msg)
    {
        $data = json_decode($msg->getBody(), true);
        $paymentName = isset($data['paymentName']) ? $data['paymentName'] : null;
        $orderNumber = isset($data['orderNumber']) ? $data['orderNumber'] : null;
        $attemptNumber = isset($data['attemptNumber']) ? $data['attemptNumber'] : null;

        try {
            $this->cancelPayment($paymentName, $orderNumber, $attemptNumber);

            $message = "Payment was canceled: paymentName=\"{$paymentName}\", orderNumber=\"$orderNumber\", attemptNumber=\"$attemptNumber\"";
            echo "$message\n";
            $this->syslog->sendLog($message);
        } catch (\Exception $e) {
            $message = "Cancel payment consumer error: paymentName=\"{$paymentName}\", orderNumber=\"$orderNumber\", attemptNumber=\"$attemptNumber\", error=\"{$e->getMessage()}\"";
            echo "$message\n";
            $this->syslog->sendLog($message);
            \Yii::error($message, 'cancel-payment');
        }

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

        $this->application->trigger('closeConnection');

    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     * @param int    $attemptNumber
     *
     * @throws InvalidConfigException
     * @throws CancelPaymentException
     */
    private function cancelPayment($paymentName, $orderNumber, $attemptNumber)
    {
        $this->profile = Profile::get($paymentName);
        $this->service = new MerchantWebService($this->profile->urlMWS, $this->profile->certificate,
            $this->profile->key, $this->profile->certificatePassword);

        if ($this->doCancelPayment($this->profile->shopId, $orderNumber, $attemptNumber)) {
            return;
        }

        if ($this->doCancelPayment($this->profile->secureDealShopId, $orderNumber, $attemptNumber)) {
            return;
        }

        if ($this->doCancelPayment($this->profile->secureDealPaymentShopId, $orderNumber, $attemptNumber)) {
            return;
        }
    }

    /**
     * @param int $shopId
     * @param int $orderNumber
     * @param int $attemptNumber
     *
     * @return bool
     * @throws \yandex\components\queue\CancelPaymentException
     * @throws \yii\base\InvalidConfigException
     */
    private function doCancelPayment($shopId, $orderNumber, $attemptNumber)
    {
        $listOrders = $this->service->listOrders($shopId, $orderNumber);
        if ($listOrders->orderCount > 0) {
            $orders = $listOrders->getOrders();
            /* @var $order Order */
            $order = current($orders);

            $this->cancelSecureDealPayment($order, $attemptNumber);

            return true;
        }

        return false;
    }

    /**
     * @param Order $order
     * @param int   $attemptNumber
     *
     * @throws CancelPaymentException
     * @throws InvalidConfigException
     */
    private function returnPaymentForOrder(Order $order, $attemptNumber)
    {
        if ($order->paid === 'true') {
            $returnPayment = $this->service->returnPayment($this->service->generateClientOrderId(), $order->shopId,
                $order->invoiceId, $order->orderSumAmount, $order->orderSumCurrencyPaycash);

            $this->saveReturnPaymentResult($order->orderNumber, $order->shopId, $returnPayment);

            $status = (int)$returnPayment->status;
            if ($status === 3) {
                throw new CancelPaymentException("Return payment error: shopId=\"$order->shopId\", invoiceId=\"$order->invoiceId\", sum=\"$order->orderSumAmount\", currency=\"$order->orderSumCurrencyPaycash\", error=\"{$returnPayment->techMessage} ($returnPayment->error)\"");
            }

            if ($status === 1) {
                $this->publish($this->profile->paymentName, $order->orderNumber, $attemptNumber + 1);

                throw new CancelPaymentException("Return payment in processing: shopId=\"$order->shopId\", invoiceId=\"$order->invoiceId\", sum=\"$order->orderSumAmount\", currency=\"$order->orderSumCurrencyPaycash\"");
            }
        }
    }

    /**
     * @param Order $order
     * @param int   $attemptNumber
     *
     * @throws CancelPaymentException
     * @throws InvalidConfigException
     */
    private function cancelSecureDealPayment(Order $order, $attemptNumber)
    {
        if ($order->clearing === Order::CLEARING_NONE) {
            $cancelPayment = $this->service->cancelPayment($order->invoiceId);

            $this->saveCancelPaymentResult($order->orderNumber, $order->shopId, $cancelPayment);

            $status = (int)$cancelPayment->status;
            if ($status === 3) {
                throw new CancelPaymentException("Return payment error: shopId=\"$order->shopId\", invoiceId=\"$order->invoiceId\", sum=\"$order->orderSumAmount\", currency=\"$order->orderSumCurrencyPaycash\", error=\"{$cancelPayment->techMessage} ($cancelPayment->error)\"");
            }

            if ($status === 1) {
                $this->publish($this->profile->paymentName, $order->orderNumber, $attemptNumber + 1);

                throw new CancelPaymentException("Return payment in processing: shopId=\"$order->shopId\", invoiceId=\"$order->invoiceId\", sum=\"$order->orderSumAmount\", currency=\"$order->orderSumCurrencyPaycash\"");
            }
        }

        if ($order->clearing === Order::CLEARING_SALE) {
            $this->returnPaymentForOrder($order, $attemptNumber);
        }
    }

    /**
     * @param string        $orderNumber
     * @param int           $shopId
     * @param ReturnPayment $returnPayment
     */
    private function saveReturnPaymentResult($orderNumber, $shopId, $returnPayment)
    {
        try {
            $yandexPayment = YandexPayment::get($orderNumber, $shopId);
            $yandexPayment->setReturnPaymentResult($returnPayment->clientOrderId, $returnPayment->status,
                $returnPayment->error, $returnPayment->techMessage, $returnPayment->processedDT);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'yandex-payment');
        }
    }

    /**
     * @param string        $orderNumber
     * @param int           $shopId
     * @param CancelPayment $cancelPayment
     */
    private function saveCancelPaymentResult($orderNumber, $shopId, $cancelPayment)
    {
        try {
            $yandexPayment = YandexPayment::get($orderNumber, $shopId);
            $yandexPayment->setCancelPaymentResult($cancelPayment->clientOrderId, $cancelPayment->status,
                $cancelPayment->error, $cancelPayment->techMessage, $cancelPayment->processedDT);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'yandex-payment');
        }
    }

}