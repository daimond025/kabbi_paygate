<?php

namespace yandex\components\parser;

interface Parser
{
    /**
     * 
     * @param string $data
     * @return array
     */
    public function parse($data);
    
}