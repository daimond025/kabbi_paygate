<?php

namespace yandex\components\parser;

use yii\base\UserException;

class ParserException extends UserException
{
    
    public function __construct($message, $previous)
    {
        parent::__construct($message, 255, $previous);
    }
    
    public function getName()
    {
        return 'Parser Exception';
    }
}