<?php

namespace yandex\components\parser;

use yii\base\Object;

class XmlAttributeParser extends Object implements Parser
{
    
    public function parse($data)
    {
        $sxml = $this->getSxmlElement($data);
        return $this->attributeAsArray($sxml);
    }
    
    public function getSxmlElement($data)
    {
        try {
            return new \SimpleXMLElement($data);
        } catch (\Exception $e) {
            throw new ParserException('Don\'t create sxml object', $e);
        }
    }
    
    public function attributeAsArray(\SimpleXMLElement $sxml)
    {
        $attributes = $sxml->attributes(); 
        
        foreach ($attributes as $key => $value) {
            $res[$key] = (string) $value;
        }
        return $res;
    }
    
}