<?php

namespace yandex\components\yandex;

use v0\Module;
use yandex\components\formatter\Formatter;
use yandex\components\httpClient\HttpClient;
use yandex\components\httpClient\HttpClientException;
use yandex\components\httpClient\SecureHttpClient;
use yandex\models\client\response\CancelPayment;
use yandex\models\client\response\ConfirmDeposition;
use yandex\models\client\response\ListOrders;
use yandex\models\client\response\ReturnPayment;
use yandex\models\client\response\RepeatCardPayment;
use yandex\components\formatter\FormFormatter;
use yandex\components\formatter\XmlFormatter;
use yandex\components\formatter\CmsFormatter;
use yii\base\InvalidConfigException;

class MerchantWebService
{
    const METHOD_LIST_ORDERS = 'listOrders';

    const METHOD_REPEAT_CARD_PAYMENT = 'repeatCardPayment';

    const METHOD_CONFIRM_DEPOSITION = 'confirmDeposition';

    const METHOD_CANCEL_PAYMENT = 'cancelPayment';

    const METHOD_RETURN_PAYMENT = 'returnPayment';

    /**
     * @var string
     */
    private $urlMWS;

    /**
     * @var \SplFileInfo
     */
    private $certificate;

    /**
     * @var string
     */
    private $certificatePassword;

    /**
     * @var \SplFileInfo
     */
    private $key;

    /**
     * @var int
     */
    private $networkTimeout;

    /**
     * MerchantWebService constructor.
     *
     * @param string       $urlMWS
     * @param \SplFileInfo $certificate
     * @param \SplFileInfo $key
     * @param string       $certificatePassword
     * @param int          $networkTimeout
     */
    public function __construct($urlMWS, $certificate, $key, $certificatePassword, $networkTimeout = 10)
    {
        $this->urlMWS = $urlMWS;
        $this->certificate = $certificate;
        $this->key = $key;
        $this->certificatePassword = $certificatePassword;
        $this->networkTimeout = $networkTimeout;
    }

    /**
     * @param int    $shopId
     * @param string $orderNumber
     *
     * @return ListOrders
     * @throws InvalidConfigException
     */
    public function listOrders($shopId, $orderNumber)
    {
        $requestParams = [
            'requestDT'   => $this->generateRequestDatetime(),
            'shopId'      => $shopId,
            'orderNumber' => $orderNumber,
        ];

        $formatter = $this->getFormatter('form');
        $response = $this->getClient($formatter)->post(self::METHOD_LIST_ORDERS, $requestParams);
        $response = $response->getBody();

        $listOrders = new ListOrders();
        $listOrders->loadXml($response);

        return $listOrders;
    }

    /**
     * @param int $shopId
     * @param int $fromTime
     * @param int $toTime
     *
     * @return ListOrders
     */
    public function getNotPaidOrders($shopId, $fromTime, $toTime)
    {
        $requestParams = [
            'requestDT'                          => $this->generateRequestDatetime(),
            'shopId'                             => $shopId,
            'paid'                               => 'false',
            'orderCreatedDatetimeGreaterOrEqual' => $this->generateRequestDatetime($fromTime),
            'orderCreatedDatetimeLessOrEqual'    => $this->generateRequestDatetime($toTime),
        ];

        $formatter = $this->getFormatter('form');
        $response = $this->getClient($formatter)->post(self::METHOD_LIST_ORDERS, $requestParams);
        $response = $response->getBody();

        $listOrders = new ListOrders();
        $listOrders->loadXml($response);

        return $listOrders;
    }

    /**
     * @param int    $clientOrderId
     * @param string $orderNumber
     * @param int    $shopId
     * @param int    $shopArticleId
     * @param int    $invoiceId
     * @param float  $amount
     * @param string|null $receipt
     *
     * @return RepeatCardPayment
     * @throws InvalidConfigException
     */
    public function repeatCardPayment($clientOrderId, $orderNumber, $shopId, $shopArticleId, $invoiceId, $amount, $receipt = null)
    {
        $requestParams = [
            'clientOrderId' => $clientOrderId,
            'invoiceId'     => $invoiceId,
            'amount'        => $amount,
            'orderNumber'   => $orderNumber,
        ];

        if (!empty($shopId)) {
            $requestParams['shopId'] = $shopId;
        }

        if (!empty($shopArticleId)) {
            $requestParams['shopArticleId'] = $shopArticleId;
        }

        if (!empty($receipt)) {
            $requestParams['ym_merchant_receipt'] = $receipt;
        }

        $formatter = $this->getFormatter('form');
        $httpClient = $this->getClient($formatter);

        $response = $httpClient->post(self::METHOD_REPEAT_CARD_PAYMENT, $requestParams);
        $response = $response->getBody();

        $repeatCardPayment = new RepeatCardPayment();
        $repeatCardPayment->loadXml($response);

        return $repeatCardPayment;
    }

    /**
     * @param int    $clientOrderId
     * @param int    $invoiceId
     * @param string $destination
     * @param float  $amount
     * @param string $currency
     *
     * @return ConfirmDeposition
     */
    public function confirmDeposition($clientOrderId, $invoiceId, $destination, $amount, $currency)
    {
        $requestParams = [
            'clientOrderId' => $clientOrderId,
            'requestDT'     => $this->generateRequestDatetime(),
            'invoiceId'     => $invoiceId,
            'destination'   => $destination,
            'amount'        => sprintf('%.2f', $amount),
            'currency'      => $currency,
            'offerAccepted' => 'true',
        ];

        $formatter = $this->getFormatter('form');
        $httpClient = $this->getClient($formatter);

        $response = $httpClient->post(self::METHOD_CONFIRM_DEPOSITION, $requestParams);
        $response = $response->getBody();

        $confirmDeposition = new ConfirmDeposition();
        $confirmDeposition->loadXml($response);

        return $confirmDeposition;
    }

    /**
     * @param int $invoiceId
     *
     * @return CancelPayment
     * @throws InvalidConfigException
     */
    public function cancelPayment($invoiceId)
    {
        $requestParams = [
            'requestDT' => $this->generateRequestDatetime(),
            'orderId'   => $invoiceId,
        ];

        $formatter = $this->getFormatter('form');
        $response = $this->getClient($formatter)->post(self::METHOD_CANCEL_PAYMENT, $requestParams);
        $response = $response->getBody();

        $cancelPayment = new CancelPayment();
        $cancelPayment->loadXml($response);

        return $cancelPayment;
    }

    /**
     * @param int    $clientOrderId
     * @param int    $shopId
     * @param int    $invoiceId
     * @param float  $amount
     * @param string $currency
     *
     * @return ReturnPayment
     * @throws InvalidConfigException
     */
    public function returnPayment($clientOrderId, $shopId, $invoiceId, $amount, $currency)
    {
        $requestParams = [
            'requestDT'     => $this->generateRequestDatetime(),
            'clientOrderId' => $clientOrderId,
            'invoiceId'     => $invoiceId,
            'shopId'        => $shopId,
            'amount'        => sprintf('%.2f', $amount),
            'currency'      => $currency,
            'cause'         => 'Need',
        ];

        $xmlFormatter = $this->getFormatter('xml', 'returnPaymentRequest');
        $formatter = $this->getFormatter('cms', $xmlFormatter);

        $response = $this->getClient($formatter)->post(self::METHOD_RETURN_PAYMENT, $requestParams);
        $response = $response->getBody();

        $returnPayment = new ReturnPayment();
        $returnPayment->loadXml($response);

        return $returnPayment;
    }

    /**
     * @return int
     */
    public function generateClientOrderId()
    {
        return round(gettimeofday(true) * 1e3);
    }


    /**
     * @param string $name
     * @param array  $params
     *
     * @return Formatter
     */
    private function getFormatter($name, $params = null)
    {
        switch ($name) {
            case 'cms':
                return new CmsFormatter($this->certificate->getRealPath(), $this->key->getRealPath(), $params);
            case 'xml':
                return new XmlFormatter($params);
            case 'form':
            default:
                return new FormFormatter($params);
        }
    }

    /**
     * @param Formatter $formatter
     *
     * @return SecureHttpClient
     * @throws InvalidConfigException
     */
    private function getClient(Formatter $formatter)
    {
        /* @var $client SecureHttpClient */
        $client = \Yii::createObject([
            'class'               => '\yandex\components\httpClient\SecureHttpClient',
            'timeout'             => $this->networkTimeout,
            'baseUri'             => $this->urlMWS,
            'certificate'         => $this->certificate->getRealPath(),
            'certificatePassword' => $this->certificatePassword,
            'verifyCertificate'   => false,
            'sslKey'              => $this->key->getRealPath(),
            'formatter'           => $formatter,
        ]);

        return $client;
    }

    /**
     * @param int $time
     *
     * @return string
     */
    private function generateRequestDatetime($time = null)
    {
        return date('Y-m-d\TH:i:sP', $time ?: time());
    }

}