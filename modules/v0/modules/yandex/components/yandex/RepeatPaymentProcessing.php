<?php

namespace yandex\components\yandex;

use yandex\exceptions\ProcessingTimeoutException;
use yandex\exceptions\YandexException;
use yandex\exceptions\YandexPaymentException;
use yandex\models\client\response\ConfirmDeposition;
use yandex\models\client\response\RepeatCardPayment;
use yandex\models\YandexPayment;
use yii\base\InvalidConfigException;
use yii\db\Exception;

/**
 * Class RepeatPaymentProcessing
 * @package yandex\components
 */
class RepeatPaymentProcessing
{
    const WAIT_SECONDS = 2;

    /**
     * @var MerchantWebService
     */
    private $service;

    /**
     * @var YandexPayment
     */
    private $yandexPayment;

    /**
     * @var int
     */
    private $processingTimeout;

    /**
     * @var int
     */
    private $startTime;

    /**
     * RepeatPaymentProcessing constructor.
     *
     * @param MerchantWebService $service
     * @param int                $processingTimeout
     */
    public function __construct(MerchantWebService $service, $processingTimeout = 30)
    {
        $this->service = $service;
        $this->processingTimeout = $processingTimeout;
    }

    /**
     * @param string $paymentName
     * @param string $customerNumber
     * @param string $orderNumber
     * @param int    $baseInvoiceId
     * @param float  $sum
     * @param int    $shopId
     * @param int    $shopArticleId
     * @param string $destination
     * @param string|null $receipt
     *
     * @throws YandexPaymentException
     * @throws YandexException
     */
    public function execute(
        $paymentName,
        $customerNumber,
        $orderNumber,
        $baseInvoiceId,
        $sum,
        $shopId,
        $shopArticleId = null,
        $destination = null,
        $receipt = null
    ) {
        $this->startTime = time();

        try {
            $this->yandexPayment = YandexPayment::register($paymentName, $customerNumber, $orderNumber,
                $shopId, $sum, $baseInvoiceId, $destination);

            $this->repeatCardPayment($orderNumber, $baseInvoiceId, $sum, $shopId, $shopArticleId, $receipt);

            $avisoData = $this->waitAndGetAvisoData();

            if ($destination !== null) {
                $this->waitConfirmDeposition($avisoData['avisoInvoiceId'], $destination, $avisoData['avisoSum'],
                    $avisoData['avisoCurrency']);
            }

            $this->saveResult(YandexPayment::RESULT_SUCCESS);
        } catch (\Exception $e) {
            $this->saveResult(YandexPayment::RESULT_FAIL);

            // cancel payment
            throw new YandexException("Repeat payment processing error: orderNumber=\"{$orderNumber}\", error=\"{$e->getMessage()}\"",
                $e);
        }
    }

    /**
     * @param string $orderNumber
     * @param int    $baseInvoiceId
     * @param float  $sum
     * @param int    $shopId
     * @param int    $shopArticleId
     * @param string|null $receipt
     *
     * @throws YandexException
     * @throws InvalidConfigException
     * @throws YandexPaymentException
     */
    private function repeatCardPayment($orderNumber, $baseInvoiceId, $sum, $shopId, $shopArticleId, $receipt = null)
    {
        $repeatCardPayment = $this->service->repeatCardPayment($this->service->generateClientOrderId(), $orderNumber,
            $shopId, $shopArticleId, $baseInvoiceId, $sum, $receipt);
        $this->saveRepeatCardPaymentResult($repeatCardPayment);

        $status = (int)$repeatCardPayment->status;
        if ($status === RepeatCardPayment::STATUS_FAIL) {
            throw new YandexException("Repeat card payment error: code=\"{$repeatCardPayment->error}\", error=\"{$repeatCardPayment->techMessage}\"");
        }

    }

    /**
     * @return array
     * @throws ProcessingTimeoutException
     */
    private function waitAndGetAvisoData()
    {
        while (true) {
            try {
                $avisoData = $this->yandexPayment->getAvisoData();
            } catch (\Exception $ignore) {
                $avisoData = null;
            }

            if (!empty($avisoData['avisoInvoiceId'])) {
                return $avisoData;
            }

            $this->checkProcessingTimeout();

            sleep(self::WAIT_SECONDS);
        }
    }

    /**
     * @param int    $invoiceId
     * @param string $destination
     * @param float  $sum
     * @param string $currency
     *
     * @throws YandexException
     * @throws YandexPaymentException
     * @throws ProcessingTimeoutException
     */
    private function waitConfirmDeposition($invoiceId, $destination, $sum, $currency)
    {
        $clientOrderId = $this->service->generateClientOrderId();

        while (true) {
            $confirmDeposition = $this->service->confirmDeposition($clientOrderId, $invoiceId, $destination, $sum,
                $currency);
            $this->saveConfirmDepositionResult($confirmDeposition);

            $status = (int)$confirmDeposition->status;
            if ($status === ConfirmDeposition::STATUS_SUCCESSFUL) {
                return;
            }

            if ($status === ConfirmDeposition::STATUS_FAILED) {
                throw new YandexException("Confirm deposition error: error=\"{$confirmDeposition->error}\"");
            }

            try {
                $this->checkProcessingTimeout();
            } catch (ProcessingTimeoutException $ignore) {
                return;
            }

            sleep(self::WAIT_SECONDS);
        }
    }

    /**
     * @param string $result
     */
    private function saveResult($result)
    {
        try {
            if ($this->yandexPayment === null) {
                throw new Exception("Error save yandex payment result: result=\"{$result}\", error=\"Payment not found in DB\"");
            }
            $this->yandexPayment->setResult($result);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'yandex-payment');
        }
    }

    /**
     * @param RepeatCardPayment $repeatCardPayment
     */
    private function saveRepeatCardPaymentResult($repeatCardPayment)
    {
        try {
            $this->yandexPayment->setRepeatPaymentResult($repeatCardPayment->clientOrderId, $repeatCardPayment->status,
                $repeatCardPayment->error, $repeatCardPayment->techMessage, $repeatCardPayment->processedDT);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'yandex-payment');
        }
    }

    /**
     * @param ConfirmDeposition $confirmDeposition
     */
    private function saveConfirmDepositionResult($confirmDeposition)
    {
        try {
            $this->yandexPayment->setConfirmDepositionResult($confirmDeposition->clientOrderId,
                $confirmDeposition->status, $confirmDeposition->error, $confirmDeposition->invoiceId,
                $confirmDeposition->requestDT);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'yandex-payment');
        }
    }

    /**
     * @throws ProcessingTimeoutException
     */
    private function checkProcessingTimeout()
    {
        if (time() > $this->startTime + $this->processingTimeout) {
            throw new ProcessingTimeoutException('Timeout exceeded');
        }
    }
}