<?php

namespace yandex\components\yandex;

use yii\base\UserException;

class YandexServiceException extends UserException
{
    
    public function __construct($message, $previous = null)
    {
        parent::__construct($message, 255, $previous);
    }
    
    public function getName()
    {
        return 'Yandex Service Exception';
    }
}