<?php

namespace yandex\components\yandex;

use v0\models\Customer;
use yandex\components\queue\CancelPaymentService;
use yandex\components\queue\MQConnection;
use yandex\exceptions\YandexException;
use yandex\exceptions\YandexPaymentException;
use yii\base\Model;
use yii\base\Object;
use yandex\exceptions\NotFoundProfileException;
use yandex\models\YandexPayment;
use yandex\models\Profile;
use yandex\components\formatter\FormFormatter;
use yandex\components\httpClient\BasicHttpClient;

class YandexCashDesk extends Object
{
    /**
     * @var int
     */
    public $networkTimeout;

    /**
     * @var int
     */
    public $processingTimeout;

    /**
     * @var Profile
     */
    private $_profile;

    /**
     * YandexCashDesk constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param Customer $customer
     * @param string   $orderNumber
     *
     * @return array
     * @throws YandexException
     */
    public function statusOrder($customer, $orderNumber)
    {
        $service = $this->getMerchantWebService();

        try {
            $profile = $this->getProfile();
            $shopId = (bool)$profile->secureDeal && $customer->getClientType() !== Customer::CLIENT_TYPE_WORKER
                ? $profile->secureDealPaymentShopId : $profile->shopId;

            $response = $service->listOrders($shopId, $orderNumber);
            $this->checkResponse($response);

            $orders = $response->getOrders();
            $order = array_shift($orders);

            return $order === null ? null : $order->toArray();
        } catch (\Exception $e) {
            throw new YandexException("Status order error: orderNumber=\"{$orderNumber}\", error=\"{$e->getMessage()}\"",
                $e);
        }
    }

    /**
     * @param Customer $customer
     * @param string   $orderNumber
     * @param float    $sum
     *
     * @return string
     * @throws YandexException
     * @throws NotFoundProfileException
     * @throws YandexServiceException
     */
    public function registerOrder($customer, $orderNumber, $sum)
    {
        $profile = $this->getProfile();
        $paymentName = $profile->paymentName;

        $secureDeal = (bool)$profile->secureDeal && $customer->getClientType() !== Customer::CLIENT_TYPE_WORKER;
        $shopId = $secureDeal ? $profile->secureDealShopId : $profile->shopId;
        $scid = $secureDeal ? $profile->secureDealScid : $profile->scid;
        $shopArticleId = $secureDeal ? $profile->secureDealShopArticleId : $profile->shopArticleId;

        $data = [
            'shopId'         => $shopId,
            'scid'           => $scid,
            'sum'            => $sum,
            'customerNumber' => $customer->clientId,
            'orderNumber'    => $orderNumber,
            'paymentType'    => 'AC',
            'rebillingOn'    => 1,
        ];

        if (!empty($shopArticleId)) {
            $data['shopArticleId'] = $shopArticleId;
        }

        try {
            $yandexPayment = YandexPayment::register($paymentName, $customer->clientId, $orderNumber, $shopId, $sum);

            /* @var $httpClient BasicHttpClient */
            $httpClient = \Yii::createObject([
                'class'          => '\yandex\components\httpClient\BasicHttpClient',
                'allowRedirects' => false, // for get url on header Location
                'formatter'      => new FormFormatter(),
                'timeout'        => $this->networkTimeout,
            ]);
            $response = $httpClient->post($profile->urlPaymentForm, $data);
            $urls = (array)$response->getHeader('Location');

            $yandexPayment->setResult(YandexPayment::RESULT_SUCCESS);

            return array_shift($urls);
        } catch (\Exception $e) {
            throw new YandexException("Register order error: customerNumber=\"{$customer->clientId}\", orderNumber=\"{$orderNumber}\", sum=\"{$sum}\", error=\"{$e->getMessage()}\"",
                $e);
        }
    }

    /**
     * @param Customer $customer
     * @param string   $orderNumber
     * @param int      $invoiceId
     * @param float    $sum
     * @param string   $destination
     * @param array    $params
     *
     * @return mixed
     * @throws YandexPaymentException
     * @throws YandexException
     */
    public function repeatCardPayment($customer, $orderNumber, $invoiceId, $sum, $destination = null, $params = [])
    {
        $paymentName = $this->_profile['paymentName'];

        $secureDeal = (bool)$this->_profile->secureDeal && $customer->getClientType() !== Customer::CLIENT_TYPE_WORKER;
        $shopId = $secureDeal ? $this->_profile->secureDealPaymentShopId : $this->_profile->shopId;
        $shopArticleId = $secureDeal ? $this->_profile->secureDealPaymentShopArticleId : $this->_profile->shopArticleId;
        $shopArticleId = $shopArticleId ?: null;
        $destination = empty($destination) ? null : $destination;
        $receipt = (int)$this->_profile->useReceipt === 1 ? $this->getReceiptData($sum, $params) : null;

        try {
            $processing = new RepeatPaymentProcessing($this->getMerchantWebService(), $this->processingTimeout);
            $processing->execute($paymentName, $customer->clientId, $orderNumber, $invoiceId, $sum, $shopId,
                $shopArticleId, $destination, $receipt);

            return $orderNumber;
        } catch (\Exception $e) {
            $this->cancelPayment($paymentName, $orderNumber);
            throw new YandexException("Repeat payment error: paymentName=\"{$paymentName}\", orderNumber=\"{$orderNumber}\", sum=\"{$sum}\", error=\"{$e->getMessage()}\"",
                $e);
        }
    }

    /**
     * @param float $sum
     * @param array $params
     *
     * @return string
     */
    private function getReceiptData($sum, $params)
    {
        $phone = isset($params['phone']) ? "+{$params['phone']}" : null;
        $email = isset($params['email']) ? $params['email'] : null;

        return json_encode([
            'customerContact' => empty($email) ? $phone : $email,
            'items'           => [
                [
                    'quantity' => 1,
                    'price'    => [
                        'amount' => $sum,
                    ],
                    'tax'      => $this->_profile->vat,
                    'text'     => $this->_profile->product,
                ],
            ],
        ]);
    }

    /**
     * @param Customer $customer
     * @param int      $invoiceId
     * @param float    $amount
     * @param string   $currency
     *
     * @return bool
     * @throws YandexException
     * @throws NotFoundProfileException
     */
    public function returnPayment($customer, $invoiceId, $amount, $currency)
    {
        $service = $this->getMerchantWebService();
        $profile = $this->getProfile();
        $shopId = (bool)$profile->secureDeal && $customer->getClientType() !== Customer::CLIENT_TYPE_WORKER
            ? $profile->secureDealPaymentShopId : $profile->shopId;

        try {
            $response = $service->returnPayment($service->generateClientOrderId(), $shopId, $invoiceId,
                $amount, $currency);
            $this->checkResponse($response);

            return (int)$response->error === 0;
        } catch (\Exception $e) {
            throw new YandexException("Return payment error: invoiceId=\"{$invoiceId}\", amount=\"{$amount}\", currency=\"{$currency}\", error=\"{$e->getMessage()}\"",
                $e);
        }
    }

    /**
     * @param string $paymentName
     *
     * @throws NotFoundProfileException
     */
    public function setProfile($paymentName)
    {
        $this->_profile = Profile::findOne(['paymentName' => $paymentName]);
    }

    /**
     * @return Profile
     * @throws NotFoundProfileException
     */
    public function getProfile()
    {
        if ($this->_profile) {
            return $this->_profile;
        } else {
            throw new NotFoundProfileException('Not found profile for yandex');
        }
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     */
    private function cancelPayment($paymentName, $orderNumber)
    {
        try {
            $service = $this->getCancelPaymentService();
            $service->publish($paymentName, $orderNumber);
        } catch (\Exception $e) {
            \Yii::error("Error publish task to cancel payment: error=\"{$e->getMessage()}\"", 'yandex-payment');
        }
    }

    /**
     * @return CancelPaymentService
     */
    private function getCancelPaymentService()
    {
        $connection = new MQConnection(
            \Yii::$app->params['rabbitMQ.host'],
            \Yii::$app->params['rabbitMQ.port'],
            \Yii::$app->params['rabbitMQ.user'],
            \Yii::$app->params['rabbitMQ.password'],
            \Yii::$app->params['rabbitMQ.virtualHost']);

        return new CancelPaymentService($connection, \Yii::$app, \Yii::$app->syslog);
    }

    /**
     * @return MerchantWebService
     */
    private function getMerchantWebService()
    {
        $profile = $this->getProfile();

        return new MerchantWebService($profile->urlMWS, $profile->certificate, $profile->key,
            $profile->certificatePassword);
    }

    private function checkResponse(Model $response)
    {
        if (!$response->validate()) {
            $errors = $response->getFirstErrors();
            throw new YandexException(array_shift($errors));
        }
    }
}