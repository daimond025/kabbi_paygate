<?php

namespace yandex\controllers;

use app\components\syslog\Syslog;
use yandex\components\queue\CancelPaymentService;
use yandex\components\queue\MQConnection;
use yandex\components\yandex\MerchantWebService;
use yandex\models\client\response\Order;
use yandex\models\Profile;
use yandex\models\YandexPayment;
use yandex\Module as Yandex;
use yandex\models\CheckOrder;
use yii\base\Model;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use yandex\models\OrderBindings;
use yandex\models\CheckOrderException;

class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    'application/xml' => Response::FORMAT_RAW,
                ],
            ],
        ];
    }

    /**
     * Yandex check order
     *
     * @param bool $monitor - is request from monitoring system
     *
     * @return mixed
     */
    public function actionCheck($monitor = false)
    {
        $checkOrder = new CheckOrder();
        $checkOrder->setAttributes(\Yii::$app->request->post(), false);

        $message = $checkOrder->getResponse();
        try {
            $checkOrder->check();
        } catch (CheckOrderException $e) {
            $message->code = $e->getCode();
            $message->message = $e->getMessage();

            if (!$monitor) {
                $this->logError("Yandex check order error: error=\"{$e->getMessage()} ({$e->getCode()})\"");
            }
        }

        \Yii::$app->response->getHeaders()->set('Content-Type', 'application/xml; charset=UTF-8');

        $response = $message->asXml();
        if (Yandex::getInstance()->enableDebug) {
            $this->logYandexCallback(\Yii::$app->requestedAction->id, \Yii::$app->request->post(), $response);
        }

        return $response;
    }

    public function actionAviso()
    {
        $checkOrder = new CheckOrder();
        $checkOrder->setAttributes(\Yii::$app->request->post(), false);

        $message = $checkOrder->getResponse();
        try {
            $checkOrder->check();

            try {
                $yandexPayment = YandexPayment::get($checkOrder->orderNumber, $checkOrder->shopId);
            } catch (\Exception $e) {
                throw new CheckOrderException($e->getMessage(), CheckOrderException::ERROR_CODE_PARSE_ERROR);
            }

            try {
                $yandexPayment->setAvisoData($checkOrder->invoiceId, $checkOrder->orderSumAmount,
                    $checkOrder->orderSumCurrencyPaycash);
            } catch (\Exception $e) {
                throw new CheckOrderException("Save payment aviso error: error=\"{$e->getMessage()}\"",
                    CheckOrderException::ERROR_CODE_PARSE_ERROR);
            }

            $gootaxResult = $yandexPayment->getResult();
            if ($gootaxResult === YandexPayment::RESULT_FAIL) {
                throw new CheckOrderException('Payment was rejected by Gootax',
                    CheckOrderException::ERROR_CODE_PARSE_ERROR);
            }

            if ($checkOrder->isAddNewCard()) {
                OrderBindings::createNewCard(
                    $checkOrder->shopId,
                    $checkOrder->customerNumber,
                    $checkOrder->invoiceId,
                    $checkOrder->cdd_pan_mask
                );

                $this->cancelPayment($yandexPayment->getPaymentName(), $checkOrder->orderNumber);
            }
        } catch (CheckOrderException $e) {
            $message->code = $e->getCode();
            $message->message = $e->getMessage();

            $this->logError("Yandex payment aviso error: shopId=\"{$checkOrder->shopId}\", orderNumber=\"{$checkOrder->orderNumber}\", error=\"{$e->getMessage()} ({$e->getCode()})\"");
        }

        \Yii::$app->response->getHeaders()->set('Content-Type', 'application/xml; charset=UTF-8');

        $response = $message->asXml('paymentAvisoResponse');
        if (Yandex::getInstance()->enableDebug) {
            $this->logYandexCallback(\Yii::$app->requestedAction->id, \Yii::$app->request->post(), $response);
        }

        return $response;
    }

    /**
     * @param string $method
     * @param array  $requestParams
     * @param string $response
     */
    private function logYandexCallback($method, $requestParams, $response)
    {
        $params = json_encode($requestParams, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $this->sendToSyslog("Yandex callback \"{$method}\": request=\"$params}\", response=\"{$response}\"");
    }

    /**
     * @param string $paymentName
     * @param int    $shopId
     * @param int    $fromTime
     * @param int    $toTime
     *
     * @return array
     */
    public function actionGetFailed($paymentName, $shopId, $fromTime, $toTime)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $profile = Profile::get($paymentName);
        $service = new MerchantWebService($profile->urlMWS, $profile->certificate, $profile->key,
            $profile->certificatePassword);

        $response = $service->getNotPaidOrders($shopId, $fromTime, $toTime);
        $this->checkResponse($response);

        $notPaidOrders = $response->getOrders();
        $successOrderNumbers = YandexPayment::getSuccessOrderNumbers($paymentName, $shopId,
            strtotime('-1day', $fromTime), strtotime('+1day', $toTime));

        return $this->getPreparedOrderData($this->getFailedOrders($notPaidOrders, $successOrderNumbers));
    }

    /**
     * @param Order[] $orders
     *
     * @return array
     */
    private function getPreparedOrderData($orders)
    {
        return array_map(function ($order) {
            /* @var $order Order */
            return [
                'shopId'      => $order->shopId,
                'orderNumber' => $order->orderNumber,
                'invoiceId'   => $order->invoiceId,
                'sum'         => $order->orderSumAmount,
                'currency'    => $order->orderSumCurrencyPaycash,
                'paid'        => $order->paid === 'true',
                'clearing'    => $order->clearing,
                'date'        => $order->createdDatetime,
            ];
        }, $orders);
    }

    /**
     * @param Order[] $notPaidOrders
     * @param array   $successOrderNumbers
     *
     * @return array
     */
    private function getFailedOrders($notPaidOrders, $successOrderNumbers = [])
    {
        return array_values(array_filter($notPaidOrders, function ($order) use ($successOrderNumbers) {
            /* @var $order Order */
            return in_array($order->orderNumber, $successOrderNumbers, false);
        }));
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     */
    private function cancelPayment($paymentName, $orderNumber)
    {
        try {
            $service = $this->getCancelPaymentService();
            $service->publish($paymentName, $orderNumber);
        } catch (\Exception $e) {
            \Yii::error("Error publish task to cancel payment: error=\"{$e->getMessage()}\"", 'yandex-payment');
        }
    }

    /**
     * @return CancelPaymentService
     */
    private function getCancelPaymentService()
    {
        $connection = new MQConnection(
            \Yii::$app->params['rabbitMQ.host'],
            \Yii::$app->params['rabbitMQ.port'],
            \Yii::$app->params['rabbitMQ.user'],
            \Yii::$app->params['rabbitMQ.password'],
            \Yii::$app->params['rabbitMQ.virtualHost']);

        return new CancelPaymentService($connection, \Yii::$app, \Yii::$app->syslog);
    }

    /**
     * @param string $message
     */
    private function sendToSyslog($message)
    {
        /* @var $syslog Syslog */
        $syslog = Yandex::getInstance()->syslog;
        if ($syslog !== null) {
            $syslog->sendLog($message);
        }
    }

    /**
     * @param string $message
     */
    private function logError($message)
    {
        $this->sendToSyslog($message);
        \Yii::error($message);
    }

    /**
     * @param Model $response
     */
    private function checkResponse(Model $response)
    {
        if (!$response->validate()) {
            $errors = $response->getFirstErrors();
            throw new YandexException(array_shift($errors));
        }
    }
}