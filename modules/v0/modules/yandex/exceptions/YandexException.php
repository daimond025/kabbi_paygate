<?php

namespace yandex\exceptions;

/**
 * Class YandexException
 * @package yandex\exceptions
 */
class YandexException extends \yii\web\HttpException
{
    const EXCEPTION_NAME = 'Yandex exception';

    public function __construct($message = null, \Exception $previous = null)
    {
        if ($message === null) {
            $message = self::EXCEPTION_NAME;
        }
        parent::__construct(500, $message, 4, $previous);
    }

    public function getName()
    {
        return self::EXCEPTION_NAME;
    }

}