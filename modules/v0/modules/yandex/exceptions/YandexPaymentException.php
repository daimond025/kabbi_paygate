<?php

namespace yandex\exceptions;

/**
 * Class YandexPaymentException
 * @package yandex\exceptions
 */
class YandexPaymentException extends \yii\web\HttpException
{
    const EXCEPTION_NAME = 'Yandex payment exception';

    public function __construct($message = null, \Exception $previous = null)
    {
        if ($message === null) {
            $message = self::EXCEPTION_NAME;
        }
        parent::__construct(500, $message, 6, $previous);
    }

    public function getName()
    {
        return self::EXCEPTION_NAME;
    }

}