<?php

namespace yandex\exceptions;

use yii\web\HttpException;

/**
 * Class ProcessingTimeoutException
 * @package yandex\exceptions
 */
class ProcessingTimeoutException extends HttpException
{
    const EXCEPTION_NAME = 'Processing timeout exception';

    public function __construct($message = null, \Exception $previous = null)
    {
        if ($message === null) {
            $message = self::EXCEPTION_NAME;
        }
        parent::__construct(500, $message, 5, $previous);
    }

    public function getName()
    {
        return self::EXCEPTION_NAME;
    }

}