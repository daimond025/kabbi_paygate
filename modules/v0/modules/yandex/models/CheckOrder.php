<?php

namespace yandex\models;

class CheckOrder extends CheckOrderDAO
{

    public function rules()
    {
        return [];
    }

    public function check()
    {
        $profile = $this->getProfile();
        if (empty($profile)) {
            throw new CheckOrderException('Don\'t find this profile', 100);
        }

        if (!$this->validateSignatureMd5($profile->password)) {
            throw new CheckOrderException('Invalid md5', 1);
        }
    }

    /**
     * @return Profile
     */
    private function getProfile()
    {
        return Profile::find()
            ->where([
                'or',
                ['shopId' => $this->shopId],
                ['secureDealShopId' => $this->shopId],
            ])
            ->one();
    }

    private function validateSignatureMd5($password)
    {
        // method toArray no save sequence params
        $checkData = implode(';', [
            $this->action,
            $this->orderSumAmount,
            $this->orderSumCurrencyPaycash,
            $this->orderSumBankPaycash,
            $this->shopId,
            $this->invoiceId,
            $this->customerNumber,
            $password,
        ]);

        return strtolower($this->md5) == strtolower(md5($checkData));
    }

    public function isAddNewCard()
    {
        return strstr($this->orderNumber, 'replicate_') &&
            strtolower($this->rebillingOn) == 'true';
    }

    public function getResponse()
    {
        $response                 = new CheckOrderResponse();
        $response->shopId         = $this->shopId;
        $response->invoiceId      = $this->invoiceId;
        $response->orderSumAmount = $this->orderSumAmount;

        return $response;
    }

}