<?php

namespace yandex\models;

use yii\base\Model;

class CheckOrderDAO extends Model
{
    
    public $requestDatetime;
    
    public $action;
    
    public $md5;
    
    public $shopId;
    
    public $shopArticleId;
    
    public $invoiceId;
    
    public $orderNumber;
    
    public $customerNumber;
    
    public $orderCreatedDatetime;
    
    public $orderSumAmount;
    
    public $orderSumCurrencyPaycash;
    
    public $orderSumBankPaycash;
    
    public $shopSumAmount;
    
    public $shopSumCurrencyPaycash;
    
    public $shopSumBankPaycash;
    
    public $paymentPayerCode;
    
    public $paymentType;
    
    public $rebillingOn;
    
    public $cdd_pan_mask;
    
    public function rules()
    {
        return [];
    }
    
}