<?php

namespace yandex\models\client\response;

use yii\base\Model;
use yandex\components\parser\XmlAttributeParser;

class RepeatCardPayment extends Model implements LoadXmlInterface
{
    const STATUS_SUCCESS = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_FAIL = 3;

    public $clientOrderId;

    public $status;

    public $error;

    public $processedDT;

    public $techMessage;

    public $invoiceId;

    public function rules()
    {
        return [];
    }

    public function loadXml($data)
    {
        $parser = new XmlAttributeParser();
        $data   = $parser->parse($data);

        $this->setAttributes($data, false);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        if ($this->error != 0) {
            $this->addError('techMessage', $this->techMessage);
        }

        return !$this->hasErrors();
    }

}