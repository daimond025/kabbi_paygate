<?php

namespace yandex\models\client\response;

use yii\base\Model;
use yandex\components\parser\XmlAttributeParser;

class ListOrders extends Model implements LoadXmlInterface
{
    
    public $status;
    
    public $error;
    
    public $processedDT;
    
    public $orderCount;
    
    public $techMessage;
    
    private $orders = [];
    
    public function rules()
    {
        return [];
    }
    
    public function getOrders()
    {
        return $this->orders;
    }
    
    /**
     * 
     * @param string $data
     */
    public function loadXml($data)
    {
        $parser = new XmlAttributeParser();
        
        $sxml = $parser->getSxmlElement($data);
        
        $listOrderData = $parser->attributeAsArray($sxml);
        $this->setAttributes($listOrderData, false);
        
        $orders = (array) $sxml->xpath('order');
        foreach ($orders as $oneOrder) {
            $dataOrder = $parser->attributeAsArray($oneOrder);
            $this->addOrder($dataOrder);
        }
    }
    
    private function addOrder($data)
    {
        $order = new Order();
        $order->setAttributes($data, false);
        
        $this->orders[] = $order;
    }
    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        
        if ($this->error != 0) {
            $this->addError('techMessage', $this->techMessage);
        }
        
        return !$this->hasErrors();
    }
    
}
