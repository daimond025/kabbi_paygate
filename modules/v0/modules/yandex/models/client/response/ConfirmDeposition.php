<?php

namespace yandex\models\client\response;

use yii\base\Model;
use yandex\components\parser\XmlAttributeParser;

class ConfirmDeposition extends Model implements LoadXmlInterface
{
    const STATUS_SUCCESSFUL = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_FAILED = 3;

    public $status;

    public $requestDT;

    public $invoiceId;

    public $error;

    public $clientOrderId;


    public function rules()
    {
        return [];
    }

    public function loadXml($data)
    {
        $parser = new XmlAttributeParser();
        $data   = $parser->parse($data);

        $this->setAttributes($data, false);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        if ((int)$this->status === 3) {
            $this->addError('error', $this->error);
        }

        return !$this->hasErrors();
    }

}