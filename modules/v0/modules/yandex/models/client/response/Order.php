<?php

namespace yandex\models\client\response;

use yii\base\Model;

class Order extends Model
{
    const CLEARING_NONE = 'none';
    const CLEARING_SALE = 'sale';
    const CLEARING_CANCEL = 'cancel';

    public $shopId;

    public $articleId;

    public $invoiceId;

    public $shopName;

    public $articleName;

    public $orderNumber;

    public $paymentSystemOrderNumber;

    public $customerNumber;

    public $createdDatetime;

    public $paid;

    public $orderSumAmount;

    public $orderSumCurrencyPaycash;

    public $orderSumBankPaycash;

    public $paidSumAmount;

    public $paidSumCurrencyPaycash;

    public $paidSumBankPaycash;

    public $receivedSumAmount;

    public $receivedSumCurrencyPaycash;

    public $receivedSumBankPaycash;

    public $shopSumAmount;

    public $shopSumCurrencyPaycash;

    public $shopSumBankPaycash;

    public $paymentDatetime;

    public $paymentAuthorizationTime;

    public $payerCode;

    public $payerAddress;

    public $payeeCode;

    public $paymentSystemDatetime;

    public $avisoReceivedDatetime;

    public $avisoStatus;

    public $paymentType;

    public $agentId;

    public $clearing;

    public $uniLabel;

    public function rules()
    {
        return [];
    }


}
