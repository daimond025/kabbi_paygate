<?php

namespace yandex\models\client\response;

interface LoadXmlInterface
{
    
    public function loadXml($data);
    
}