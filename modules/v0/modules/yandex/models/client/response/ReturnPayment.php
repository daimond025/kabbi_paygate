<?php

namespace yandex\models\client\response;

use yii\base\Model;
use yandex\components\parser\XmlAttributeParser;

class ReturnPayment extends Model implements LoadXmlInterface
{
    
    public $clientOrderId;
    
    public $status;
    
    public $error;
    
    public $processedDT;
    
    public $techMessage;
    
    public function rules()
    {
        return [];
    }
    
    public function loadXml($data)
    {
        $parser = new XmlAttributeParser();
        $data = $parser->parse($data);
        
        $this->setAttributes($data, false);
    }
    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        
        if ($this->error != 0) {
            $this->addError('techMessage', $this->techMessage);
        }
        
        return !$this->hasErrors();
    }
    
}