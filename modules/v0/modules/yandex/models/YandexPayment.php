<?php

namespace yandex\models;

use yandex\exceptions\YandexPaymentException;
use yii\db\Exception;

/**
 * Class YandexPayment
 * @package yandex\models
 */
class YandexPayment
{
    const LOG_CATEGORY = 'Yandex payment';

    const RESULT_WAITING = 'WAITING';
    const RESULT_SUCCESS = 'SUCCESS';
    const RESULT_FAIL = 'FAIL';

    /**
     * @var YandexPaymentDAO
     */
    private $paymentDAO;

    /**
     * YandexPayment constructor.
     *
     * @param YandexPaymentDAO $paymentDAO
     */
    public function __construct(YandexPaymentDAO $paymentDAO)
    {
        $this->paymentDAO = $paymentDAO;
    }

    /**
     * @param string $paymentName
     * @param string $customerNumber
     * @param string $orderNumber
     * @param int    $shopId
     * @param float  $sum
     * @param int    $baseInvoiceId
     * @param string $destination
     *
     * @return YandexPayment
     * @throws YandexPaymentException
     */
    public static function register(
        $paymentName,
        $customerNumber,
        $orderNumber,
        $shopId,
        $sum,
        $baseInvoiceId = null,
        $destination = null
    ) {
        $params = [
            'paymentName'    => $paymentName,
            'customerNumber' => $customerNumber,
            'orderNumber'    => $orderNumber,
            'shopId'         => $shopId,
            'sum'            => $sum,
            'baseInvoiceId'  => $baseInvoiceId,
            'destination'    => $destination
        ];

        $payment = new self(new YandexPaymentDAO());
        $payment->updateModel($params, 'Register payment error');

        return $payment;
    }

    /**
     * @param string $orderNumber
     * @param int    $shopId
     *
     * @return YandexPayment
     * @throws YandexPaymentException
     */
    public static function get($orderNumber, $shopId)
    {
        $params = [
            'orderNumber' => $orderNumber,
            'shopId'      => $shopId,
        ];

        try {
            $paymentDAO = YandexPaymentDAO::find()
                ->where($params)
                ->one();

            if (empty($paymentDAO)) {
                throw new Exception('Payment not found');
            }

            return new self($paymentDAO);
        } catch (\Exception $e) {
            $paramStr = json_encode($params);
            throw new YandexPaymentException("Getting payment error: params={$paramStr}, error={$e->getMessage()}");
        }
    }

    /**
     * @param int    $invoiceId
     * @param float  $sum
     * @param string $currency
     *
     * @throws YandexPaymentException
     */
    public function setAvisoData($invoiceId, $sum, $currency)
    {
        $params = [
            'avisoInvoiceId' => $invoiceId,
            'avisoSum'       => $sum,
            'avisoCurrency'  => $currency,
        ];

        $this->updateModel($params, 'Set aviso error');
    }

    /**
     * @return array
     */
    public function getAvisoData()
    {
        $this->paymentDAO->refresh();

        return [
            'avisoInvoiceId' => $this->paymentDAO->avisoInvoiceId,
            'avisoSum'       => $this->paymentDAO->avisoSum,
            'avisoCurrency'  => $this->paymentDAO->avisoCurrency,
        ];
    }

    /**
     * @param int    $id
     * @param string $status
     * @param string $error
     * @param string $techMessage
     * @param string $processedDT
     *
     * @throws YandexPaymentException
     */
    public function setRepeatPaymentResult($id, $status, $error, $techMessage, $processedDT)
    {
        $params = [
            'repeatId'          => $id,
            'repeatStatus'      => $status,
            'repeatError'       => $error,
            'repeatTechMessage' => $techMessage,
            'repeatProcessedDT' => $processedDT,
        ];

        $this->updateModel($params, 'Set repeat payment result error');
    }

    /**
     * @param int    $id
     * @param string $status
     * @param string $error
     * @param string $techMessage
     * @param string $processedDT
     *
     * @throws YandexPaymentException
     */
    public function setCancelPaymentResult($id, $status, $error, $techMessage, $processedDT)
    {
        $params = [
            'cancelId'          => $id,
            'cancelStatus'      => $status,
            'cancelError'       => $error,
            'cancelTechMessage' => $techMessage,
            'cancelProcessedDT' => $processedDT,
        ];

        $this->updateModel($params, 'Set cancel payment result error');
    }

    /**
     * @param int    $id
     * @param string $status
     * @param string $error
     * @param int    $invoiceId
     * @param string $requestDT
     *
     * @throws YandexPaymentException
     */
    public function setConfirmDepositionResult($id, $status, $error, $invoiceId, $requestDT)
    {
        $params = [
            'confirmId'          => $id,
            'confirmStatus'      => $status,
            'confirmError'       => $error,
            'confirmTechMessage' => $invoiceId,
            'confirmRequestDT'   => $requestDT,
        ];

        $this->updateModel($params, 'Set confirm deposition result error');
    }

    /**
     * @param int    $id
     * @param string $status
     * @param string $error
     * @param string $techMessage
     * @param string $processedDT
     *
     * @throws YandexPaymentException
     */
    public function setReturnPaymentResult($id, $status, $error, $techMessage, $processedDT)
    {
        $params = [
            'returnId'          => $id,
            'returnStatus'      => $status,
            'returnError'       => $error,
            'returnTechMessage' => $techMessage,
            'returnProcessedDT' => $processedDT,
        ];

        $this->updateModel($params, 'Set return payment result error');
    }

    /**
     * @param string $result
     *
     * @throws YandexPaymentException
     */
    public function setResult($result)
    {
        $params = [
            'gootaxResult' => $result,
        ];

        $this->updateModel($params, 'Set gootax result error');
    }

    /**
     * @return string
     */
    public function getResult()
    {
        $this->paymentDAO->refresh();

        return $this->paymentDAO->gootaxResult;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentDAO->paymentName;
    }

    /**
     * @param string $paymentName
     * @param int    $shopId
     * @param int    $fromTime
     * @param int    $toTime
     *
     * @return array
     */
    public static function getSuccessOrderNumbers($paymentName, $shopId, $fromTime, $toTime)
    {
        $orderNumbers = YandexPaymentDAO::find()
            ->select('orderNumber')
            ->where([
                'and',
                ['paymentName' => $paymentName],
                ['shopId' => $shopId],
                ['gootaxResult' => self::RESULT_SUCCESS],
                ['>=', 'createdAt', $fromTime],
                ['<=', 'createdAt', $toTime],
            ])
            ->column();

        return empty($orderNumbers) ? [] : $orderNumbers;
    }

    /**
     * @param array  $params
     * @param string $errorTitle
     *
     * @throws YandexPaymentException
     */
    private function updateModel($params, $errorTitle = 'Error')
    {
        try {
            $this->paymentDAO->refresh();
            $this->paymentDAO->setAttributes($params);

            if (!$this->paymentDAO->save()) {
                throw new Exception(implode('; ', $this->paymentDAO->getFirstErrors()));
            }
        } catch (\Exception $e) {
            $paramStr = json_encode($params);
            throw new YandexPaymentException("{$errorTitle}: params={$paramStr}, error={$e->getMessage()}");
        }
    }

}