<?php

namespace yandex\models;

use yii\base\Model;
use yandex\components\formatter\XmlFormatter;

class CheckOrderResponse extends Model
{
    
    public $performedDatetime;
    
    public $code = 0;
    
    public $shopId;
    
    public $invoiceId;
    
    public $message;
    
    public $techMessage;
    
    private $orderSumAmount;
    
    public function init()
    {
        $this->performedDatetime = date('Y-m-d\TH:i:sP');
    }
    
    public function rules()
    {
        return [];
    }
    
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'orderSumAmount';
        return $fields;
    }
    
    public function asXml($root = null)
    {
        $root = $root ? $root : 'checkOrderResponse';
        $formatter = new XmlFormatter($root);
        return $formatter->format($this->toArray());
    }
    
    public function setOrderSumAmount($value)
    {
        $this->orderSumAmount = (float) $value;
    }
    
    public function getOrderSumAmount()
    {
        return sprintf("%.2f", $this->orderSumAmount);
    }
}