<?php

namespace yandex\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "yandex_payment".
 *
 * @property integer $id
 * @property string  $paymentName
 * @property string  $customerNumber
 * @property string  $orderNumber
 * @property integer $baseInvoiceId
 * @property string  $destination
 * @property string  $sum
 * @property integer $shopId
 * @property integer $avisoInvoiceId
 * @property string  $avisoSum
 * @property string  $avisoCurrency
 * @property integer $repeatId
 * @property string  $repeatStatus
 * @property string  $repeatError
 * @property string  $repeatTechMessage
 * @property string  $repeatProcessedDT
 * @property integer $cancelId
 * @property string  $cancelStatus
 * @property string  $cancelError
 * @property string  $cancelTechMessage
 * @property string  $cancelProcessedDT
 * @property integer $confirmId
 * @property string  $confirmStatus
 * @property string  $confirmError
 * @property string  $confirmInvoiceId
 * @property string  $confirmRequestDT
 * @property integer $returnId
 * @property string  $returnStatus
 * @property string  $returnError
 * @property string  $returnTechMessage
 * @property string  $returnProcessedDT
 * @property string  $gootaxResult
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class YandexPaymentDAO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yandex_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'customerNumber', 'orderNumber', 'shopId'], 'required'],
            [
                [
                    'baseInvoiceId',
                    'shopId',
                    'avisoInvoiceId',
                    'repeatId',
                    'cancelId',
                    'confirmId',
                    'returnId',
                    'createdAt',
                    'updatedAt',
                ],
                'integer',
            ],
            [['sum', 'avisoSum'], 'number'],
            [['gootaxResult'], 'string'],
            [
                [
                    'paymentName',
                    'customerNumber',
                    'orderNumber',
                    'destination',
                    'avisoCurrency',
                    'repeatStatus',
                    'repeatError',
                    'repeatTechMessage',
                    'repeatProcessedDT',
                    'cancelStatus',
                    'cancelError',
                    'cancelTechMessage',
                    'cancelProcessedDT',
                    'confirmStatus',
                    'confirmError',
                    'confirmInvoiceId',
                    'confirmRequestDT',
                    'returnStatus',
                    'returnError',
                    'returnTechMessage',
                    'returnProcessedDT',
                ],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

}
