<?php

namespace yandex\models;

use yandex\components\creatorFile\CreatorFile;
use yandex\exceptions\NotFoundProfileException;

/**
 *
 * @property string       $urlPaymentForm
 * @property string       $urlMWS
 * @property \SplFileInfo $certificate
 * @property \SplFileInfo $key
 *
 */
class Profile extends ProfileDAO
{
    const URL_PAYMENT_FORM = 'https://money.yandex.ru/eshop.xml';
    const DEBUG_URL_PAYMENT_FORM = 'https://demomoney.yandex.ru/eshop.xml';

    const URL_CARD_BIND_FORM = 'https://money.yandex.ru/cardauth';
    const DEBUG_URL_CARD_BIND_FORM = 'https://demomoney.yandex.ru/cardauth';

    const URL_MWS = 'https://penelope.yamoney.ru/webservice/mws/api/';
    const DEBUG_URL_MWS = 'https://penelope-demo.yamoney.ru:8083/webservice/mws/api/';

    private $_creatorFile;

    private $_certificate;

    private $_key;

    public function rules()
    {
        return parent::rules();
    }

    public function canUseProfitCard()
    {
        return (bool)$this->secureDeal;
    }

    public function getUrlPaymentForm()
    {
        return $this->isDebug ? self::DEBUG_URL_PAYMENT_FORM : self::URL_PAYMENT_FORM;
    }

    public function getUrlCardBindForm()
    {
        return $this->isDebug ? self::DEBUG_URL_CARD_BIND_FORM : self::URL_CARD_BIND_FORM;
    }

    public function getUrlMWS()
    {
        return $this->isDebug ? self::DEBUG_URL_MWS : self::URL_MWS;
    }

    public function getCertificate()
    {
        if (is_null($this->_certificate)) {
            $this->_certificate = $this->getCreatorFile()
                ->createTmpFile($this->certificateContent);
        }

        return $this->_certificate;
    }

    protected function getCreatorFile()
    {
        if (is_null($this->_creatorFile)) {
            $this->_creatorFile = new CreatorFile();
        }

        return $this->_creatorFile;
    }

    public function getKey()
    {
        if (is_null($this->_key)) {
            $this->_key = $this->getCreatorFile()->createTmpFile($this->keyContent);
        }

        return $this->_key;
    }

    public static function get($paymentName)
    {
        $profile = self::findOne(['paymentName' => $paymentName]);
        if (!$profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }

        return $profile;
    }

}