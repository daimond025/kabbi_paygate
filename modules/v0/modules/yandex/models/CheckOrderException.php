<?php

namespace yandex\models;

use yii\base\UserException;

class CheckOrderException extends UserException
{
    const ERROR_CODE_INVALID_HASH = 1;
    const ERROR_CODE_PARSE_ERROR = 200;

    public function getName()
    {
        return 'Check Order Exception';
    }
}