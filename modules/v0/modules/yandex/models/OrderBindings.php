<?php

namespace yandex\models;

use app\components\syslog\Syslog;


/**
 *
 * This is the model class for table "yandex_order_binding".
 *
 * @property integer $id
 * @property integer $shopId
 * @property string  $clientId
 * @property integer $invoiceId
 * @property string  $pan
 *
 */
class OrderBindings extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yandex_order_binding';
    }

    public function rules()
    {
        return [
            [['shopId', 'clientId', 'invoiceId'], 'required'],
            [['shopId', 'invoiceId'], 'integer', 'message' => Syslog::YII_MESSAGE_TO_INTEGER],
            [['clientId', 'pan'], 'string', 'max' => 32, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'shopId'    => 'shopId',
            'clientId'  => 'clientId',
            'invoiceId' => 'invoceId',
            'pan'       => 'pan',
        ];
    }

    public function getBindings($shopId, $clientId)
    {
        return self::find()->asArray()->andWhere(['shopId' => $shopId, 'clientId' => $clientId])->all();
    }

    public function deleteBinding($shopId, $invoiceId)
    {
        $binding = self::findOne(['shopId' => $shopId, 'invoiceId' => $invoiceId]);
        if ($binding) {
            $binding->delete();
        }
    }

    public static function createNewCard($shopId, $customerNumber, $invoiceId, $pan)
    {
        OrderBindings::deleteAll(['shopId' => $shopId, 'clientId' => $customerNumber, 'pan' => $pan]);

        $binding             = new OrderBindings();
        $binding->attributes = [
            'shopId'    => $shopId,
            'clientId'  => $customerNumber,
            'invoiceId' => $invoiceId,
            'pan'       => $pan,
        ];

        return $binding->save();
    }
}