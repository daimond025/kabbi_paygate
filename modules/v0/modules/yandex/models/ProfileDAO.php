<?php

namespace yandex\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;
use yii\behaviors\TimestampBehavior;

/**
 *
 * This is the model class for table "yandex_payment_gate_profile".
 *
 * @property integer         $id
 * @property string          $paymentName
 * @property integer         $typeId
 * @property integer         $shopId
 * @property integer         $scid
 * @property integer         $shopArticleId
 * @property string          $password
 * @property float           $commission
 * @property boolean         $isDebug
 * @property string          $certificateContent
 * @property string          $certificatePassword
 * @property string          $keyContent
 * @property integer         $secureDeal
 * @property integer         $secureDealShopId
 * @property integer         $secureDealScid
 * @property integer         $secureDealShopArticleId
 * @property integer         $secureDealPaymentShopId
 * @property integer         $secureDealPaymentScid
 * @property integer         $secureDealPaymentShopArticleId
 * @property integer         $cardBindingSum
 * @property integer         $useReceipt
 * @property string          $product
 * @property integer         $vat
 *
 * @property PaymentGateType $type
 */
class ProfileDAO extends \yii\db\ActiveRecord
{
    const TYPE_ID = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yandex_payment_gate_profile';
    }

    public function rules()
    {
        return [
            [['paymentName', 'typeId', 'shopId', 'scid', 'certificateContent', 'keyContent'], 'required'],
            [
                ['typeId', 'shopId', 'scid', 'shopArticleId', 'isDebug', 'cardBindingSum', 'createdAt', 'updatedAt'],
                'integer',
                'message' => Syslog::YII_MESSAGE_TO_INTEGER,
            ],
            [['paymentName'], 'string', 'max' => 32, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['paymentName'], 'unique', 'message' => Syslog::YII_MESSAGE_TO_UNIQUE],
            [['password', 'certificatePassword'], 'string', 'max' => 255],
            [['commission'], 'default', 'value' => 0],
            [['isDebug'], 'default', 'value' => 1],
            [['cardBindingSum'], 'default', 'value' => 1],
            [['certificatePassword'], 'default', 'value' => ''],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
            [['certificateContent', 'keyContent'], 'safe'],
            [
                [
                    'secureDeal',
                    'secureDealShopId',
                    'secureDealScid',
                    'secureDealShopArticleId',
                    'secureDealPaymentShopId',
                    'secureDealPaymentScid',
                    'secureDealPaymentShopArticleId',
                ],
                'integer',
                'message' => Syslog::YII_MESSAGE_TO_INTEGER,
            ],
            ['secureDeal', 'default', 'value' => 0],
            ['secureDealShopId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealScid', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentShopId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentScid', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            [
                [
                    'secureDealShopId',
                    'secureDealScid',
                    'secureDealShopArticleId',
                    'secureDealPaymentShopId',
                    'secureDealPaymentScid',
                    'secureDealPaymentShopArticleId',
                ],
                'filter',
                'filter' => [$this, 'filterSecureDealFields'],
            ],
            ['useReceipt', 'default', 'value' => 0],
            ['product', 'required', 'when' => [$this, 'requireReceipt']],
            ['vat', 'required', 'when' => [$this, 'requireReceipt']],
            [['vat'], 'integer', 'min' => 1, 'max' => 6, 'message' => Syslog::YII_MESSAGE_TO_INTEGER],
            [['product', 'vat'], 'filter', 'filter' => [$this, 'filterReceiptFields']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                             => 'ID',
            'paymentName'                    => 'paymentName',
            'typeId'                         => 'typeId',
            'shopId'                         => 'shopId',
            'scid'                           => 'scid',
            'password'                       => 'password',
            'commission'                     => 'commission',
            'isDebug'                        => 'isDebug',
            'certificateContent'             => 'certificateContent',
            'keyContent'                     => 'keyContent',
            'secureDeal'                     => 'Secure deal',
            'secureDealShopId'               => 'Secure deal shopId',
            'secureDealScid'                 => 'Secure deal scid',
            'secureDealShopArticleId'        => 'Secure deal shopArticleId',
            'secureDealPaymentScid'          => 'Secure deal scid for payment',
            'secureDealPaymentShopId'        => 'Secure deal shopId for payment',
            'secureDealPaymentShopArticleId' => 'Secure deal shopArticleId for payment',
            'cardBindingSum'                 => 'Card binding sum',
            'useReceipt'                     => 'Use receipt',
            'product'                        => 'Product',
            'vat'                            => 'Vat',
        ];
    }

    /**
     * @inherited
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    public function filterSecureDealFields($value)
    {
        return !empty($this->secureDeal) ? $value : null;
    }

    public function requireSecureDeal($model)
    {
        return !empty($model->secureDeal);
    }

    public function filterReceiptFields($value)
    {
        return !empty($this->useReceipt) ? $value : null;
    }

    public function requireReceipt($model)
    {
        return !empty($model->useReceipt);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    // Stub for correctly work method load
    public function setTypeId($value)
    {
    }

    public function fields()
    {
        return [
            'paymentName',
            'typeId',
            'shopId',
            'scid',
            'shopArticleId',
            'commission',
            'isDebug',
            'secureDeal',
            'secureDealShopId',
            'secureDealScid',
            'secureDealShopArticleId',
            'secureDealPaymentScid',
            'secureDealPaymentShopId',
            'secureDealPaymentShopArticleId',
            'cardBindingSum',
            'useReceipt',
            'product',
            'vat',
        ];
    }
}