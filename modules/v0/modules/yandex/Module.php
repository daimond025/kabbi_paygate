<?php

namespace yandex;

class Module extends \yii\base\Module
{
    public $networkTimeout;
    public $processingTimeout;

    public $paymentRetryCount;
    public $paymentRetryTimeout;
    public $waitAttemptCount;
    public $waitAttemptTimeout;
    public $externalUrl;
    public $syslog;
    public $enableDebug;

    public function init()
    {
        parent::init();

        $this->components = [
            'payment'            => [
                'class'             => 'yandex\components\yandex\YandexCashDesk',
                'networkTimeout'    => $this->networkTimeout,
                'processingTimeout' => $this->processingTimeout,
            ],
            'paymentGateProfile' => 'yandex\models\Profile',
            'bindingManager'     => 'yandex\models\OrderBindings',
        ];

        $this->layout = 'yandex';

        \Yii::$app->layout = false;
    }

}