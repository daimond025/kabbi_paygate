<?php
namespace sberbank\models;

use sberbank\components\payment\SberbankApi;
use sberbank\exception\NotFoundProfileException;
/**
* @property string $paymentName
* @property string $username
* @property string $password
* @property float $commission
* @property string $url
* @property string $returnUrl
* @property string $failUrl
*/
class Profile extends \yii\base\Model
{
    const MODE_BINDING = 'binding';
    
    const MODE_PAYMENT = 'payment';
    
    private $mode = self::MODE_BINDING;
    
    /**
     * @var \sberbank\models\ProfileDAO
     */
    private $profile; 
    
    
    public function __construct(ProfileDAO $profile = null, $config = [])
    {
        $this->profile = $profile;
        parent::__construct($config);
    }
    
    public function setMode($mode)
    {
        if ($mode == self::MODE_BINDING || $mode == self::MODE_PAYMENT) {
            $this->mode = $mode;
        }
    }
    
    public function getMode()
    {
        return $this->mode;
    }
    
    public function getProfile()
    {
        if ($this->profile) {
            return $this->profile;
        } else {
            throw new NotFoundProfileException('No set ProfileDAO in sberbank');
        }
    }
    
    public function __get($name)
    {
        if ($this->profile->hasAttribute($name)) {
            return $this->getProfile()->getAttribute($name); 
        } else {
            return parent::__get($name);
        }
    }

    public function getUsername()
    {
        if ($this->mode == self::MODE_BINDING) {
            return $this->getProfile()->usernameBinding;
        } else {
            return $this->getProfile()->usernamePayment;
        }
    }
    
    public function getPassword()
    {
        if ($this->mode == self::MODE_BINDING) {
            return $this->getProfile()->passwordBinding;
        } else {
            return $this->getProfile()->passwordPayment;
        }
    }
    
    public function getApi()
    {
        return new SberbankApi($this);
    } 
}