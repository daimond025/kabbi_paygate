<?php

namespace sberbank\models;

use v0\Module as V0;
use v0\models\PaymentGateType;
use app\components\syslog\Syslog;

/**
 * This is the model class for table "payment_gate_profile".
 *
 * @property integer $id
 * @property string  $paymentName
 * @property integer $typeId
 * @property string  $usernameBinding
 * @property string  $passwordBinding
 * @property string  $usernamePayment
 * @property string  $passwordPayment
 * @property float   $commission
 * @property string  $url
 * @property string  $returnUrl
 * @property string  $failUrl
 */
class ProfileDAO extends \yii\db\ActiveRecord
{

    const TYPE_ID = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sberbank_payment_gate_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'url'], 'required'],
            [['paymentName'], 'string', 'max' => 32, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['usernameBinding', 'usernamePayment'], 'string', 'max' => 32],
            [['passwordBinding', 'passwordPayment'], 'string', 'max' => 256],
            [['url', 'returnUrl', 'failUrl'], 'string', 'max' => 255, 'tooLong' => Syslog::YII_MESSAGE_STRING_TOO_LONG],
            [['paymentName'], 'unique', 'message' => Syslog::YII_MESSAGE_TO_UNIQUE],
            [['commission'], 'default', 'value' => 0],
            [['returnUrl'], 'default', 'value' => 'payment_success_ru.html'],
            [['failUrl'], 'default', 'value' => 'errors_ru.html'],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'paymentName'     => 'paymentName',
            'typeId'          => 'typeId',
            'usernameBinding' => 'usernameBinding',
            'passwordBinding' => 'passwordBinding',
            'commission'      => 'commission',
            'usernamePayment' => 'usernamePayment',
            'passwordPayment' => 'passwordPayment',
            'url'             => 'url',
            'returnUrl'       => 'returnUrl',
            'failUrl'         => 'failUrl',
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    // Stub for correctly work method load
    public function setTypeId($value)
    {
    }

    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }


    public function fields()
    {
        return [
            'paymentName',
            'usernameBinding',
            'usernamePayment',
            'url',
            'typeId',
            'commission',
            'returnUrl',
            'failUrl',
        ];
    }

}
