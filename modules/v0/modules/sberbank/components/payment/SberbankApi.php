<?php
namespace sberbank\components\payment;

use app\components\syslog\Syslog;
use GuzzleHttp\Client;
use GuzzleHttp\json_encode;
use GuzzleHttp\json_decode;

class SberbankApi extends \yii\base\Object
{
    const TIMEOUT = 10.0;

    private $profile;

    public function __construct(\sberbank\models\Profile $profile, $config = [])
    {
        $this->profile = $profile;
        parent::__construct($config);
    }

    /**
     *
     * @param \sberbank\models\Profile $profile
     * @param string                   $orderNumber
     * @param integer                  $amount
     * @param integer                  $currency
     * @param string                   $description
     * @param mixed                    $jsonParam
     */
    public function register(
        $clientId,
        $orderNumber,
        $amount,
        $currency,
        $description,
        $bindingId = null
    ) {
        $data = [
            'orderNumber' => $orderNumber,
            'amount'      => $amount,
            'currency'    => $currency,
            'description' => $description,
            'returnUrl'   => $this->profile->returnUrl,
            'failUrl'     => $this->profile->failUrl,
            'clientId'    => $clientId,
        ];
        if ($bindingId) {
            $data['bindingId'] = $bindingId;
        }
        $result = $this->send('register.do', $data);

        return [
            'url'   => key_exists('formUrl', $result) ? $result['formUrl'] : null,
            'order' => key_exists('orderId', $result) ? $result['orderId'] : null,
        ];
    }

    public function refund($orderId, $amount)
    {
        $this->send('refund.do', [
            'orderId' => $orderId,
            'amount'  => $amount,
        ]);

        return true;
    }

    public function status($orderId)
    {
        $result = $this->send('getOrderStatusExtended.do', [
            'orderId' => $orderId,
        ]);
        if (key_exists('bindingInfo', $result)) {
            $bindingInfo = $result['bindingInfo'];
            $return      = [
                'client_id'  => key_exists('clientId', $bindingInfo) ? $bindingInfo['clientId'] : null,
                'binding_id' => key_exists('bindingId', $bindingInfo) ? $bindingInfo['bindingId'] : null,
            ];
        } else {
            $return = [];
        }

        return array_merge($return, [
            'pan'        => key_exists('pan', $result) ? $result['Pan'] : null,
            'amount'     => $result['amount'],
            'currency'   => $result['currency'],
            'actionCode' => key_exists('actionCode', $result) ? $result['actionCode'] : null,
        ]);
    }

    public function bindings($clientId)
    {
        $result = $this->send('getBindings.do', [
            'clientId' => $clientId,
        ]);
        if (key_exists('bindings', $result)) {
            $yeld = [];
            foreach ($result['bindings'] as $binding) {
                if (key_exists('bindingId', $binding) && key_exists('maskedPan', $binding)) {
                    $yeld[] = [
                        'binding' => $binding['bindingId'],
                        'pan'     => $binding['maskedPan'],
                    ];
                }
            }

            return $yeld;
        } else {
            throw new SberbankException('No key bindings in response');
        }
    }

    public function payment($order, $binding)
    {
        $this->send('paymentOrderBinding.do', [
            'mdOrder'   => $order,
            'bindingId' => $binding,
        ]);
        $actionCode = $this->status($order)['actionCode'];

        $processing = new PaymentProcessing();
        $processing->validate($actionCode);

        return true;
    }

    public function unbind($bindingId)
    {
        $result = $this->send('unBindCard.do', [
            'bindingId' => $bindingId,
        ]);

        return true;
    }

    private function send($method, $data)
    {
        $data   = array_merge($data, [
            'userName' => $this->profile->username,
            'password' => $this->profile->password,
        ]);
        $result = json_decode($this->sendRequest($method, $data), true);
        if (empty($result)) {
            throw new SberbankException();
        }
        $resultLow = array_change_key_case($result);
        if (key_exists('errorcode', $resultLow)) {
            $msg = key_exists('errormessage', $resultLow) ? $resultLow['errormessage'] : null;
            if ($resultLow['errorcode'] != 0) {
                throw new SberbankException($msg);
            }

        }

        return $result;
    }

    private function sendRequest($method, $data)
    {
        $http = new Client([
            'base_uri' => $this->profile->url,
            'timeout'  => self::TIMEOUT,
        ]);
        try {
            $response = $http->request('POST', $method, ['form_params' => $data]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new SberbankException('CURL Error or Timeout', $e);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw new SberbankException('Status code is 4xx', $e);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new SberbankException('Status code is 5xx', $e);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw new SberbankException('Illegal error', $e);
        }
        $response = $response->getBody();

        $this->addDebugLog($this->profile->url . '/' . $method, $data, $response);

        return $response;
    }

    private function addDebugLog($url, $request, $response)
    {
        try {
            /* @var $logger Syslog */
            $logger = \v0\Module::getComponent('syslog');

            // hide password on log
            if (key_exists('password', $request)) {
                $request[] = 'password';
                unset($request['password']);
            }

            $message = sprintf('%s %s -> %s', $url, json_encode($request), $response);
            $logger->addDebugLog($message);
        } catch (\yii\base\Exception $ignore) {
        }
    }

}