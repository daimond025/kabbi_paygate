<?php

namespace sberbank\components\payment;

use app\components\exception\NotEnoughMoneyHttpException;


/**
 * Class PaymentProcessing
 * @package sberbank\components\payment
 */
class PaymentProcessing
{
    const ACTION_CODE_SUCCESS_PAYMENT = 0;
    const ACTION_CODE_NOT_ENOUGH_MONEY = 116;

    /**
     * Validate payment processing
     *
     * @param int $actionCode
     *
     * @throws NotEnoughMoneyHttpException
     * @throws SberbankException
     */
    public function validate($actionCode)
    {
        switch ($actionCode) {
            case self::ACTION_CODE_SUCCESS_PAYMENT:
                break;
            case self::ACTION_CODE_NOT_ENOUGH_MONEY:
                throw new NotEnoughMoneyHttpException('on card');
            default:
                throw new SberbankException("Processing error [actionCode={$actionCode}]");
        }
    }
}