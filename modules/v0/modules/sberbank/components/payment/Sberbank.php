<?php
namespace sberbank\components\payment;

use sberbank\models\Profile;
use sberbank\models\ProfileDAO;
use sberbank\exception\NotFoundProfileException;

class Sberbank extends \yii\base\Object
{
    
    /**
     * @var \sberbank\models\Profile
     */
    private $profile;
    
    public function __construct($config = [])
    {
        $this->profile = new Profile(null);
        parent::__construct($config);
    }

    public function getProfile()
    {
        if ($this->profile) {
            return $this->profile;
        } else {
            throw new NotFoundProfileException('No set profile');
        }
    }
    
    public function setProfile($paymentName)
    {
        $profile = ProfileDAO::findOne(['paymentName' => $paymentName]);
        if ($profile) {
            $this->profile = new Profile($profile);
        }
    }
    
    public function getUrlAndOrderIdForRegistryCard($clientId)
    {
        $this->profile->setMode(Profile::MODE_BINDING);
        return $this->profile->getApi()->register(
            $clientId,
            'replicate_' . \Yii::$app->security->generateRandomString(8),
            100,
            643,
            'Test order for add new card'
        );
    }
    
    public function charge($amount, $currency, $clientId, $bindingId, $description, $orderNumber = '')
    {
        $this->profile->setMode(Profile::MODE_PAYMENT);
        $api = $this->profile->getApi();
        $orderId = $api->register($clientId, $orderNumber, $amount,
                 $currency, $description, $bindingId)['order'];
        $api->payment($orderId, $bindingId);
        return $orderId;
    }
    
    public function refundCharge($orderId, $amount, $isBindingProfile = false)
    {
        if ($isBindingProfile) {
            $this->profile->setMode(Profile::MODE_BINDING);
        } else {
            $this->profile->setMode(Profile::MODE_PAYMENT);
        }
        return $this->profile->getApi()->refund($orderId, $amount);
    }
    
    public function getStatusCharge($orderId, $isBindingProfile = false)
    {
        if ($isBindingProfile) {
            $this->profile->setMode(Profile::MODE_BINDING);
        } else {
            $this->profile->setMode(Profile::MODE_PAYMENT);
        }
        return $this->profile->getApi()->status($orderId);
    }
    
    public function getCardList($clientId)
    {
        $this->profile->setMode(Profile::MODE_BINDING);
        return $this->profile->getApi()->bindings($clientId);
    }
    
    public function deleteCard($bindingId)
    {
        $this->profile->setMode(Profile::MODE_BINDING);
        return $this->profile->getApi()->unbind($bindingId);
    }
    
}