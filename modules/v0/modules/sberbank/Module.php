<?php

namespace sberbank;

class Module extends \yii\base\Module
{
    
    public function init()
    {
        parent::init();

        $this->components = [
            'payment' => 'sberbank\components\payment\Sberbank',
            'paymentGateProfile' => 'sberbank\models\ProfileDAO',
        ];

    }
    
}