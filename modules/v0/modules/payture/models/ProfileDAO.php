<?php

namespace payture\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payture_profile".
 *
 * @property integer $id
 * @property string $paymentName
 * @property string $key3dsAdd
 * @property string $keyPay
 * @property string $password
 * @property integer $isDebug
 * @property double $commission
 * @property string $currency
 * @property integer $createdAt
 * @property integer $updatedAt
 */
class ProfileDAO extends ActiveRecord
{
    const TYPE_ID = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payture_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentName', 'key3dsAdd', 'keyPay', 'password', 'currency'], 'required'],
            [['isDebug', 'createdAt', 'updatedAt'], 'integer'],
            [['commission'], 'number'],
            [['paymentName'], 'string', 'max' => 32],
            [['key3dsAdd', 'keyPay', 'password', 'currency'], 'string', 'max' => 255],
            [['paymentName'], 'unique'],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'paymentName' => 'Payment Name',
            'key3dsAdd'   => 'Key3ds Add',
            'keyPay'      => 'Key Pay',
            'password'    => 'Password',
            'isDebug'     => 'Is Debug',
            'commission'  => 'Commission',
            'currency'    => 'Currency',
            'createdAt'   => 'Created At',
            'updatedAt'   => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    public function setTypeId($value)
    {
    }

    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'typeId',
            'paymentName',
            'key3dsAdd',
            'keyPay',
            'isDebug',
            'commission',
            'currency',
        ];
    }
}
