<?php

namespace payture\models;

class DbPayment
{
    /**
     * @var string
     */
    private $paymentName;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $merchantOrderId;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    public function __construct($paymentName, $clientId, $orderNumber, $merchantOrderId, $amount, $currency)
    {
        $this->paymentName = $paymentName;
        $this->clientId = $clientId;
        $this->orderNumber = $orderNumber;
        $this->merchantOrderId = $merchantOrderId;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getMerchantOrderId()
    {
        return $this->merchantOrderId;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'paymentName'     => $this->paymentName,
            'clientId'        => $this->clientId,
            'orderNumber'     => $this->orderNumber,
            'merchantOrderId' => $this->merchantOrderId,
            'amount'          => $this->amount,
        ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}