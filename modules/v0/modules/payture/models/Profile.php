<?php

namespace payture\models;

use payture\exceptions\NotFoundProfileException;

/**
 * Class Profile
 * @package payture\models
 */
class Profile extends ProfileDAO
{
    const URL_API = 'https://secure.payture.com/vwapi';
    const URL_API_DEBUG = 'https://sandbox3.payture.com/vwapi';

    /**
     * @param $paymentName
     *
     * @return static
     * @throws NotFoundProfileException
     */
    public static function get($paymentName)
    {
        $profile = self::findOne(['paymentName' => $paymentName]);
        if (!$profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }

        return $profile;
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return (int)$this->isDebug === 1 ? self::URL_API_DEBUG : self::URL_API;
    }

}