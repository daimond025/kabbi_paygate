<?php

namespace payture;

use GuzzleHttp\Client;
use payture\components\api\Payture;
use payture\components\DbPaymentManager;
use payture\components\PaytureService;
use payture\models\Profile;

/**
 * Class Module
 * @package payture
 */
class Module extends \yii\base\Module
{
    const DEFAULT_USER_PASSWORD = 'jxtymckj;ysqgfhjkmgjkmpjdfntkz!';

    public $networkTimeout;
    public $syslog;
    public $userIp;

    public function init()
    {
        parent::init();

        $httpClient = new Client(['timeout' => $this->networkTimeout]);
        $api = new Payture($httpClient, $this->syslog);
        $manager = new DbPaymentManager($this->syslog);
        $service = new PaytureService($api, $manager, self::DEFAULT_USER_PASSWORD, $this->userIp);

        $this->components = [
            'paymentGateProfile' => Profile::class,
            'paytureService'     => $service,
        ];
    }

}