<?php

namespace payture\components\exceptions;

use yii\web\HttpException;

class PaytureException extends HttpException
{
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct(500, $message, $code, $previous);
    }
}