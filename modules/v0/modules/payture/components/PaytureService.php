<?php

namespace payture\components;

use payture\components\api\enum\Error;
use payture\components\api\exceptions\RequestException;
use payture\components\api\models\ActivateRequest;
use payture\components\api\models\Card;
use payture\components\api\models\CheckUserRequest;
use payture\components\api\models\GetListRequest;
use payture\components\api\models\InitRequest;
use payture\components\api\models\PayRequest;
use payture\components\api\models\PayStatusRequest;
use payture\components\api\models\RefundRequest;
use payture\components\api\models\RegisterUserRequest;
use payture\components\api\models\RemoveRequest;
use payture\components\api\models\Payment;
use payture\components\api\Payture;
use payture\components\exceptions\PaytureException;
use payture\exceptions\NotFoundProfileException;
use payture\models\DbPayment;
use payture\models\Profile;

/**
 * Class PaytureService
 * @package payture\components
 */
class PaytureService
{
    /**
     * @var Payture
     */
    private $api;

    /**
     * @var DbPaymentManager
     */
    private $dbPaymentManager;

    /**
     * @var string
     */
    private $userPassword;

    /**
     * @var string
     */
    private $userIp;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string|null
     */
    private $language;

    public function __construct(Payture $api, DbPaymentManager $dbPaymentManager, $userPassword, $userIp)
    {
        $this->api = $api;
        $this->dbPaymentManager = $dbPaymentManager;
        $this->userPassword = $userPassword;
        $this->userIp = $userIp;
    }

    /**
     * @param string $paymentName
     *
     * @throws NotFoundProfileException
     */
    public function setProfile($paymentName)
    {
        $this->profile = Profile::get($paymentName);
    }

    /**
     * @return Profile
     *
     * @throws NotFoundProfileException
     */
    public function getProfile()
    {
        if ($this->profile === null) {
            throw new NotFoundProfileException('Payture profile not found');
        }

        return $this->profile;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        switch ($language) {
            case 'en':
                $this->language = 'En';
                break;
            case 'de':
                $this->language = 'De';
                break;
            default:
                $this->language = 'Ru';
        }
    }

    /**
     * @param string $clientId
     *
     * @return array
     * @throws NotFoundProfileException
     * @throws RequestException
     */
    public function getCards($clientId)
    {
        $clientCards = $this->getClientCards($clientId);

        $cards = [];
        foreach ($clientCards as $card) {
            /** @var Card $card */
            if ($card->isActive()) {
                $cards[] = ['pan' => $card->getPan(), 'binding' => $card->getId()];
            }
        }

        return $cards;
    }

    /**
     * @param string $clientId
     * @return string
     *
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function getUrlForBindingCard($clientId)
    {
        $profile = $this->getProfile();

        $request = new InitRequest($profile->getApiUrl(), $profile->key3dsAdd, $clientId, $this->userPassword,
            $this->userIp);
        $request->setLanguage($this->language);
        $response = $this->api->init($request);

        if (!$response->isSuccess()) {
            throw new PaytureException("Init error: code={$response->getErrorCode()}, paymentName={$profile->paymentName}, clientId={$clientId}");
        }

        return "{$profile->getApiUrl()}/Add?SessionId={$response->getSessionId()}";
    }

    /**
     * @param string $clientId
     * @param string $cardId
     *
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function removeCard($clientId, $cardId)
    {
        $profile = $this->getProfile();

        $request = new RemoveRequest($profile->getApiUrl(), $profile->key3dsAdd, $clientId, $this->userPassword,
            $cardId);
        $response = $this->api->remove($request);

        if ($response->isSuccess()) {
            return;
        }

        $card = $this->findClientCard($clientId, $cardId);
        $pan = $card !== null ? $card->getPan() : null;

        throw new PaytureException("Remove card error: code={$response->getErrorCode()}, paymentName={$profile->paymentName}, cardId={$cardId}, pan={$pan}");
    }

    /**
     * @param string $clientId
     * @param string $cardId
     * @param string $orderId
     * @param int $amount
     * @param int $currency
     *
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function pay($clientId, $cardId, $orderId, $amount, $currency)
    {
        $profile = $this->getProfile();


        if ((int)$profile->currency !== (int)$currency) {
            throw new PaytureException("Invalid currency: profileCurrency={$profile->currency}, orderCurrency={$currency}");
        }

        $card = $this->findClientCard($clientId, $cardId);
        if ($card === null) {
            throw new PaytureException("Card not found: paymentName={$profile->paymentName}, clientId={$clientId}, cardId={$cardId}");
        }

        if (!$card->isActive()) {
            throw new PaytureException("Card is not active: paymentName={$profile->paymentName}, clientId={$clientId}, cardId={$cardId}, pan={$card->getPan()}");
        }

        $request = new PayRequest($profile->getApiUrl(), $profile->keyPay, $clientId, $this->userPassword, $cardId,
            $orderId, $amount, $this->userIp);
        $response = $this->api->pay($request);

        if ($response->isSuccess()) {
            $dbPayment = new DbPayment($profile->paymentName, $clientId, $orderId, $response->getMerchantOrderId(),
                $response->getAmount(), $currency);
            $this->dbPaymentManager->save($dbPayment);

            return;
        }

        throw new PaytureException("Pay error: code={$response->getErrorCode()}, paymentName={$profile->paymentName}, clientId={$clientId}, cardId={$cardId}, orderId={$orderId}, amount={$amount}, currency={$currency}");
    }

    /**
     * @param string $clientId
     * @param string $orderId
     *
     * @return Payment
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function status($clientId, $orderId)
    {
        return $this->getPayment($clientId, $orderId);
    }

    /**
     * @param string $clientId
     * @param string $orderId
     * @param int $amount
     *
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    public function refund($clientId, $orderId, $amount)
    {
        $profile = $this->getProfile();

        $request = new RefundRequest($profile->getApiUrl(), $profile->keyPay, $profile->password, $orderId, $amount);
        $response = $this->api->refund($request);

        if ($response->isSuccess()) {
            return;
        }

        throw new PaytureException("Refund error: code={$response->getErrorCode()}, paymentName={$profile->paymentName}, clientId={$clientId}, orderId={$orderId}, amount={$amount}");
    }

    /**
     * @param string $clientId
     * @param string $cardId
     *
     * @return Card|null
     *
     * @throws NotFoundProfileException
     * @throws RequestException
     */
    public function findClientCard($clientId, $cardId)
    {
        $cards = $this->getClientCards($clientId);
        foreach ($cards as $card) {
            /** @var Card $card */
            if ($card->getId() === $cardId) {
                return $card;
            }
        }

        return null;
    }

    /**
     * @param string $clientId
     * @return array
     *
     * @throws NotFoundProfileException
     * @throws RequestException
     */
    private function getClientCards($clientId)
    {
        $profile = $this->getProfile();

        $request = new GetListRequest($profile->getApiUrl(), $profile->key3dsAdd, $clientId, $this->userPassword);
        $response = $this->api->getList($request);

        return $response->getCards();
    }

    /**
     * @param string $clientId
     * @param string $orderId
     *
     * @return Payment
     * @throws NotFoundProfileException
     * @throws PaytureException
     * @throws RequestException
     */
    private function getPayment($clientId, $orderId)
    {
        $profile = $this->getProfile();

        $dbPayment = $this->dbPaymentManager->get($profile->paymentName, $orderId);

        $request = new PayStatusRequest($profile->getApiUrl(), $profile->keyPay, $orderId);
        $response = $this->api->payStatus($request);

        if ($response->isSuccess()) {
            $card = $this->findClientCard($clientId, $response->getCardId());

            return new Payment($clientId, $response->getOrderId(), $response->getStatus(), $card,
                $response->getAmount(), $dbPayment->getCurrency());
        }

        throw new PaytureException("Status error: code={$response->getErrorCode()}, paymentName={$profile->paymentName}, clientId={$clientId}, orderId={$orderId}");
    }
}