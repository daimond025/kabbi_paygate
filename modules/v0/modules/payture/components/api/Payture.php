<?php

namespace payture\components\api;

use app\components\syslog\Syslog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Uri;
use payture\components\api\exceptions\RequestException;
use payture\components\api\models\ActivateRequest;
use payture\components\api\models\ActivateResponse;
use payture\components\api\models\CheckUserRequest;
use payture\components\api\models\CheckUserResponse;
use payture\components\api\models\GetListRequest;
use payture\components\api\models\GetListResponse;
use payture\components\api\models\InitRequest;
use payture\components\api\models\InitResponse;
use payture\components\api\models\PayRequest;
use payture\components\api\models\PayResponse;
use payture\components\api\models\PayStatusRequest;
use payture\components\api\models\PayStatusResponse;
use payture\components\api\models\RefundRequest;
use payture\components\api\models\RefundResponse;
use payture\components\api\models\RegisterUserRequest;
use payture\components\api\models\RegisterUserResponse;
use payture\components\api\models\RemoveRequest;
use payture\components\api\models\RemoveResponse;
use payture\components\api\models\RequestInterface;

/**
 * Class Payture
 * @package payture\components\api
 */
class Payture
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Syslog
     */
    private $logger;

    public function __construct(Client $client, Syslog $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param RegisterUserRequest $request
     *
     * @return RegisterUserResponse
     * @throws RequestException
     */
    public function registerUser(RegisterUserRequest $request)
    {
        $response = new RegisterUserResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param CheckUserRequest $request
     *
     * @return CheckUserResponse
     * @throws RequestException
     */
    public function checkUser(CheckUserRequest $request)
    {
        $response = new CheckUserResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param InitRequest $request
     *
     * @return InitResponse
     * @throws RequestException
     */
    public function init(InitRequest $request)
    {
        $response = new InitResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param ActivateRequest $request
     *
     * @return ActivateResponse
     * @throws RequestException
     */
    public function activate(ActivateRequest $request)
    {
        $response = new ActivateResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param GetListRequest $request
     *
     * @return GetListResponse
     * @throws RequestException
     */
    public function getList(GetListRequest $request)
    {
        $response = new GetListResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param RemoveRequest $request
     *
     * @return RemoveResponse
     * @throws RequestException
     */
    public function remove(RemoveRequest $request)
    {
        $response = new RemoveResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param PayStatusRequest $request
     *
     * @return PayStatusResponse
     * @throws RequestException
     */
    public function payStatus(PayStatusRequest $request)
    {
        $response = new PayStatusResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param PayRequest $request
     *
     * @return PayResponse
     * @throws RequestException
     */
    public function pay(PayRequest $request)
    {
        $response = new PayResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param RefundRequest $request
     *
     * @return RefundResponse
     * @throws RequestException
     */
    public function refund(RefundRequest $request)
    {
        $response = new RefundResponse();
        $response->load($this->sendRequest($request));

        return $response;
    }

    /**
     * @param RequestInterface $request
     *
     * @return string
     * @throws RequestException
     */
    private function sendRequest(RequestInterface $request)
    {
        $url = $request->generateUrl();
        try {
            $response = $this->client->get(new Uri($url));

            $content = $response->getBody()->getContents();
            $this->addDebugLog($request->generateUrl(), $content);

            return $content;
        } catch (\Exception $ex) {
            $response = $ex instanceof BadResponseException && $ex->hasResponse() ? $ex->getResponse() : null;
            $content = $response !== null ? $response->getBody()->getContents() : null;
            $this->addDebugLog($request->generateUrl(), $content);

            throw new RequestException("Send request error: url={$request->generateUrl()}, response={$content}, error=\"{$ex->getMessage()}\"");
        }
    }

    /**
     * @param string $request
     * @param string $response
     */
    private function addDebugLog($request, $response)
    {
        try {
            $message = sprintf('%s -> %s', $request, $response);
            $this->logger->addDebugLog($message);
        } catch (\Exception $ignore) {
        }
    }
}