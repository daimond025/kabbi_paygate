<?php

namespace payture\components\api\enum;

class Error
{
    const USER_NOT_FOUND = 'USER_NOT_FOUND';
    const DUPLICATE_USER = 'DUPLICATE_USER';
}