<?php

namespace payture\components\api\exceptions;

use yii\web\HttpException;

class RequestException extends HttpException
{
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct(500, $message, $code, $previous);
    }
}