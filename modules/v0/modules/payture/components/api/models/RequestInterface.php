<?php

namespace payture\components\api\models;

interface RequestInterface
{
    /**
     * @return string
     */
    public function generateUrl();
}