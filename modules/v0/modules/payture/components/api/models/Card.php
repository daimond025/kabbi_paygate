<?php

namespace payture\components\api\models;

class Card
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $pan;

    /**
     * @var bool
     */
    private $isActive;

    public function __construct($id, $pan, $isActive)
    {
        $this->id = $id;
        $this->pan = $pan;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }
}