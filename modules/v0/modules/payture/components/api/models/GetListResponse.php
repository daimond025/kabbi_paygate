<?php

namespace payture\components\api\models;

class GetListResponse extends BaseResponse
{
    const CARD_STATUS_IS_ACTIVE = 'IsActive';

    /**
     * @var array
     */
    private $cards;

    public function load($content)
    {
        parent::load($content);

        $this->cards = [];
        if ($this->isSuccess) {
            $xml = simplexml_load_string($content);

            foreach ($xml as $card) {
                $id = isset($card['CardId']) ? (string)$card['CardId'] : '';
                $pan = isset($card['CardName']) ? (string)$card['CardName'] : '';
                $isActive = isset($card['Status']) ? (string)$card['Status'] === self::CARD_STATUS_IS_ACTIVE : false;
                $this->cards[] = new Card($id, $pan, $isActive);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getCards()
    {
        return $this->cards;
    }
}