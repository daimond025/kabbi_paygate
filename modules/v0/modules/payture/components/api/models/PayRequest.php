<?php

namespace payture\components\api\models;

class PayRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $cardId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $userIp;

    public function __construct($url, $key, $login, $password, $cardId, $orderId, $amount, $userIp)
    {
        $this->url = $url;
        $this->key = $key;
        $this->login = $login;
        $this->password = $password;
        $this->cardId = $cardId;
        $this->orderId = $orderId;
        $this->amount = $amount;
        $this->userIp = $userIp;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = urlencode(sprintf('VWUserLgn=%s;VWUserPsw=%s;CardId=%s;OrderId=%s;Amount=%s;IP=%s;',
            $this->login, $this->password, $this->cardId, $this->orderId, $this->amount, $this->userIp));

        return "{$this->url}/Pay?VWID={$this->key}&DATA={$data}";
    }
}