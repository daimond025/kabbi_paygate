<?php

namespace payture\components\api\models;

class PayStatusRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $orderId;

    public function __construct($url, $key, $orderId)
    {
        $this->url = $url;
        $this->key = $key;
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = urlencode("OrderId={$this->orderId};");

        return "{$this->url}/PayStatus?VWID={$this->key}&DATA={$data}";
    }
}