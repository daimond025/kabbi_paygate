<?php

namespace payture\components\api\models;

class Payment
{
    const STATUS_CHARGED = 'Charged';

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $status;

    /**
     * @var Card|null
     */
    private $card;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var int
     */
    private $currency;

    public function __construct($clientId, $orderId, $status, $card, $amount, $currency)
    {
        $this->clientId = $clientId;
        $this->orderId = $orderId;
        $this->status = $status;
        $this->card = $card;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return null|Card
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isCharged()
    {
        return $this->status === self::STATUS_CHARGED;
    }
}