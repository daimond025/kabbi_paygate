<?php

namespace payture\components\api\models;

class CheckUserRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    public function __construct($url, $key, $login, $password)
    {
        $this->url = $url;
        $this->key = $key;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = urlencode(sprintf('VWUserLgn=%s;VWUserPsw=%s;', $this->login, $this->password));

        return "{$this->url}/Check?VWID={$this->key}&DATA={$data}";
    }
}