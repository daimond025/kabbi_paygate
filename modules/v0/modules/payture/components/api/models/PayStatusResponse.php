<?php

namespace payture\components\api\models;

class PayStatusResponse extends BaseResponse
{
    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $orderId;

    /**
     * @var string|null
     */
    private $cardId;

    /**
     * @var int|null
     */
    private $amount;

    public function load($content)
    {
        parent::load($content);

        $xml = simplexml_load_string($content);
        $this->status = $this->isSuccess() && isset($xml['Status']) ? (string)$xml['Status'] : null;
        $this->orderId = $this->isSuccess() && isset($xml['OrderId']) ? (string)$xml['OrderId'] : null;
        $this->cardId = $this->isSuccess() && isset($xml['CardId']) ? (string)$xml['CardId'] : null;
        $this->amount = $this->isSuccess() && isset($xml['Amount']) ? (int)$xml['Amount'] : null;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return null|string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return null|string
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
}