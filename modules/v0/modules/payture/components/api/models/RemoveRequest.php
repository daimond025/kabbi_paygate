<?php

namespace payture\components\api\models;

class RemoveRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $cardId;

    public function __construct($url, $key, $login, $password, $cardId)
    {
        $this->url = $url;
        $this->key = $key;
        $this->login = $login;
        $this->password = $password;
        $this->cardId = $cardId;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = urlencode(sprintf('VWUserLgn=%s;VWUserPsw=%s;CardId=%s;', $this->login, $this->password,
            $this->cardId));

        return "{$this->url}/Remove?VWID={$this->key}&DATA={$data}";
    }
}