<?php

namespace payture\components\api\models;

class InitRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $userIp;

    /**
     * @var string|null
     */
    private $language;

    public function __construct($url, $key, $login, $password, $userIp)
    {
        $this->url = $url;
        $this->key = $key;
        $this->login = $login;
        $this->password = $password;
        $this->userIp = $userIp;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = sprintf('VWUserLgn=%s;VWUserPsw=%s;SessionType=Add;TemplateTag=mobile;IP=%s;', $this->login,
            $this->password, $this->userIp);
        if ($this->language !== null) {
            $data .= "Language={$this->language};";
        }
        $data = urlencode($data);

        return "{$this->url}/Init?VWID={$this->key}&DATA={$data}";
    }
}