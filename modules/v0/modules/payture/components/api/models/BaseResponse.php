<?php

namespace payture\components\api\models;

abstract class BaseResponse
{
    /**
     * @var bool
     */
    protected $isSuccess;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string|null
     */
    private $errorCode;

    /**
     * @param $content
     *
     * @return $this
     */
    public function load($content)
    {
        $xml = simplexml_load_string($content);
        $this->isSuccess = isset($xml['Success']) ? (string)$xml['Success'] === 'True' : false;

        if (!$this->isSuccess) {
            $this->errorCode = isset($xml['ErrCode']) ? (string)$xml['ErrCode'] : 'Unknown error';
        }

        $this->login = isset($xml['VWUserLgn']) ? (string)$xml['VWUserLgn'] : '';

        return $this;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * @return null|string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}