<?php

namespace payture\components\api\models;

class RefundRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var int
     */
    private $amount;

    public function __construct($url, $key, $password, $orderId, $amount)
    {
        $this->url = $url;
        $this->key = $key;
        $this->password = $password;
        $this->orderId = $orderId;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = urlencode(sprintf('Password=%s;OrderId=%s;Amount=%s;', $this->password, $this->orderId, $this->amount));

        return "{$this->url}/Refund?VWID={$this->key}&DATA={$data}";
    }
}