<?php

namespace payture\components\api\models;

class RemoveResponse extends BaseResponse
{
    /**
     * @var string|null
     */
    private $cardId;

    public function load($content)
    {
        parent::load($content);

        $xml = simplexml_load_string($content);
        $this->cardId = isset($xml['CardId']) ? (string)$xml['CardId'] : null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCardId()
    {
        return $this->cardId;
    }
}