<?php

namespace payture\components\api\models;

class PayResponse extends BaseResponse
{
    /**
     * @var string|null
     */
    private $orderId;

    /**
     * @var string|null
     */
    private $merchantOrderId;

    /**
     * @var int|null
     */
    private $amount;

    public function load($content)
    {
        parent::load($content);

        $xml = simplexml_load_string($content);
        $this->orderId = isset($xml['OrderId']) ? (string)$xml['OrderId'] : null;
        $this->merchantOrderId = isset($xml['MerchantOrderId']) ? (string)$xml['MerchantOrderId'] : null;
        $this->amount = isset($xml['Amount']) ? (int)$xml['Amount'] : null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return string|null
     */
    public function getMerchantOrderId()
    {
        return $this->merchantOrderId;
    }

    /**
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
}