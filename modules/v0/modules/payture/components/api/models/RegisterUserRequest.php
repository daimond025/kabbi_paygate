<?php

namespace payture\components\api\models;

class RegisterUserRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string|null
     */
    private $phone;

    public function __construct($url, $key, $login, $password, $phone = null)
    {
        $this->key = $key;
        $this->login = $login;
        $this->password = $password;
        $this->phone = $phone;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        $data = sprintf('VWUserLgn=%s;VWUserPsw=%s;', $this->login, $this->password);
        if ($this->phone !== null) {
            $data .= "PhoneNumber={$this->phone};";
        }
        $data = urlencode($data);

        return "{$this->url}/Register?VWID={$this->key}&DATA={$data}";
    }
}