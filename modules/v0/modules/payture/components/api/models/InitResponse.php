<?php

namespace payture\components\api\models;

use payture\components\api\exceptions\RequestException;

class InitResponse extends BaseResponse
{
    /**
     * @var int|null
     */
    private $amount;

    /**
     * @var string|null
     */
    private $sessionId;

    /**
     * @var string|null
     */
    private $sessionType;

    /**
     * @param string $content
     *
     * @return InitResponse
     * @throws RequestException
     */
    public function load($content)
    {
        parent::load($content);

        $xml = simplexml_load_string($content);
        $this->amount = isset($xml['Amount']) ? (int)$xml['Amount'] : null;
        $this->sessionId = isset($xml['SessionId']) ? (string)$xml['SessionId'] : null;
        $this->sessionType = isset($xml['SessionType']) ? (string)$xml['SessionType'] : null;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string|null
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return string|null
     */
    public function getSessionType()
    {
        return $this->sessionType;
    }

}