<?php

namespace payture\components\api\models;

class RefundResponse extends BaseResponse
{
    /**
     * @var string|null
     */
    private $orderId;

    /**
     * @var int|null
     */
    private $newAmount;

    public function load($content)
    {
        parent::load($content);

        $xml = simplexml_load_string($content);
        $this->orderId = isset($xml['OrderId']) ? (string)$xml['OrderId'] : null;
        $this->newAmount = $this->isSuccess() && isset($xml['NewAmount']) ? (int)$xml['NewAmount'] : null;

        return $this;
    }
}