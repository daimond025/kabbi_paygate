<?php

namespace payture\components;

use app\components\syslog\Syslog;
use payture\models\PaymentDAO;
use payture\components\exceptions\PaytureException;
use payture\models\DbPayment;

class DbPaymentManager
{
    /**
     * @var Syslog
     */
    private $logger;

    public function __construct(Syslog $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     *
     * @return DbPayment
     * @throws PaytureException
     */
    public function get($paymentName, $orderNumber)
    {
        try {
            $model = $this->findPayment($paymentName, $orderNumber);
            if ($model === null) {
                throw new PaytureException("Payment not found: paymentName=$paymentName, orderNumber=$orderNumber");
            }

            return new DbPayment($model->paymentName, $model->clientId, $model->orderNumber, $model->merchantOrderId,
                $model->amount, $model->currency);
        } catch (\Exception $ex) {
            throw new PaytureException("Get payment error: error=\"{$ex->getMessage()}\"");
        }
    }

    /**
     * @param DbPayment $payment
     */
    public function save(DbPayment $payment)
    {
        try {
            $model = $this->findPayment($payment->getPaymentName(), $payment->getOrderNumber());
            if ($model === null) {
                $model = new PaymentDAO();
            }

            $model->paymentName = $payment->getPaymentName();
            $model->clientId = $payment->getClientId();
            $model->orderNumber = $payment->getOrderNumber();
            $model->merchantOrderId = $payment->getMerchantOrderId();
            $model->amount = $payment->getAmount();
            $model->currency = $payment->getCurrency();

            if (!$model->save()) {
                throw new PaytureException(implode('; ', $model->getFirstErrors()));
            }
        } catch (\Exception $ex) {
            $this->logger->addDebugLog("Save payment error: error=\"{$ex->getMessage()}\", paymentData={$payment}");
        }
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     *
     * @return PaymentDAO|null
     */
    private function findPayment($paymentName, $orderNumber)
    {
        $model = PaymentDAO::find()->where(['paymentName' => $paymentName, 'orderNumber' => $orderNumber])->one();

        return empty($model) ? null : $model;
    }
}