<?php

namespace kkb\models\requests;

use kkb\components\sign\Sign;
use kkb\exceptions\PaymentException;
use kkb\models\Profile;

class RefundPaymentRequest
{
    const COMMAND_REVERSE = 'reverse';
    const COMMAND_REFUND = 'refund';

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $approvalCode;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $currencyCode;

    public function __construct(Profile $profile, $status, $reference, $approvalCode, $orderId, $amount, $currencyCode)
    {
        $this->profile = $profile;

        $this->status = $status;
        $this->reference = $reference;
        $this->approvalCode = $approvalCode;
        $this->orderId = $orderId;
        $this->amount = $amount;
        $this->currencyCode = $currencyCode;
    }

    public function generateRequest($reason = 'Refund payment')
    {
        $template =
            '<merchant id="%MERCHANT_ID%">
            <command type="%COMMAND%"/>
            <payment reference="%REFERENCE%" approval_code="%APPROVAL_CODE%" orderid="%ORDER_ID%" amount="%AMOUNT%" currency_code="%CURRENCY_CODE%"/>
            <reason>%REASON%</reason>
        </merchant>';

        $command = $this->getCommand();

        $data = str_replace([
            '%MERCHANT_ID%',
            '%COMMAND%',
            '%REFERENCE%',
            '%APPROVAL_CODE%',
            '%ORDER_ID%',
            '%AMOUNT%',
            '%CURRENCY_CODE%',
            '%REASON%',
        ],
            [
                $this->profile->merchantId,
                $command,
                $this->reference,
                $this->approvalCode,
                $this->orderId,
                $this->amount,
                $this->currencyCode,
                $reason,
            ], $template);

        $sign = new Sign();
        $sign->invert();
        $sign->loadPrivateKey($this->profile->privateCertificate, $this->profile->privateCertificatePass);

        return "<document>{$data}<merchant_sign type=\"RSA\" cert_id=\"{$this->profile->certificateId}\">{$sign->sign64($data)}</merchant_sign></document>";
    }

    private function getCommand()
    {
        if ($this->status !== '0' && $this->status !== '1') {
            throw new PaymentException("Cancel payment error: status={$this->status}");
        }

        return (string)$this->status === '1' ? self::COMMAND_REFUND : self::COMMAND_REVERSE;
    }
}