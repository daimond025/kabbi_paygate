<?php

namespace kkb\models\requests;

use kkb\components\sign\Sign;
use kkb\models\Profile;

class RepeatPaymentRequest
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $binding;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    public function __construct(Profile $profile, $clientId, $orderId, $binding, $amount, $currency)
    {
        $this->profile = $profile;
        $this->clientId = $clientId;
        $this->orderId = $orderId;
        $this->binding = $binding;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function getSignedOrder()
    {
        $template = '<merchant cert_id="%CERTIFICATE_ID%" name="%SHOP_NAME%">
            <order order_id="%ORDER_ID%" amount="%AMOUNT%" currency="%CURRENCY%">
                <department merchant_id="%MERCHANT_ID%" Reference="%REFERENCE%" order_al="%ORDER_AL%"/>
            </order>
        </merchant>';

        $data = str_replace([
            '%CERTIFICATE_ID%',
            '%SHOP_NAME%',
            '%ORDER_ID%',
            '%AMOUNT%',
            '%CURRENCY%',
            '%MERCHANT_ID%',
            '%REFERENCE%',
            '%ORDER_AL%',
        ], [
            $this->profile->certificateId,
            $this->profile->merchantName,
            $this->orderId,
            $this->amount,
            $this->currency,
            $this->profile->merchantId,
            $this->binding,
            $this->clientId,
        ], $template);

        $sign = new Sign();
        $sign->invert();
        $sign->loadPrivateKey($this->profile->privateCertificate, $this->profile->privateCertificatePass);

        return base64_encode("<document>{$data}<merchant_sign type=\"RSA\">{$sign->sign64($data)}</merchant_sign></document>");
    }
}