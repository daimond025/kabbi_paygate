<?php

namespace kkb\models\requests;

use kkb\components\sign\Sign;
use kkb\models\Profile;

class RegisterCardRequest
{
    const MAX_PAYMENTS_PER_DAY = 200;
    const CARD_LIFE_IN_MONTHS = 36;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $orderId;

    public function __construct(Profile $profile, $clientId, $orderId)
    {
        $this->profile = $profile;
        $this->clientId = $clientId;
        $this->orderId = $orderId;
    }

    public function getSignedOrder()
    {
        $template =
            '<merchant cert_id="%CERTIFICATE_ID%" name="%SHOP_ID%">
                <order order_id="%ORDER_ID%" amount="%AMOUNT%" currency="%CURRENCY%">
                    <department merchant_id="%MERCHANT%" amount="%AMOUNT%" person_id="%PERSON_ID%" recur_freq="%RECUR_FREQ%"
                        recur_exp="%RECUR_EXP%" RL="%RL%"/>
                </order>
            </merchant>';

        $data = str_replace([
            '%CERTIFICATE_ID%',
            '%SHOP_ID%',
            '%ORDER_ID%',
            '%AMOUNT%',
            '%CURRENCY%',
            '%MERCHANT%',
            '%PERSON_ID%',
            '%RECUR_FREQ%',
            '%RECUR_EXP%',
            '%RL%',
        ],
            [
                $this->profile->certificateId,
                $this->profile->merchantName,
                $this->orderId,
                $this->profile->cardBindingSum,
                $this->profile->currency,
                $this->profile->merchantId,
                $this->clientId,
                self::MAX_PAYMENTS_PER_DAY,
                $this->generateCardExpiry(),
                $this->profile->paymentName,
            ], $template);

        $sign = new Sign();
        $sign->invert();
        $sign->loadPrivateKey($this->profile->privateCertificate, $this->profile->privateCertificatePass);

        return base64_encode("<document>$data<merchant_sign type=\"RSA\">{$sign->sign64($data)}</merchant_sign></document>");
    }

    private function generateCardExpiry()
    {
        $time = strtotime('+' . self::CARD_LIFE_IN_MONTHS . ' month');

        return date('Ymd', $time);
    }
}
