<?php

namespace kkb\models\requests;

use kkb\components\sign\Sign;
use kkb\models\Profile;

class PaymentInfoRequest
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $orderId;

    public function __construct(Profile $profile, $orderId)
    {
        $this->profile = $profile;
        $this->orderId = $orderId;
    }

    public function generate()
    {
        $template = '<merchant id="%MERCHANT_ID%"><order id="%ORDER_ID%"/></merchant>';
        $data = str_replace(['%MERCHANT_ID%', '%ORDER_ID%'], [$this->profile->merchantId, $this->orderId], $template);

        $sign = new Sign();
        $sign->invert();
        $sign->loadPrivateKey($this->profile->privateCertificate, $this->profile->privateCertificatePass);

        return "<document>{$data}<merchant_sign type=\"RSA\" cert_id=\"{$this->profile->certificateId}\">{$sign->sign64($data)}</merchant_sign></document>";
    }
}