<?php

namespace kkb\models\requests;

use kkb\components\sign\Sign;
use kkb\models\Profile;

class CardActionRequest
{
    const ACTION_LIST = 'list';
    const ACTION_DELETE = 'delete';

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $clientId;

    public function __construct(Profile $profile, $clientId)
    {
        $this->profile = $profile;
        $this->clientId = $clientId;
    }

    public function generate($action, $cardHash = null)
    {
        $template = '<merchant id="%MERCHANT_ID%"><client person_id="%CLIENT_ID%" action="%ACTION%" %HASH%/></merchant>';

        $data = str_replace([
            '%MERCHANT_ID%',
            '%CLIENT_ID%',
            '%ACTION%',
            '%HASH%',
        ],
            [
                $this->profile->merchantId,
                $this->clientId,
                $action,
                $cardHash === null ? '' : "hashcardmd5=\"{$cardHash}\"",
            ], $template);

        $sign = new Sign();
        $sign->invert();
        $sign->loadPrivateKey($this->profile->privateCertificate, $this->profile->privateCertificatePass);

        return "<document>{$data}<merchant_sign type=\"RSA\" cert_id=\"{$this->profile->certificateId}\">{$sign->sign64($data)}</merchant_sign></document>";
    }
}