<?php

namespace kkb\models\responses;

use kkb\exceptions\InvalidPaymentDataException;
use kkb\exceptions\PaymentException;
use kkb\models\Profile;

class RepeatPaymentResponseHandler
{
    const SUCCESS = '00';

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $inputError;

    /**
     * @var string
     */
    private $paymentError;

    /**
     * @var string
     */
    private $systemError;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $result;

    public function __construct(Profile $profile, $data)
    {
        $this->profile = $profile;
        $this->load($data);
    }

    public function validate()
    {
        if ($this->profile->merchantId !== $this->merchantId) {
            throw new InvalidPaymentDataException('Invalid merchantId error');
        }

        if ($this->result !== self::SUCCESS) {
            $error = implode('; ',
                array_filter([$this->inputError, $this->paymentError, $this->systemError], function ($item) {
                    return !empty($item);
                }));
            throw new PaymentException("Repeat payment error: result={$this->result}, error={$error}");
        }
    }

    private function load($data)
    {
        $xml = new \SimpleXMLElement($data);

        $this->inputError = isset($xml->xpath('//error/@input')[0]['input'])
            ? (string)$xml->xpath('//error/@input')[0]['input'] : '';
        $this->paymentError = isset($xml->xpath('//error/@payment')[0]['payment'])
            ? (string)$xml->xpath('//error/@payment')[0]['payment'] : '';
        $this->systemError = isset($xml->xpath('//error/@system')[0]['system'])
            ? (string)$xml->xpath('//error/@system')[0]['system'] : '';

        $this->merchantId = isset($xml->xpath('//payment/@MerchantID')[0]['MerchantID'])
            ? (string)$xml->xpath('//payment/@MerchantID')[0]['MerchantID'] : '';
        $this->orderId = isset($xml->xpath('//payment/@OrderId')[0]['OrderId'])
            ? (string)$xml->xpath('//payment/@OrderId')[0]['OrderId'] : '';
        $this->amount = isset($xml->xpath('//payment/@amount')[0]['amount'])
            ? (float)$xml->xpath('//payment/@amount')[0]['amount'] : 0;
        $this->reference = isset($xml->xpath('//payment/@reference')[0]['reference'])
            ? (string)$xml->xpath('//payment/@reference')[0]['reference'] : '';
        $this->result = isset($xml->xpath('//payment/@Result')[0]['Result'])
            ? (string)$xml->xpath('//payment/@Result')[0]['Result'] : '';
    }
}

