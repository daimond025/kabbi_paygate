<?php

namespace kkb\models\responses;

use kkb\components\KkbService;
use kkb\components\sign\Sign;
use kkb\exceptions\InvalidPaymentDataException;
use kkb\exceptions\InvalidSignatureException;
use kkb\exceptions\PaymentException;

class PaymentSuccessResponseHandler
{
    const SUCCESS = '00';

    /**
     * @var KkbService
     */
    private $service;

    /**
     * @var string
     */
    private $bankData;

    /**
     * @var string
     */
    private $bankSign;

    /**
     * @var string
     */
    private $paymentName;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $responseCode;

    /**
     * @var string
     */
    private $currency;

    public function __construct(KkbService $service, $data)
    {
        $this->load($data);

        $this->service = $service;
        $this->service->setProfile($this->paymentName);
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function validate()
    {
        $sign = new Sign();
        $sign->invert();

        if (!$sign->isValid64($this->bankData, $this->bankSign, $this->service->getProfile()->publicCertificate)) {
            throw new InvalidSignatureException('Invalid bank signature error');
        }

        $merchantId = $this->service->getProfile()->merchantId;
        if ($merchantId !== $this->merchantId) {
            throw new InvalidPaymentDataException("Invalid merchantId error: actual={$this->merchantId}, expected={$merchantId}");
        }

        if ($this->responseCode !== self::SUCCESS) {
            throw new PaymentException("Register card error: response_code={$this->responseCode}");
        }

        //        $payment = KkbPayment::get($this->paymentName, $this->orderNumber);
        //        $amount = $payment->getAmount();
        //
        //        if ($amount !== null && abs($amount - $this->amount) > 0.01) {
        //            throw new InvalidPaymentDataException('Invalid payment amount error');
        //        }
    }

    private function load($data)
    {
        $xml = new \SimpleXMLElement($data);

        $this->bankData = isset($xml->xpath('//bank')[0]) ? $xml->xpath('//bank')[0]->asXML() : '';
        $this->bankSign = isset($xml->xpath('//bank_sign')[0]) ? (string)$xml->xpath('//bank_sign')[0] : '';
        $this->merchantId = isset($xml->xpath('//payment/@merchant_id')[0]['merchant_id'])
            ? (string)$xml->xpath('//payment/@merchant_id')[0]['merchant_id'] : '';
        $this->paymentName = isset($xml->xpath('//department/@rl')[0]['rl'])
            ? (string)$xml->xpath('//department/@rl')[0]['rl'] : '';
        $this->orderId = isset($xml->xpath('//order/@order_id')[0]['order_id'])
            ? (string)$xml->xpath('//order/@order_id')[0]['order_id'] : '';
        $this->amount = isset($xml->xpath('//payment/@amount')[0]['amount'])
            ? (float)$xml->xpath('//payment/@amount')[0]['amount'] : 0;
        $this->reference = isset($xml->xpath('//payment/@reference')[0]['reference'])
            ? (string)$xml->xpath('//payment/@reference')[0]['reference'] : '';
        $this->responseCode = isset($xml->xpath('//payment/@response_code')[0]['response_code'])
            ? (string)$xml->xpath('//payment/@response_code')[0]['response_code'] : '';
        $this->currency = isset($xml->xpath('//order/@currency')[0]['currency'])
            ? (string)$xml->xpath('//order/@currency')[0]['currency'] : '';
    }
}

