<?php

namespace kkb\models\responses;

use kkb\components\sign\Sign;
use kkb\exceptions\InvalidSignatureException;
use kkb\exceptions\PaymentException;
use kkb\models\Profile;

class RefundPaymentResponseHandler
{
    const SUCCESS = '00';
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $bankData;

    /**
     * @var string
     */
    private $bankSign;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    public function __construct(Profile $profile, $data)
    {
        $this->profile = $profile;
        $this->load($data);
    }

    public function validate()
    {
        $sign = new Sign();
        $sign->invert();

        if (!$sign->isValid64($this->bankData, $this->bankSign, $this->profile->publicCertificate)) {
            throw new InvalidSignatureException('Invalid bank signature error');
        }

        if ($this->code !== self::SUCCESS) {
            throw new PaymentException("Refund payment error: result={$this->code}, error={$this->message}");
        }
    }

    private function load($data)
    {
        $xml = new \SimpleXMLElement($data);

        $this->bankData = isset($xml->xpath('//bank')[0]) ? $xml->xpath('//bank')[0]->asXML() : '';
        $this->bankSign = isset($xml->xpath('//bank_sign')[0]) ? (string)$xml->xpath('//bank_sign')[0] : '';
        $this->code = isset($xml->xpath('//response/@code')[0]['code']) ? (string)$xml->xpath('//response/@code')[0]['code'] : '';
        $this->message = isset($xml->xpath('//response/@message')[0]['message'])
            ? (string)$xml->xpath('//response/@message')[0]['message'] : '';
    }
}

