<?php

namespace kkb\models\responses;

class CardListResponseHandler
{
    private $cards = [];

    public function __construct($data)
    {
        $this->load($data);
    }

    public function getCards()
    {
        return $this->cards;
    }

    private function load($data)
    {
        $xml = new \SimpleXMLElement($data);
        $cardItems = $xml->xpath('//cards/item');

        if (is_array($cardItems)) {
            foreach ($cardItems as $item) {
                $this->cards[] = [
                    'pan'     => isset($item['cardnumasked']) ? (string)$item['cardnumasked'] : '',
                    'binding' => isset($item['reference']) ? (string)$item['reference'] : '',
                    'hash'    => isset($item['hash']) ? (string)$item['hash'] : '',
                ];
            }
        }
    }
}