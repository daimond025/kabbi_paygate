<?php

namespace kkb\models\responses;

use kkb\components\sign\Sign;
use kkb\exceptions\InvalidSignatureException;
use kkb\exceptions\PaymentException;
use kkb\models\Profile;

class PaymentInfoResponseHandler
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $bankData;

    /**
     * @var string
     */
    private $bankSign;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $approvalCode;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $pan;

    /**
     * @var string
     */
    private $clientId;

    public function __construct(Profile $profile, $data)
    {
        $this->profile = $profile;
        $this->load($data);
    }

    public function validate()
    {
        $sign = new Sign();
        $sign->invert();

        if (!$sign->isValid64($this->bankData, $this->bankSign, $this->profile->publicCertificate)) {
            throw new InvalidSignatureException('Invalid bank signature error');
        }
    }

    public function getPaymentInfo()
    {
        return [
            'status'       => $this->status,
            'reference'    => $this->reference,
            'approvalCode' => $this->approvalCode,
            'orderId'      => $this->orderId,
            'amount'       => $this->amount,
            'currency'     => $this->currency,
            'pan'          => $this->pan,
            'clientId'     => $this->clientId,
        ];
    }

    private function load($data)
    {
        $xml = new \SimpleXMLElement($data);

        $error = isset($xml->xpath('//Error')[0]) ? (string)$xml->xpath('//Error') : null;
        if ($error !== null) {
            throw new PaymentException("Payment status error: error=$error");
        }

        $this->bankData = isset($xml->xpath('//bank')[0]) ? $xml->xpath('//bank')[0]->asXML() : '';
        $this->bankSign = isset($xml->xpath('//bank_sign')[0])
            ? (string)$xml->xpath('//bank_sign')[0] : '';
        $this->status = isset($xml->xpath('//response/@AcceptRejectCode')[0]['AcceptRejectCode'])
            ? (string)$xml->xpath('//response/@AcceptRejectCode')[0]['AcceptRejectCode'] : '';
        $this->reference = isset($xml->xpath('//response/@reference')[0]['reference'])
            ? (string)$xml->xpath('//response/@reference')[0]['reference'] : '';
        $this->approvalCode = isset($xml->xpath('//response/@approval_code')[0]['approval_code'])
            ? (string)$xml->xpath('//response/@approval_code')[0]['approval_code'] : '';
        $this->orderId = isset($xml->xpath('//response/@OrderID')[0]['OrderID'])
            ? (string)$xml->xpath('//response/@OrderID')[0]['OrderID'] : '';
        $this->amount = isset($xml->xpath('//response/@amount')[0]['amount'])
            ? (float)$xml->xpath('//response/@amount')[0]['amount'] : 0;
        $this->currency = isset($xml->xpath('//response/@currencycode')[0]['currencycode'])
            ? (string)$xml->xpath('//response/@currencycode')[0]['currencycode'] : '';
        $this->pan = isset($xml->xpath('//response/@cardhash')[0]['cardhash'])
            ? (string)$xml->xpath('//response/@cardhash')[0]['cardhash'] : '';
        $this->clientId = isset($xml->xpath('//response/@person_id')[0]['person_id'])
            ? (string)$xml->xpath('//response/@person_id')[0]['person_id'] : '';
    }
}

