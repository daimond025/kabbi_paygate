<?php

namespace kkb\models;

use app\components\syslog\Syslog;
use v0\models\PaymentGateType;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kkb_payment_gate_profile".
 *
 * @property integer  $id
 * @property string   $paymentName
 * @property string   $merchantId
 * @property string   $merchantName
 * @property string   $certificateId
 * @property resource $privateCertificate
 * @property string   $privateCertificatePass
 * @property resource $publicCertificate
 * @property integer  $isDebug
 * @property integer  $cardBindingSum
 * @property double   $commission
 * @property string   $currency
 * @property integer  $createdAt
 * @property integer  $updatedAt
 */
class ProfileDAO extends ActiveRecord
{
    const TYPE_ID = 8;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kkb_payment_gate_profile';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'paymentName',
                    'merchantId',
                    'merchantName',
                    'certificateId',
                    'privateCertificate',
                    'privateCertificatePass',
                    'publicCertificate',
                    'cardBindingSum',
                ],
                'required',
            ],
            [['privateCertificate', 'publicCertificate'], 'string'],
            [['isDebug', 'createdAt', 'updatedAt'], 'integer'],
            [['paymentName', 'merchantId', 'certificateId'], 'string', 'max' => 32],
            [['merchantName', 'privateCertificatePass'], 'string', 'max' => 255],
            [['currency'], 'string', 'max' => 3],
            [['paymentName'], 'unique'],
            ['cardBindingSum', 'integer'],
            ['cardBindingSum', 'default', 'value' => 1],
            [
                ['commission'],
                'number',
                'message'  => Syslog::YII_MESSAGE_TO_INTEGER,
                'max'      => 100,
                'tooBig'   => Syslog::YII_MESSAGE_INTEGER_TOO_BIG,
                'min'      => 0,
                'tooSmall' => Syslog::YII_MESSAGE_INTEGER_TOO_SMALL,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                     => 'ID',
            'paymentName'            => 'Payment Name',
            'merchantId'             => 'Merchant ID',
            'merchantName'           => 'Merchant Name',
            'certificateId'          => 'Certificate ID',
            'privateCertificate'     => 'Private Certificate',
            'privateCertificatePass' => 'Private Certificate Pass',
            'publicCertificate'      => 'Public Certificate',
            'isDebug'                => 'Is Debug',
            'cardBindingSum'         => 'Card binding sum',
            'commission'             => 'Commission',
            'currency'               => 'Currency',
            'createdAt'              => 'Created At',
            'updatedAt'              => 'Updated At',
        ];
    }

    public function getTypeId()
    {
        return self::TYPE_ID;
    }

    public function setTypeId($value)
    {
    }

    public function getType()
    {
        return $this->hasOne(PaymentGateType::className(), ['id' => 'typeId']);
    }

    public function fields()
    {
        return [
            'paymentName',
            'typeId',
            'merchantId',
            'merchantName',
            'certificateId',
            'isDebug',
            'cardBindingSum',
            'commission',
            'currency',
        ];
    }
}
