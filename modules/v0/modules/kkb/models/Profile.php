<?php

namespace kkb\models;

use yandex\exceptions\NotFoundProfileException;

class Profile extends ProfileDAO
{
    const URL_API = 'https://epay.kkb.kz/jsp/';
    const URL_API_DEBUG = 'https://testpay.kkb.kz/jsp/';

    public static function get($paymentName)
    {
        $profile = self::find()->where(['=', 'paymentName', $paymentName])->one();
        if (!$profile) {
            throw new NotFoundProfileException(sprintf('Not found profile: %s', $paymentName));
        }

        return $profile;
    }

    public function getApiUrl()
    {
        return (int)$this->isDebug === 1 ? self::URL_API_DEBUG : self::URL_API;
    }

}