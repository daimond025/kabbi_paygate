<?php

namespace kkb\assets;

use yii\web\AssetBundle;

class KkbAssets extends AssetBundle
{
    public $sourcePath = '@kkb/assets';

    public $js = [
        'source/main.js',
    ];

    public $css = [
        'source/styles/css/main.css',
    ];

    public $depends = ['yii\web\JqueryAsset'];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}