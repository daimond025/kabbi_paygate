<?php

namespace kkb\components;

use app\components\syslog\Syslog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use paycom\exceptions\HttpClientException;

class KkbApi
{
    const CARD_URL = 'remote/control_recur.jsp';
    const REPEAT_PAYMENT_URL = 'hbpay/rec.jsp';
    const PAYMENT_STATUS_URL = 'remote/checkOrdern.jsp';
    const PAYMENT_CONTROL_URL = 'remote/control.jsp';

    /**
     * @var Syslog
     */
    private $logger;

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client, Syslog $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function getCards($baseUrl, $query)
    {
        return $this->get($baseUrl . self::CARD_URL, $query, 'Get cards');
    }

    public function removeCard($baseUrl, $query)
    {
        return $this->get($baseUrl . self::CARD_URL, $query, 'Remove card');
    }

    public function repeatPayment($baseUrl, $signedOrder)
    {
        return $this->post($baseUrl . self::REPEAT_PAYMENT_URL, ['Signed_Order_B64' => $signedOrder], 'Repeat payment');
    }

    public function getPaymentInfo($baseUrl, $query)
    {
        return $this->get($baseUrl . self::PAYMENT_STATUS_URL, $query, 'Get payment info');
    }

    public function refundPayment($baseUrl, $query)
    {
        return $this->get($baseUrl . self::PAYMENT_CONTROL_URL, $query, 'Refund/reverse payment');
    }

    private function get($url, $query, $errorTitle = 'Request')
    {
        try {
            $response = $this->client->get($url . '?' . urlencode($query));

            $result = $response->getBody()->getContents();
            $this->addDebugLog($url, $query, $result);

            return $result;
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $this->addDebugLog($url, $query, $response->getBody()->getContents());

            throw new HttpClientException("$errorTitle error: error={$e->getMessage()}", $e);
        } catch (\Exception $e) {
            throw new HttpClientException("$errorTitle error: error={$e->getMessage()}", $e);
        }
    }

    private function post($url, $params, $errorTitle = 'Request')
    {
        try {
            $response = $this->client->post($url, [
                RequestOptions::FORM_PARAMS => $params,
            ]);

            $result = $response->getBody()->getContents();
            $this->addDebugLog($url, json_encode($params), $result);

            return $result;
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $this->addDebugLog($url, json_encode($params), $response->getBody()->getContents());

            throw new HttpClientException("$errorTitle error: error={$e->getMessage()}", $e);
        } catch (\Exception $e) {
            throw new HttpClientException("$errorTitle error: error={$e->getMessage()}", $e);
        }
    }

    private function addDebugLog($url, $request, $response)
    {
        try {
            $message = sprintf('%s %s -> %s', $url, $request, $response);
            $this->logger->addDebugLog($message);
        } catch (\Exception $ignore) {
        }
    }
}