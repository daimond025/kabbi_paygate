<?php

namespace kkb\components;

use kkb\exceptions\NotFoundProfileException;
use kkb\exceptions\PaymentException;
use kkb\models\Profile;
use kkb\models\requests\CardActionRequest;
use kkb\models\requests\PaymentInfoRequest;
use kkb\models\requests\RefundPaymentRequest;
use kkb\models\requests\RegisterCardRequest;
use kkb\models\requests\RepeatPaymentRequest;
use kkb\models\responses\CardListResponseHandler;
use kkb\models\responses\PaymentInfoResponseHandler;
use kkb\models\responses\PaymentSuccessResponseHandler;
use kkb\models\responses\RefundPaymentResponseHandler;
use kkb\models\responses\RepeatPaymentResponseHandler;
use v0\components\currency\CurrencyException;
use v0\components\currency\CurrencyService;
use yii\helpers\Url;

class KkbService
{
    const DEFAULT_LOCALE = 'ru';
    const AVAILABLE_LOCALES = ['ru', 'en'];

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var KkbApi
     */
    private $api;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * @var string
     */
    private $externalUrl;

    public function __construct(KkbApi $api, CurrencyService $currencyService, $externalUrl)
    {
        $this->api = $api;
        $this->externalUrl = rtrim($externalUrl, '/');
        $this->currencyService = $currencyService;
    }

    public function setProfile($paymentName)
    {
        $this->profile = Profile::get($paymentName);
    }

    public function getProfile()
    {
        if ($this->profile === null) {
            throw new NotFoundProfileException('Kkb profile not found');
        }

        return $this->profile;
    }

    public function filterLocale($locale)
    {
        return in_array($locale, self::AVAILABLE_LOCALES, true) ? $locale : self::DEFAULT_LOCALE;
    }

    public function getLanguageByLocale($locale)
    {
        if ($locale === 'en') {
            return 'eng';
        }

        return 'rus';
    }

    public function getRegisterCardUrlAndOrder($customer, $locale = self::DEFAULT_LOCALE)
    {
        $orderId = $this->generateOrderId();

        return [
            'url'   => $this->externalUrl . Url::to([
                    '/v0/kkb/card/index',
                    'paymentName' => $this->getProfile()->paymentName,
                    'clientId'    => $customer->clientId,
                    'orderId'     => $orderId,
                    'locale'      => $locale,
                ]),
            'order' => $orderId,
        ];
    }

    public function getProcessPaymentUrl()
    {
        return "{$this->getProfile()->getApiUrl()}process/logon.jsp";
    }

    public function getCardSuccessUrl($locale = self::DEFAULT_LOCALE)
    {
        return $this->externalUrl . Url::to(['/v0/kkb/card/success', 'locale' => $locale]);
    }

    public function getCardFailUrl($locale = self::DEFAULT_LOCALE)
    {
        return $this->externalUrl . Url::to(['/v0/kkb/card/fail', 'locale' => $locale]);
    }

    public function getPaymentSuccessUrl()
    {
        return $this->externalUrl . Url::to('/v0/kkb/payment/success');
    }

    public function getRegisterCardSignedOrder($clientId, $orderId = null)
    {
        if ($orderId === null) {
            $orderId = $this->generateOrderId();
        }

        $request = new RegisterCardRequest($this->getProfile(), $clientId, $orderId);

        return $request->getSignedOrder();
    }

    public function handlePaymentSuccess($data)
    {
        $handler = new PaymentSuccessResponseHandler($this, $data);
        $handler->validate();
    }

    public function getCards($clientId)
    {
        $request = new CardActionRequest($this->getProfile(), $clientId);
        $response = $this->api->getCards($this->profile->getApiUrl(),
            $request->generate(CardActionRequest::ACTION_LIST));

        $handler = new CardListResponseHandler($response);

        return $handler->getCards();
    }

    public function removeCard($clientId, $reference)
    {
        $cardHash = $this->getCardHash($clientId, $reference);
        if ($cardHash === null) {
            return;
        }

        $request = new CardActionRequest($this->getProfile(), $clientId);
        $this->api->removeCard($this->profile->getApiUrl(),
            $request->generate(CardActionRequest::ACTION_DELETE, $cardHash));
    }

    public function repeatPayment($clientId, $orderId, $bindingId, $amount, $currency)
    {
        $normalizedOrderId = $this->getNormalizedOrderId($orderId);
        $convertedAmount = $this->convertAmountToMajorUnit($amount, $currency);

        $profile = $this->getProfile();
        if ((int)$profile->currency !== (int)$currency) {
            throw new PaymentException("Invalid currency: profileCurrency={$profile->currency}, orderCurrency={$currency}");
        }

        $request = new RepeatPaymentRequest($this->getProfile(), $clientId, $normalizedOrderId, $bindingId,
            $convertedAmount, $currency);
        $response = $this->api->repeatPayment($this->profile->getApiUrl(), $request->getSignedOrder());

        $handler = new RepeatPaymentResponseHandler($this->profile, $response);
        $handler->validate();

        return $normalizedOrderId;
    }

    public function getPaymentInfo($orderId)
    {
        if ($orderId === null) {
            throw new PaymentException("Get payment info error: orderId={$orderId}");
        }

        $request = new PaymentInfoRequest($this->getProfile(), $orderId);
        $response = $this->api->getPaymentInfo($this->profile->getApiUrl(), $request->generate());

        $handler = new PaymentInfoResponseHandler($this->profile, $response);
        $handler->validate();

        return $handler->getPaymentInfo();
    }

    public function refundPayment($orderId)
    {
        $paymentInfo = $this->getPaymentInfo($orderId);
        $request = new RefundPaymentRequest($this->profile, $paymentInfo['status'], $paymentInfo['reference'],
            $paymentInfo['approvalCode'], $paymentInfo['orderId'], $paymentInfo['amount'], $paymentInfo['currency']);
        $response = $this->api->refundPayment($this->profile->getApiUrl(), $request->generateRequest('Refund money'));

        $handler = new RefundPaymentResponseHandler($this->profile, $response);
        $handler->validate();
    }

    private function getCardHash($clientId, $reference)
    {
        $hashes = array_map(function ($item) {
            return $item['hash'];
        }, array_filter($this->getCards($clientId), function ($item) use ($reference) {
            return $item['binding'] === $reference;
        }));

        return !empty($hashes) ? current($hashes) : null;
    }

    private function generateOrderId()
    {
        return (string)round(microtime(true) * 10 ** 4);
    }

    private function getNormalizedOrderId($orderId)
    {
        return str_pad($orderId, 6, '0', STR_PAD_LEFT);
    }

    private function convertAmountToMajorUnit($amount, $currencyCode)
    {
        $currency = $this->currencyService->getCurrency($currencyCode);
        if ($currency['minorUnit'] === null) {
            throw new CurrencyException("Unknown minor unit of currency: currency={$currency}");
        }

        return (int)$currency['minorUnit'] === 0 ? $amount : $amount / (10 ** $currency['minorUnit']);
    }
}