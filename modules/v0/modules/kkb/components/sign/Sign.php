<?php

namespace kkb\components\sign;

class Sign
{
    private $invert;

    private $privateKey;

    public function loadPrivateKey($key, $password = null)
    {
        $privateKey = $password !== null ? openssl_get_privatekey($key, $password) : openssl_get_privatekey($key);
        if (!is_resource($privateKey)) {
            throw new CertificateException('Incorrect private key or password');
        }

        $this->privateKey = $privateKey;
    }

    public function invert()
    {
        $this->invert = 1;
    }

    private function reverse($str)
    {
        return strrev($str);
    }

    public function sign($str)
    {
        if ($this->privateKey) {
            openssl_sign($str, $out, $this->privateKey);

            if ($this->invert === 1) {
                $out = $this->reverse($out);
            }

            return $out;
        }
    }

    public function sign64($str)
    {
        return base64_encode($this->sign($str));
    }

    public function isValid($data, $str, $key)
    {
        if ($this->invert === 1) {
            $str = $this->reverse($str);
        }

        return openssl_verify($data, $str, $key);
    }

    public function isValid64($data, $str, $publicKey)
    {
        return $this->isValid($data, base64_decode($str), $publicKey);
    }
}