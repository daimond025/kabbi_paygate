<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;

$this->title = 'EPay';

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="/favicon.ico">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <?= $content ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>