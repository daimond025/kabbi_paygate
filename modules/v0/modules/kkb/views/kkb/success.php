<?php

use kkb\Module;
use kkb\assets\KkbAssets;

KkbAssets::register($this);

?>

<div class="success">
    <img src="/images/success.svg">
    <p><?= Module::t('main', 'Your card was successfully added and you can use it for paying!') ?></p>
</div>