<?php

use kkb\assets\KkbAssets;
use kkb\Module;
use yii\web\View;

/* @var $this View */
/* @var $signedOrder string */
/* @var $language string */
/* @var $processUrl string */
/* @var $backLink string */
/* @var $failureBackLink string */
/* @var $postLink string */
/* @var $failurePostLink string */

KkbAssets::register($this);

?>

<div class="container" style="padding-top: 50px; text-align: center">
    <div class="js-text" style="display: none; font-size: 18px;">
        <?= Module::t('main', 'Now you will be redirected') ?>
    </div>
    <form name="SendOrder" method="post" action="<?= $processUrl ?>">
        <input type="hidden" name="Signed_Order_B64" value="<?= $signedOrder ?>">
        <input type="hidden" name="Language" value="<?= $language ?>">
        <input type="hidden" name="BackLink" value="<?= $backLink ?>">
        <input type="hidden" name="FailureBackLink" value="<?= $failureBackLink ?>">
        <input type="hidden" name="PostLink" value="<?= $postLink ?>">
        <noscript>
            <input class="js-submit button" type="submit" value="<?= Module::t('main', 'Add card') ?>">
        </noscript>
    </form>
</div>



