<?php

use kkb\Module;
use kkb\assets\KkbAssets;

KkbAssets::register($this);

?>

<div class="error">
    <img src="/images/error.svg">
    <p><?= Module::t('main', 'Error') ?>:<br>
        <?= Module::t('main',
            'Credit card data is incorrect or service is temporarily unavailable. Check the data or try later.') ?></p>
</div>