<?php

namespace kkb;

use GuzzleHttp\Client;
use kkb\components\KkbApi;
use kkb\components\KkbService;
use kkb\models\Profile;
use v0\Module as V0;
use yii\i18n\PhpMessageSource;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'kkb\controllers';

    public $externalUrl;
    public $networkTimeout;
    public $syslog;
    public $enableDebug;

    public function init()
    {
        parent::init();

        $httpClient = new Client(['timeout' => $this->networkTimeout]);
        $kkbApi = new KkbApi($httpClient, $this->syslog);
        $kkbService = new KkbService($kkbApi, V0::getComponent('currency_service'), $this->externalUrl);

        $this->components = [
            'paymentGateProfile' => Profile::class,
            'kkbService'         => $kkbService,
        ];

        $this->layout = 'main';
        \Yii::$app->layout = false;

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['kkb/*'] = [
            'class'          => PhpMessageSource::class,
            'sourceLanguage' => 'en-US',
            'basePath'       => '@kkb/messages',
            'fileMap'        => [
                'kkb/main' => 'main.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('kkb/' . $category, $message, $params, $language);
    }

}