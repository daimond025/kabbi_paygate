<?php

namespace kkb\controllers;

use app\components\syslog\Syslog;
use kkb\components\KkbService;
use kkb\Module;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class KkbController extends Controller
{
    /**
     * @var KkbService
     */
    private $kkbService;

    /**
     * @var Syslog
     */
    private $syslog;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    '*/*' => Response::FORMAT_HTML,
                ],
            ],
        ];
    }

    /**
     * @override
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        $locale = $this->kkbService->filterLocale(\Yii::$app->request->get('locale'));
        \Yii::$app->language = $locale === null ? KkbService::DEFAULT_LOCALE : $locale;

        return parent::beforeAction($action);
    }

    public function init()
    {
        $this->kkbService = Module::getInstance()->get('kkbService');
        $this->syslog = Module::getInstance()->syslog;

        parent::init();
    }

    public function actionCardIndex()
    {
        $request = \Yii::$app->request;
        $paymentName = $request->get('paymentName');
        $clientId = $request->get('clientId');
        $orderId = $request->get('orderId');

        try {
            $this->kkbService->setProfile($paymentName);
            $signedOrder = $this->kkbService->getRegisterCardSignedOrder($clientId, $orderId);

            $language = $this->kkbService->getLanguageByLocale(\Yii::$app->language);
            $processUrl = $this->kkbService->getProcessPaymentUrl();
            $backLink = $this->kkbService->getCardSuccessUrl(\Yii::$app->language);
            $failureBackLink = $this->kkbService->getCardFailUrl(\Yii::$app->language);
            $postLink = $this->kkbService->getPaymentSuccessUrl();

        } catch (\Exception $ex) {
            $this->logError("Register create error (Kkb): paymentName=\"{$paymentName}\", clientId=\"{$clientId}\", orderId=\"{$orderId}\", error=\"{$ex->getMessage()}\"");

            return $this->redirect('/v0/kkb/kkb/card-fail');
        }

        return $this->render('index',
            compact('signedOrder', 'language', 'processUrl', 'backLink', 'failureBackLink', 'postLink'));
    }

    public function actionCardSuccess()
    {
        return $this->render('success');
    }

    public function actionCardFail()
    {
        return $this->render('fail');
    }

    public function actionPaymentSuccess()
    {
        $request = \Yii::$app->request;
        $response = $request->post('response');

        try {
            $this->kkbService->handlePaymentSuccess($response);
        } catch (\Exception $ex) {
            $this->logError("Handle success payment error (Kkb): error=\"{$ex->getMessage()}\"");
        }

        if (Module::getInstance()->enableDebug) {
            $this->logResponse(\Yii::$app->requestedAction->id, \Yii::$app->request->post());
        }

        return 0;
    }

    private function logResponse($method, $post)
    {
        $postParams = json_encode($post, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $this->sendToSyslog("Kkb callback \"{$method}\": post params=\"$postParams}\"");
    }

    private function sendToSyslog($message)
    {
        if ($this->syslog !== null) {
            $this->syslog->sendLog($message);
        }
    }

    private function logError($message)
    {
        $this->sendToSyslog($message);
        \Yii::error($message);
    }
}