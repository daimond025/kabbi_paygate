<?php

namespace v0\controllers;

use yii\filters\ContentNegotiator;
use yii\web\Response;

class CardController extends \yii\rest\Controller
{
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }
    
    public function actions()
    {
        return [
            'index' => 'v0\actions\card\GetListCardOnCustomer',
            'create' => 'v0\actions\card\Create',
            'delete' => 'v0\actions\card\Delete',
            'check' => 'v0\actions\card\Check',
        ];
    }
}
