<?php

namespace v0\controllers;

use yii\filters\ContentNegotiator;
use yii\web\Response;

class PaymentController extends \yii\rest\Controller
{
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }
    
    public function actions()
    {
        return [
            'deposit' => 'v0\actions\payment\Deposit',
            'refund' => 'v0\actions\payment\Refund',
            'status' => 'v0\actions\payment\Status',
        ];
    }
}
