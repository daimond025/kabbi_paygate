<?php

namespace v0\controllers;

use yii\filters\ContentNegotiator;
use yii\web\Response;

class ProfileController extends \yii\rest\Controller
{
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'index' => 'v0\actions\profile\GetAllTypes',
            'create' => 'v0\actions\profile\Create',
            'update' => 'v0\actions\profile\Update',
            'delete' => 'v0\actions\profile\Delete',
            'view' => 'v0\actions\profile\View',
        ];
    }
}
