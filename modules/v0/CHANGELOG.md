# Микросервис для работы с платежными шлюзами в ресурсе Gootax

## Версия 0.5.7 [2017-08-28] ##

* удалены неиспользуемые проверки в методе `status`

## Версия 0.5.6 [2017-08-28] ##

* [#3355](https://red.gootax.pro/issues/3355) для мониторинга состояния добавлены методы: `status` и `version`
* [#3356](https://red.gootax.pro/issues/3356) переработана конфигурация и обновлены зависимости
* переработана структура тестов

## Версия 0.5.5 [2017-04-04] ##

* Добавлено логирование переменных X_REAL_IP и HTTP_X_REAL_PORT.
* Добавлено логирование запросов через модуль Guzzle.
* Переработана логика работы с Яндекс кассой.


## Версия 0.5.4 [2017-01-31] ##

* Добавлена Yii-реализация функции getallheaders(), для корректной работы в php-интерпретаторе без её поддержки.


## Версия 0.5.3 [2016-12-13] ##

* Убран лишний пробел в логах если отсутствует request_id.
* Исправлена работа кэша при получении курса валют.


## Версия 0.5.2 [2016-11-17] ##

* Для сбербанка добавлена валидация результата процессинга оплаты.
* Добавлено логирование идентификатора запроса.
* Добавлен composer.lock для фиксации версий расширений.
* В debug строчках лога теперь скрывается поле с паролем.
* Изменено значение по-умолчанию identity в компоненте `Syslog` с `Payment` на `paygate`.
* Добавлена реализация платежного шлюза Страйп (Stripe), но временно отключена. Для возможности добавления карты нужно открыть доступ к следующему урл: `/v0/stripe/[\\w\\d]+/[\\w\\d]+`


## Версия 0.5.1 [2016-09-21] ##

* Запрещенно указывать тип профиля `typeId` при обновлени профиля платежного шлюза
* Исправлена ошибка при оплате несуществующей картой в платежном шлюзе сбербанка 
* Исправлена работа платежного шлюза при проверке карты
* Увеличена максимальная длина полей `passwordBinding` и `passwordPayment` в профиле сбербанка с 32 до 255.
* Поправлена работа куков в сервисе `cbr.ru`


## Версия 0.5.0 [2016-09-15] ##

* Добавлены изменения в платежный шлюз Сбербанка. Поля `username` и `password` 
заменены на `usernameBinding`, `passwordBinding`, `usernamePayment` и `passwordPayment`. 


## Версия 0.4.3 [2016-08-18] ##

* Исправлена критическая ошибка при работе с платежными шлюзами Сбербанка, Альфабанка и ВТБ24


## Версия 0.4.2 [2016-08-18] ##

* Изменены дополнительные uri ПШ сервиса "Яндекс.Касса" для проверки заказа `/v0/yandex/order/check` и для уведомления перевода `v0/yandex/order/aviso`. (Прошлые значения `/checkorder.php` и `/paymentaviso.php` соответствнно)


## Версия 0.4.1 [2016-08-08] ##

* Изменения подписи у syslog при неизвестном значениях адреса и порта. Теперь будет выглядеть так: `???:??? -> "PUT /blabla"`
* Изменено описание в syslog при совершении платежа картой. Добавлена валюта, например: `"Deposit order number='123' on amount='10000', currency='643' with pan='123456xxxx1234' is successful"`
* Изменено описание в syslog при обновлении профиля. Добавлены названия параметров и их значения, являющимися публичными и не превышающие длину более 255 символов, например: `Update params: commission='10', password, user='testUser', keyContent on the profile is successful`. Тут `password` является секретными данными, а `keyContent` превышает длину в 255 символов.


## Версия 0.4.0 [2016-08-01] ##

* В поле commission профиля теперь можно хранить дробное значение
* При получении данных профиля скрыто значения поля password
* Добавлена интеграция Яндекс.Кассы в платежный шлюз со следующими изменениями:
	* Тип профиля `typeId = 4`
	* Набор полей профиля `paymentName`, `scid`, `shopId`, `password`, `keyContent`, `certificateContent`, `certificatePassword`, `commission` и `isDebug`.
	* Сервис "Яндекс.Касса" может обратится по дополнительным uri ПШ для проверки заказа `/checkorder.php` и для уведомления перевода `/paymentaviso.php`


## Версия 0.3.2 [2016-06-23] ##

* Изменение архитектуры в модуле `v0` при обработке actions. Используется принцип middleware
* Изменение работы с компонентом `syslog`. Данные нужно явно отправлять в компонент. 
* Изменение запроса с `/v0/version` на `/version`


## Версия 0.3.1 [2016-06-03] ##

* Получения ставки конвертации из ЦРБ берется по текущему времени Москвы. 
* Исправлена ошибка работы компонента приложения `curl`. Теперь можно совершать запросы без данных.
* Исправлена ошибка проверки минимально стоимости оплаты заказа. Теперь берется значение после конвертации. 


## Версия 0.3.0 [2016-05-31] ##

* Добавление необязательного параметра currency при добавлении новой карты.
* Добавление запроса `/v0/version` для получения версии в модуле `v0`.
* Использование автоматической конвертации валют у Сбербанка.
* Приведение логов к единому стилю: КТО -> "ЧТО": РЕЗУЛЬТАТ (ПОЧЕМУ) СТАТИСТИКА
* Добавление тестов на конвертацию.


## Версия 0.2.0-rc1 [2016-04-27] ##

* Добавлен новые виды ошибок: Операция не приемлема и недостаточно средст на счет клиента.
* Изменены значения по-умолчанию в платежном шлюзе для параметра returnUrl и failUrl.
* Изменён вид логированных сообщений. Сообщения ориентированы на вход другой программы и для статистики.
Сообщения для отладки минимизированы и выводит только общение с банком.
* Добавлен параметр для настройки таймаута ответа от банка. 
