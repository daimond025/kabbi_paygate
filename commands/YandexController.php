<?php

namespace app\commands;

use yandex\components\queue\CancelPaymentException;
use yandex\components\queue\MQConnection;
use yandex\components\queue\CancelPaymentService;
use yii\console\Controller;

/**
 * Class YandexController
 * @package app\commands
 */
class YandexController extends Controller
{
    public function actionCancelPaymentConsumerStart()
    {
        $service = $this->getCancelPaymentService();
        $service->startConsumer();
    }

    /**
     * @param string $paymentName
     * @param string $orderNumber
     * @param int    $attemptNumber
     *
     * @throws CancelPaymentException
     */
    public function actionPublish($paymentName, $orderNumber, $attemptNumber = 1)
    {
        $service = $this->getCancelPaymentService();
        $service->publish($paymentName, $orderNumber, $attemptNumber);
    }

    /**
     * @return CancelPaymentService
     */
    private function getCancelPaymentService()
    {
        $connection = new MQConnection(
            \Yii::$app->params['rabbitMQ.host'],
            \Yii::$app->params['rabbitMQ.port'],
            \Yii::$app->params['rabbitMQ.user'],
            \Yii::$app->params['rabbitMQ.password'],
            \Yii::$app->params['rabbitMQ.virtualHost']);

        return new CancelPaymentService($connection, \Yii::$app, \Yii::$app->syslog);
    }

}