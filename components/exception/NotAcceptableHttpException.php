<?php
namespace app\components\exception;

use Yii;
use yii\web\HttpException;

class NotAcceptableHttpException extends HttpException
{
    public function __construct($message = '', \Exception $previous = null)
    {
        if (! $message) {
            $message = Yii::t('app/error', 'No set');
        }
        parent::__construct(406, $message, 5, $previous);
    }
    
    public function getName()
    {
        return 'Not acceptable';
    }
    
}