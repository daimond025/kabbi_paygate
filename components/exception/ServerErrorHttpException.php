<?php

namespace app\components\exception;

class ServerErrorHttpException extends \yii\web\ServerErrorHttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        parent::__construct($message, 255, $previous);
    }
}