<?php
namespace app\components\exception;

use yii\web\HttpException;

class InvalidArgumentException extends HttpException
{
    public function __construct($message = '', \Exception $previous = null)
    {
        parent::__construct(400, $message, 1, $previous);
    }
    
    public function getName()
    {
        return 'Invalid argument';
    }
    
}