<?php

namespace app\components\exception;

class NotFoundHttpException extends \yii\web\NotFoundHttpException
{
    public function __construct($message = '', \Exception $previous = null)
    {
        parent::__construct($message, 2, $previous);
    }
}