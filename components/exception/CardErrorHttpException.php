<?php
namespace app\components\exception;

class CardErrorHttpException extends \yii\web\HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        if (is_null($message)) {
            $message = \Yii::t('app/error', 'it wasn\'t succeeded to make payment');
        }
        parent::__construct(400, $message, 4, $previous);
    }
    
    public function getName()
    {
        return 'Card error';
    }
    
}