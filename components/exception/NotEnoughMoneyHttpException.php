<?php
namespace app\components\exception;

use Yii;
use yii\web\HttpException;

class NotEnoughMoneyHttpException extends HttpException
{
    public function __construct($message = 'no set', \Exception $previous = null)
    {
        parent::__construct(402, $message, 16, $previous);
    }
    
    public function getName()
    {
        return 'Not enough money';
    }
}