<?php
namespace app\components\exception;

class ServiceErrorHttpException extends \yii\web\HttpException
{
    public function __construct($message = null, \Exception $previous = null)
    {
        if (is_null($message)) {
            $message = \Yii::t('app/error', 'The payment gate internal error');
        }
        parent::__construct(424, $message, 3, $previous);
    }
    
    public function getName()
    {
        return 'Error from bank';
    }
}