<?php

namespace app\components\syslog;

use yii\base\Object;

class Syslog extends Object
{
    /* @see http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html */
    const YII_MESSAGE_STRING_TOO_LONG = "{attribute}=''{value}'' should contain at most {max, number} {max, plural, one{character} other{characters}}";
    const YII_MESSAGE_TO_INTEGER = "{attribute}='{value}' must be an integer";
    const YII_MESSAGE_TO_UNIQUE = "{attribute}='{value}' has already been taken";
    const YII_MESSAGE_INTEGER_TOO_BIG = "{attribute}='{value}' must be no greater than {max}";
    const YII_MESSAGE_INTEGER_TOO_SMALL = "{attribute}='{value}' must be no less than {min}";
    const YII_MESSAGE_TO_EXISTS = "{attribute}='{value}' is invalid";

    public $enable = true;

    public $enableDebug = false;

    public $identity = 'paygate';

    public $facility = LOG_USER;

    private $_debugLog = [];

    private $_startSystemTime;

    private $_isSent = false;

    public function init()
    {
        $this->_startSystemTime = $this->getSystemsTime();
        parent::init();
    }

    private function getSystemsTime()
    {
        $data = getrusage();

        return [
            microtime(true) * 1e6,
            $data["ru_utime.tv_sec"] * 1e6 + $data["ru_utime.tv_usec"],
            $data["ru_stime.tv_sec"] * 1e6 + $data["ru_stime.tv_usec"],
        ];
    }

    public function reset()
    {
        $this->_isSent = false;
    }

    public function sendLog($log = '')
    {
        if ($this->_isSent) {
            return;
        }

        if ($this->enable) {
            openlog($this->getIdentity(), LOG_ODELAY, $this->facility);

            $logLevel = $log instanceof \yii\web\HttpException ? LOG_WARNING : LOG_INFO;

            syslog($logLevel, $this->getHeaderMessage($log));
            if ($this->enableDebug || $logLevel < LOG_INFO) {
                foreach ($this->_debugLog as $log) {
                    syslog($logLevel, $log);
                }
            }
            closelog();
        }

        $this->_isSent = true;
    }

    private function getIdentity()
    {
        return $this->identity . '-' . \Yii::$app->params['version'];
    }

    private function getHeaderMessage($log)
    {
        $message = $this->getSignature();
        if ($log instanceof \yii\web\HttpException) {
            $message .= "{$log->statusCode} {$log->getName()} ({$log->getMessage()}) ";
        } else {
            $message .= "200 OK ($log) ";
        }
        $message .= $this->getStatistics();

        return $message;
    }

    private function getHeaders()
    {
        if (function_exists('getallheaders')) {
            $headers = getallheaders();
        } elseif (function_exists('http_get_request_headers')) {
            $headers = http_get_request_headers();
        } else {
            $headers = [];
            foreach ($_SERVER as $name => $value) {
                if (strncmp($name, 'HTTP_', 5) === 0) {
                    $name = str_replace(' ', '-',
                        ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                    $headers[$name] = $value;
                }
            }
        }

        return $headers;
    }

    private function getRequestId()
    {
        $headers = $this->getHeaders();

        return isset($headers['Request-Id']) ? $headers['Request-Id'] : null;
    }

    private function getSender()
    {
        list($realIp, $realPort) = $this->selectServerParams(['HTTP_X_REAL_IP', 'HTTP_X_REAL_PORT']);
        list($remoteIp, $remotePort) = $this->selectServerParams(['REMOTE_ADDR', 'REMOTE_PORT'], '???');

        return implode(':', [
            empty($realIp) ? $remoteIp : $realIp,
            empty($realPort) ? $remotePort : $realPort,
        ]);
    }

    private function getSignature()
    {
        $client = $this->getSender();
        $answer = implode(' ', array_filter($this->selectServerParams(
            ['REQUEST_METHOD', 'REQUEST_URI', 'SERVER_PROTOCOL'])));
        $requestId = $this->getRequestId();
        $requestId = empty($requestId) ? '' : $requestId . ' ';

        return "$client -> {$requestId}\"$answer\": ";
    }

    private function selectServerParams($keys, $defaultValue = '')
    {
        $result = [];
        foreach ($keys as $key) {
            $result[] = key_exists($key, $_SERVER) ? $_SERVER[$key] : $defaultValue;
        }

        return $result;
    }

    private function getStatistics()
    {
        list($time, $utime, $stime) = array_map(function ($a, $b) {
            return ($b - $a) / 1000.0;
        }, $this->_startSystemTime, $this->getSystemsTime());

        // see http://stackoverflow.com/questions/1361451/get-size-of-post-request-in-php#1361451
        $sizeRequest = key_exists('CONTENT_LENGTH', $_SERVER) ? $_SERVER['CONTENT_LENGTH'] : 0;

        $memmory = memory_get_peak_usage() / 1000.0;

        return sprintf("n:%db t:%.3fms u:%.3fms s:%.3fms m:%dkb", $sizeRequest, $time, $utime, $stime, $memmory);
    }

    public function addDebugLog($log)
    {
        $this->_debugLog[] = $this->getSignature() . 'DEBUG: ' . $log;
    }

}